package com.example.ducng.checkinproject.util.mvp.manager.request_list_member;

import android.util.Log;

import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.data.model.manager.get_list_member.IModelGetMemberSuccess;
import com.example.ducng.checkinproject.data.model.manager.get_list_member.IModelGetMember;
import com.example.ducng.checkinproject.data.model.manager.get_list_member.ModelGetListMember;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.IViewRequestListMembe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PresenterRequestListMember implements IPresenterRequestListMember {
    private IModelGetMember iModelGetMember;

    public PresenterRequestListMember() {
    }

    @Override
    public void requestListMember(String groupId) {
        iModelGetMember = new ModelGetListMember();
        iModelGetMember.getUserInGroupToFirebase( groupId);
    }


}
