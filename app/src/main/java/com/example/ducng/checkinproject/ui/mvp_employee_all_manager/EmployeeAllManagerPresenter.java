package com.example.ducng.checkinproject.ui.mvp_employee_all_manager;

import com.example.ducng.checkinproject.models.model_employee_all.IModelEmployeeAllManager;
import com.example.ducng.checkinproject.models.model_employee_all.ModelEmployeeAllManager;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public class EmployeeAllManagerPresenter implements IEmployeeAllManagerContact.IEmployeeAllManagerPresenter, IGetAllEmployeeFirebase {
    IEmployeeAllManagerContact.IEmployeeAllManagerView view;
    IModelEmployeeAllManager modelEmployeeAllManager;

    public EmployeeAllManagerPresenter(IEmployeeAllManagerContact.IEmployeeAllManagerView view) {
        this.view = view;
        this.modelEmployeeAllManager = new ModelEmployeeAllManager();
    }

    @Override
    public void getAllEmployee() {
        //modelEmployeeAllManager.loadAllEmployee(this);
        modelEmployeeAllManager.loadAllEmployeeUseEventBus();
    }

    @Override
    public void getAllEmployeeSucceed(List<User> users) {
        view.loadSucceed(users);
    }

    @Override
    public void getAllEmployeeFailue() {
        view.loadFailue();
    }
}
