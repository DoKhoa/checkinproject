package com.example.ducng.checkinproject.ui.mvp_add_employee;

import com.example.ducng.checkinproject.models.model_add_employee.IModelAddEmployee;
import com.example.ducng.checkinproject.models.model_display_group.IModelDisplayGroup;
import com.example.ducng.checkinproject.models.model_add_employee.ModelAddEmployee;
import com.example.ducng.checkinproject.models.model_display_group.ModelDisplayGroup;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.mvp_display_group.IGetGroupFromFirebase;

import java.util.ArrayList;
import java.util.List;

public class AddEmployeePresenter implements IAddEmployeeContact.IAddEmployeePresenter,
        IGetGroupFromFirebase, IGetEmployeeWithoutGroupFirebase, IAddEmployeeFirebase, IUpdateLeaderFirebase {

    private IAddEmployeeContact.IAddEmployeeView view;
    private IModelDisplayGroup modelDisplayGroup;
    private IModelAddEmployee  modelAddEmployee;

    public AddEmployeePresenter(IAddEmployeeContact.IAddEmployeeView view) {
        this.view = view;
        this.modelDisplayGroup = new ModelDisplayGroup();
        modelAddEmployee = new ModelAddEmployee();
    }

    @Override
    public void addAllNameGroup() {
        modelDisplayGroup.loadGroupFormFirebase(this);
    }

    @Override
    public void getAllUserWithoutGroup() {
       modelAddEmployee.loadEmployeeWithoutGroup(this);
    }

    @Override
    public void addEmployeeToGroup(User user, String groupId, int priovity) {
        modelAddEmployee.addEmployeeToGroup(this,  user, groupId, priovity);
    }

    @Override
    public void updateLead(User user, String groupId) {
        modelAddEmployee.updateLeaderGroup(this,user, groupId);
    }

    @Override
    public void getSucceed(List<Group> groups) {
        List<String> groupNames = new ArrayList<String>();
        if(groupNames != null){
            for(Group group: groups){
                groupNames.add(group.getId());
            }
        }
        view.displaySpinnerGroup(groupNames);
    }

    @Override
    public void getFailue() {
        view.showToast("Load  du lieu that bai");
    }

    @Override
    public void getEmployeeWithoutGroupSucceed(List<User> users) {
//        List<String> usersName = new ArrayList<String>();
//        if(usersName != null){
//            for(User user: users){
//                usersName.add(user.getName());
//            }
//        }
        view.displayAutoCompleteTVEmployee(users);
    }

    @Override
    public void getEmployeeWithoutGroupFailue() {
        view.showToast("Load du lieu that bai");
    }

    @Override
    public void addEmployeeSucceed() {
        view.addSucceed();
    }

    @Override
    public void addEmployeeFailue() {
        view.showToast("Chua the them nhan vien vao nhom");
    }


    @Override
    public void updateLeadSucceed(String managerId) {
        view.addSucceed();
    }

    @Override
    public void updateLeadFailue() {
        view.showToast("Thay doi chuyen nhom that bai");
    }
}
