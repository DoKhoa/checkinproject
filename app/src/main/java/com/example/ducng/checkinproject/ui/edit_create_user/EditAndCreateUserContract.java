package com.example.ducng.checkinproject.ui.edit_create_user;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.util.mvp.IBasePresenter;
import com.example.ducng.checkinproject.util.mvp.IBaseView;

interface EditAndCreateUserContract {
    interface View extends IBaseView {
        void setupViewWithUser(User user);
        void startActivity(Intent intent,int requestCode);
        void startAct(Intent intent);
        void setImageUser(String url);
        void setImageUser(Uri uri);
        void setLinkAvatar(String link);
        boolean allowStoragePermissions(String[] permissions,int requestCode);
        void setDayOfBirth(String date);
        void setGender(String gender);
        void clearAll();
        void setAvatarHome(String link);

    }
    interface Presenter extends IBasePresenter<View> {
        void getUser(Context context);
        void updateUser(User user,Context context);
        void createUser(User user,Context context);
        void onActivityResult(int requestCode, int resultCode, Intent data,Context context);
        void changeAvatar(final Context context);
        void showTimePicker(Context context);
        void showGenderPicker(Context context);
        void sendEmail(String email,String defaultPassword);
    }
}
