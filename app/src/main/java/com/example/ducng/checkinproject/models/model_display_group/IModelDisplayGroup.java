package com.example.ducng.checkinproject.models.model_display_group;

import com.example.ducng.checkinproject.ui.mvp_display_group.IGetGroupFromFirebase;

public interface IModelDisplayGroup {
    void loadGroupFormFirebase(IGetGroupFromFirebase iGetGroupFromFirebase);
    void loadGrouFromFirebaseUseEventBus();
}
