package com.example.ducng.checkinproject.data.model;

public class User {
    private String id;
    private String name;
    private  String dob;
    private boolean gender;
    private String device;
    private  String cmt;
    private String image;
    private String email;
    private  String phone;
    private String groupId;
   private String defaultPassword;

    private int priovity;
    /**
        0: Nhân viên
        1: Trưởng nhóm
        2: Quản lý
     */
    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }


    public User() {
    }

    public User(String id, String name, String dob, boolean gender, String device, String cmt, String email, String phone, String groupId, int priovity) {
        this.id = id;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.device = device;
        this.cmt = cmt;
        this.email = email;
        this.phone = phone;
        this.groupId = groupId;
        this.priovity = priovity;
    }

    public User(String id, String name, String device, String cmt, String email) {
        this.id = id;
        this.name = name;
        this.device = device;
        this.cmt = cmt;
        this.email = email;
    }
    public String getName() {
        if (name == null) {
            return "";
        } else {
            return name;
        }

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        if (dob == null) {
            return "";
        } else {
            return dob;
        }
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDevice() {
        if (device == null) {
            return "";
        } else {
            return device;
        }
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getCmt() {
        if (cmt == null) {
            return "";
        } else {
            return cmt;
        }
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getImage() {
        if (image == null) {
            return "";
        } else {
            return image;
        }

    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        if (email == null) {
            return "";
        } else {
            return email;
        }

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        if (phone == null) {
            return "";
        } else {
            return phone;
        }
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGroupId() {
        if (groupId == null) {
            return "";
        } else {
            return groupId;
        }

    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }



    public int getPriovity() {
        return priovity;
    }

    public void setPriovity(int priovity) {
        this.priovity = priovity;
    }
}
