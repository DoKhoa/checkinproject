package com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.home.manager.SendUserLogin;
import com.example.ducng.checkinproject.ui.home.manager.adapter.UserAdapter;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.EventGetCheckInSuccess;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.EventGetCheckinFailt;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.MemberCheckInDetailFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.SendingUserClick;
import com.example.ducng.checkinproject.util.NetworkHelper;
import com.example.ducng.checkinproject.util.mvp.IBaseView;
import com.example.ducng.checkinproject.util.mvp.manager.request_list_member.IPresenterRequestListMember;
import com.example.ducng.checkinproject.util.mvp.manager.request_list_member.PresenterRequestListMember;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class ManagerFragment extends Fragment{
    public static final String ITEM_USER_CLICK = "item_user_click";
    private View rootView;
    private GridView grvUsers;
    private Context context;
    private IPresenterRequestListMember iPresenterRequestListMember;
    private ProgressDialog progressWaiting;
    private User userLogIn;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe( sticky = true,threadMode = ThreadMode.MAIN)
    public void getUserLogin(SendUserLogin sendUserLogin) {
        Log.d("gfh", "getUserLogin: ");
        if (sendUserLogin != null) {
            this.userLogIn = sendUserLogin.getUser();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCheckinSuccess(EventGetMemberSuccess getMemberSuccess){
        if (getMemberSuccess!=null) {
            final List<User> list =getMemberSuccess.getUsers();
            UserAdapter userAdapter = new UserAdapter(context, list);
            grvUsers.setAdapter(userAdapter);
            progressWaiting.dismiss();
            grvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
                    EventBus.getDefault().postSticky(new SendingUserClick(list.get(i),1));
                    getFragmentManager().beginTransaction()
                            .add(R.id.frament_in_frament_home_manager,
                                    memberCheckInDetailFragment,
                                    HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
                            .show(memberCheckInDetailFragment)
                            .commit();
                }
            });
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCheckinFailt(EventGetMemberFailt getMemberFailt) {
        if (getMemberFailt!=null) {
            progressWaiting.dismiss();
            Toast.makeText(context, "Danh sách nhân viên trống", Toast.LENGTH_SHORT).show();
        }
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_manager_thanh, container, false);
        context = container.getContext();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressWaiting = new ProgressDialog(context);
        progressWaiting.setMessage("Xin hãy đợi...");
        inits();
    }

    private void inits() {
        grvUsers = rootView.findViewById(R.id.grview_in_frament_home_group);

        progressWaiting.show();
        if (userLogIn != null) {
            if (userLogIn.getGroupId() != null && userLogIn.getPriovity() == 1) {
                NetworkHelper networkHelper = new NetworkHelper();
                if (!networkHelper.isNetworkAvailable(context)) {
                    progressWaiting.dismiss();
                    Toast.makeText(context, "Kết nối Internet để tiếp tục sử dụng!", Toast.LENGTH_LONG).show();
                    return;
                }
                progressWaiting.hide();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        iPresenterRequestListMember = new PresenterRequestListMember();
                        iPresenterRequestListMember.requestListMember(userLogIn.getGroupId());
                    }
                }).start();
            } else {
                progressWaiting.dismiss();
                Toast.makeText(context, "Bạn không phải trưởng nhớm, nên không thể sử dụng tính năng này!", Toast.LENGTH_SHORT).show();
            }
        } else {
            progressWaiting.dismiss();
            Log.d("gfh", "inits: ");
            Toast.makeText(context, "Bạn không phải trưởng nhớm, nên không thể sử dụng tính năng này!", Toast.LENGTH_SHORT).show();
        }
    }

}
