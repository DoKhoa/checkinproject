package com.example.ducng.checkinproject;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.ducng.checkinproject.data.model.Notification;
import com.example.ducng.checkinproject.ui.home.HomeActivity;
import com.example.ducng.checkinproject.ui.login.LoginActivity;
import com.example.ducng.checkinproject.ui.notification.EventNotification;
import com.example.ducng.checkinproject.ui.notification.MyResultReceiver;
import com.example.ducng.checkinproject.ui.notification.MyService;
import com.example.ducng.checkinproject.util.Constaint;
import com.example.ducng.checkinproject.util.Utils;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class BaseActivity extends AppCompatActivity  implements MyResultReceiver.Receiver {
    public MyResultReceiver mReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set up for result receiver
        mReceiver = new MyResultReceiver(new Handler());
        mReceiver.setReceiver(this);
        //
    }

    public void setupUI(final View view) {
        if (!(view instanceof EditText) && !(view instanceof Button) && !(view instanceof ImageButton)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(final View view, final MotionEvent motionEvent) {
                    hideSoftKeyboard(view);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); ++i) {
                this.setupUI(((ViewGroup) view).getChildAt(i));
            }
        }
    }
    public void hideSoftKeyboard(final View view) {
        ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public void hideSoftKeyboard() {
        ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(this.getWindow().getDecorView().findViewById(android.R.id.content).getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        if (this instanceof HomeActivity) {
            hideSoftKeyboard(this.getWindow().getDecorView().findViewById(android.R.id.content));
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.common_app_finish));
            builder.setPositiveButton(getString(R.string.common_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ActivityCompat.finishAffinity(BaseActivity.this);
                }
            });

            builder.setNegativeButton(getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.setCancelable(true);
            builder.show();

        } else {
            hideSoftKeyboard(this.getWindow().getDecorView().findViewById(android.R.id.content));
            super.onBackPressed();
        }
    }
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == MyService.RESULT_CODE_NOTIFICATION) {
            Notification notification = Utils.convertGsonToObject(resultData.getString(MyService.NOTIFICATION),Notification.class);
            String key = resultData.getString(MyService.KEY_NOTIFICATION);
            setNotification(notification.getTitle(),notification.getBody());
            FirebaseDatabase.getInstance().getReference().child("notifications").child(notification.getIdUser()).child(key).child("show").setValue(true);
        }
    }

    private void setNotification(String title,String body) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(Constaint.OPEN_FROM_NOTIFICATION,Constaint.OPEN_FROM_NOTIFICATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_stat_ic_notification)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventNotification notification) {
        setNotification(notification.getTitle(),notification.getBody());
        FirebaseDatabase.getInstance().getReference().child("notifications").child(notification.getIdUser()).child(notification.getKey()).child("show").setValue(true);
    }
}
