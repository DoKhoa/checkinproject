package com.example.ducng.checkinproject.util.mvp.manager.request_list_member;

public interface IPresenterRequestListMember {
    void requestListMember(String groupId);
}
