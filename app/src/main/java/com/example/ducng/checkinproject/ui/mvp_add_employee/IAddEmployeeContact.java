package com.example.ducng.checkinproject.ui.mvp_add_employee;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IAddEmployeeContact {
    interface IAddEmployeeView {
        void displaySpinnerGroup(List<String> nameGroups);
        void displayAutoCompleteTVEmployee(List<User> employees);
        void addSucceed();
        void updateSucceed();
        void showToast(String str);
    }

    interface IAddEmployeePresenter{
        void addAllNameGroup();
        void getAllUserWithoutGroup();
        void addEmployeeToGroup(User user, String groupId, int priovity);
        void updateLead(User user, String groupId);
    }
}
