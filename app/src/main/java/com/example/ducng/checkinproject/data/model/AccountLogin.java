package com.example.ducng.checkinproject.data.model;

public class AccountLogin {
    private String id;

    public AccountLogin() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public int getPriovity() {
        return priovity;
    }

    public void setPriovity(int priovity) {
        this.priovity = priovity;
    }

    public AccountLogin(String email, String defaultPassword) {

    }

    public AccountLogin(String id, String defaultPassword, String name, String dob, boolean gender, String device, String cmt, String image, String email, String phone, String groupId, int priovity) {

        this.id = id;
        this.defaultPassword = defaultPassword;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.device = device;
        this.cmt = cmt;
        this.image = image;
        this.email = email;
        this.phone = phone;
        this.groupId = groupId;
        this.priovity = priovity;
    }

    private String defaultPassword;
    private String name;
    private String dob;
    private boolean gender;
    private String device;
    private String cmt;
    private String image;
    private String email;
    private String phone;
    private String groupId;
    private int priovity;
}
