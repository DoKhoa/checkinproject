package com.example.ducng.checkinproject.ui.home.checkin;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.util.mvp.IBasePresenter;
import com.example.ducng.checkinproject.util.mvp.IBaseView;

interface ICheckInContract {
    interface View extends IBaseView{
        void showInputReason();
        void hideInputReason();
        void setReason(String reason);
        void showWorkStatus(int status);
        void setUpViewWithCheckIn(CheckIn checkIn);
        void showReasonError();
    }
    interface Presenter extends IBasePresenter<View>{
        void getCheckIn(String uid,int day);
        void setWorkStatus(int status);
        void updateCheckIn(String uid,int pickedDate,int status,String reason);
    }
}
