package com.example.ducng.checkinproject.ui.mvp_employee_all_manager;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_employee_all.EventEmployees;
import com.example.ducng.checkinproject.models.model_employee_all.EventEmployeesMessage;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.MemberCheckInDetailFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.SendingUserClick;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.IListenClickOkInDialog;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.TransferGroupDialog;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeAllManagerFragment extends Fragment
        implements IEmployeeAllManagerContact.IEmployeeAllManagerView, IListenClickOkInDialog {
    List<User> users;
    IEmployeeAllManagerContact.IEmployeeAllManagerPresenter presenter;
    GridViewAdapter adapter;
    TransferGroupDialog dialog;
    @BindView(R.id.gv_employee_all)
    GridView gvEmployeeAll;
    @BindView(R.id.progress_bar_all_employee)
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_all, container, false);
        ButterKnife.bind(this, view);
        inits();
        return view;
    }


//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        Log.d("fgh", "onViewCreated: ");
//
//    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    private void inits() {
        users = new ArrayList<User>();
        adapter = new GridViewAdapter(users, getContext());
        gvEmployeeAll.setAdapter(adapter);
        presenter = new EmployeeAllManagerPresenter(this);
        presenter.getAllEmployee();
        progressBar.setVisibility(View.VISIBLE);
        dialog = new TransferGroupDialog();
        dialog.setOnClick(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventEmployees(EventEmployees eventEmployees){
        progressBar.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        this.users.clear();
        this.users.addAll(eventEmployees.getUsers());
        adapter.notifyDataSetChanged();
        gvEmployeeAll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User user = users.get(i);
//                Bundle bundle = new Bundle();
//                Gson gson = new Gson();
//                String userString = gson.toJson(user);
//                bundle.putString(ManagerFragment.ITEM_USER_CLICK, userString);
//                EventBus.getDefault().post(user);
                EventBus.getDefault().postSticky(new SendingUserClick(user,1));
                MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
                getFragmentManager().beginTransaction()
                        .add(R.id.content_manager,
                                memberCheckInDetailFragment, HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
                        .show(memberCheckInDetailFragment)
                        .commit();
//                memberCheckInDetailFragment.setArguments(bundle);
            }
        });
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventEmployeesMessage(EventEmployeesMessage message){
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void loadSucceed(final List<User> users) {
//        progressBar.setVisibility(View.GONE);
//        this.users.clear();
//        this.users.addAll(users);
//        adapter.notifyDataSetChanged();
//        gvEmployeeAll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                User user = users.get(i);
////                Bundle bundle = new Bundle();
////                Gson gson = new Gson();
////                String userString = gson.toJson(user);
////                bundle.putString(ManagerFragment.ITEM_USER_CLICK, userString);
////                EventBus.getDefault().post(user);
//                EventBus.getDefault().postSticky(new SendingUserClick(user,1));
//                MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
//                getFragmentManager().beginTransaction()
//                        .add(R.id.content_manager,
//                                memberCheckInDetailFragment, HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
//                        .show(memberCheckInDetailFragment)
//                        .commit();
////                memberCheckInDetailFragment.setArguments(bundle);
//            }
//        });
    }

    @Override
    public void loadFailue() {
//        progressBar.setVisibility(View.GONE);
//        Toast.makeText(getContext(), "Không có nhân viên nào để hiển thị", Toast.LENGTH_LONG).show();
    }

    private void openTransferGroupDialog(DialogFragment dialogFragment) {
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        dialogFragment.show(fm, "Open Dialog Transfer Group");
    }

    @Override
    public void click() {
        presenter.getAllEmployee();
        adapter.notifyDataSetChanged();
    }
}
