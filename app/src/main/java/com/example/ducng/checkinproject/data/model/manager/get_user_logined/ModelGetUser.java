package com.example.ducng.checkinproject.data.model.manager.get_user_logined;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.EventCallbackUserLogInSuccess;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.EventCallbackUserLoginFailt;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

public class ModelGetUser implements IModelRequestUser {

    private static final String TAG = "ModelGetUser";

    @Override
    public void getUser() {
        FirebaseUser firebaseUse = FirebaseAuth.getInstance().getCurrentUser();
        String useId = FirebaseAuth.getInstance().getUid();
        if (firebaseUse != null) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
            reference.child("users").child(useId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        Log.d("shsd", "onDataChange: ");
                        EventBus.getDefault().postSticky(new EventCallbackUserLogInSuccess(user));
                    } else
                        EventBus.getDefault().post(new EventCallbackUserLoginFailt("Get null"));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("shsd", "onCancelled: ");
                    EventBus.getDefault().post(new EventCallbackUserLoginFailt("Get null"));
                }
            });
        } else
            EventBus.getDefault().post(new EventCallbackUserLoginFailt("Get null"));


    }
}
