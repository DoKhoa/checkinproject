package com.example.ducng.checkinproject.models.model_display_group;

import com.example.ducng.checkinproject.data.model.Group;

import java.util.List;

public class EventGroups {
    List<Group> groups;

    public EventGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
