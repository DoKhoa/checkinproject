package com.example.ducng.checkinproject.ui.home.checkin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;

import com.example.ducng.checkinproject.data.database.checkindatabase.CheckInDataRepository;
import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.data.model.EventMessage;
import com.example.ducng.checkinproject.util.Utils;
import com.example.ducng.checkinproject.util.mvp.BaseView;
import com.google.firebase.auth.FirebaseAuth;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckInFragment extends BaseView implements ICheckInContract.View {
    @BindView(R.id.imv_morning_work)
    ImageView morningWork;
    @BindView(R.id.imv_afternoon_work)
    ImageView afternoonWork;
    @BindView(R.id.imv_full_work)
    ImageView fullWork;
    @BindView(R.id.imv_absent)
    ImageView absent;
    @BindView(R.id.reason_layout)
    LinearLayout reasonLayout;
    @BindView(R.id.btn_ok)
    TextView btnOK;
    @BindView(R.id.date_spinner)
    Spinner spinner;
    @BindView(R.id.edt_reason)
    EditText edtReason;
    private ICheckInContract.Presenter presenter;
    private int selectedDate;
    private int status;
    public CheckInFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckInDataRepository checkInDataRepository = CheckInDataRepository.getInstance();
        presenter = new CheckInPresenter(this,checkInDataRepository);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_in, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetCheckIn(EventCheckin eventCheckin) {
        setProgressBar(false);
        if (eventCheckin.getKey().equals(EventCheckin.KEY_GET)) {
            CheckIn checkIn = eventCheckin.getCheckIn();
            setUpViewWithCheckIn(checkIn);
        } else if (eventCheckin.getKey().equals(EventCheckin.KEY_GET_ERROR)) {
            CheckIn checkIn = new CheckIn(eventCheckin.getUid(), Utils.getDate(eventCheckin.getDay()),0);
            setUpViewWithCheckIn(checkIn);
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMessage(EventMessage eventMessage) {
        setProgressBar(false);
        showToastMessage(eventMessage.getMessage());
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        selectedDate=0;
        loadCheckIn();
        String[] day = getResources().getStringArray(R.array.day_of_week);
        int begin = Utils.getDayOfWeek();
        List<String> listDay = new ArrayList<>(Arrays.asList(day).subList(begin, begin + 7));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),android.R.layout.simple_spinner_item,listDay);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setReason("");
                selectedDate=i;
                loadCheckIn();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick({R.id.imv_absent, R.id.imv_morning_work, R.id.imv_afternoon_work, R.id.imv_full_work})
    public void selectWorkStatus(View view) {
        switch (view.getId()) {
            case R.id.imv_absent:
                status=0;
                presenter.setWorkStatus(0);
                break;
            case R.id.imv_morning_work:
                status=1;
                presenter.setWorkStatus(1);
                break;
            case R.id.imv_afternoon_work:
                status=2;
                presenter.setWorkStatus(2);
                break;
            case R.id.imv_full_work:
                status=3;
                presenter.setWorkStatus(3);
                break;
            default:
                break;
        }
    }
    @OnClick(R.id.btn_ok)
    public void updateCheckIn(){
        presenter.updateCheckIn(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid(),selectedDate,status,edtReason.getText().toString());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onViewInactive();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onViewActive(this);
    }

    @Override
    public void showInputReason() {
        reasonLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideInputReason() {
        reasonLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setReason(String reason) {
        if (reason!=null){
            edtReason.setText(reason);
        }
    }


    @Override
    public void showWorkStatus(int status) {
        if (status<3){
            showInputReason();
        }else {
            edtReason.setText("");
            hideInputReason();
        }
        switch (status) {
            case 0:
                morningWork.setImageResource(R.drawable.morning_gray);
                afternoonWork.setImageResource(R.drawable.afternoon_gray);
                fullWork.setImageResource(R.drawable.full_gray);
                absent.setImageResource(R.drawable.absent_color);
                break;
            case 1:
                morningWork.setImageResource(R.drawable.morning_color);
                afternoonWork.setImageResource(R.drawable.afternoon_gray);
                fullWork.setImageResource(R.drawable.full_gray);
                absent.setImageResource(R.drawable.absent_gray);
                break;
            case 2:
                morningWork.setImageResource(R.drawable.morning_gray);
                afternoonWork.setImageResource(R.drawable.afternoon_color);
                fullWork.setImageResource(R.drawable.full_gray);
                absent.setImageResource(R.drawable.absent_gray);
                break;
            case 3:
                morningWork.setImageResource(R.drawable.morning_gray);
                afternoonWork.setImageResource(R.drawable.afternoon_gray);
                fullWork.setImageResource(R.drawable.full_color);
                absent.setImageResource(R.drawable.absent_gray);
                break;

        }
    }

    @Override
    public void setUpViewWithCheckIn(CheckIn checkIn) {
        status = checkIn.getStatus();
        showWorkStatus(checkIn.getStatus());
        if (checkIn.getStatus()<3){
            showInputReason();
            setReason(checkIn.getLyDo());
        }else {
            hideInputReason();
        }
    }

    @Override
    public void showReasonError() {
        edtReason.setError(getString(R.string.input_reason_request));
    }

    private void loadCheckIn(){
        presenter.getCheckIn(FirebaseAuth.getInstance().getUid(),selectedDate);
    }
}
