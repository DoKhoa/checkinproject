package com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member;

import com.example.ducng.checkinproject.data.model.CheckIn;

import java.util.List;

public class EventGetCheckInSuccess {
    private CheckIn checkInofNow;
    private List<CheckIn> checkIns;
    public EventGetCheckInSuccess(CheckIn checkInNow, List<CheckIn> checkIns){
        this.checkInofNow= checkInNow;
        this.checkIns=checkIns;
    }

    public CheckIn getCheckInofNow() {
        return checkInofNow;
    }

    public List<CheckIn> getCheckIns() {
        return checkIns;
    }
}
