package com.example.ducng.checkinproject.models.model_add_employee;

public class EventAddEmpoyeeMessage {
    private String message;
    private int target;
    // target 1: change group
    //2: update leader
    //3: update piority
    //private String managerId;

    public EventAddEmpoyeeMessage(String message, int target) {
        this.message = message;
        this.target = target;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

//    public String getManagerId() {
//        return managerId;
//    }
//
//    public void setManagerId(String managerId) {
//        this.managerId = managerId;
//    }
}