package com.example.ducng.checkinproject.data.model.manager.get_checkin_of_member;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ModelGetCheckinOfMenber implements IModelGetCheckinMember {
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private List<CheckIn>checkIns;

    public ModelGetCheckinOfMenber(){
        database = FirebaseDatabase.getInstance();
        checkIns= new ArrayList<>();
    }
    @Override
    public void getDataCheckininFribase(final IModelGetCheckinCallback iModelGetCheckinCallback, final String userId) {
        reference= database.getReference("checkin");
        reference.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    CheckIn tempCheckIn= snapshot.getValue(CheckIn.class);
                    CheckIn checkIn= new CheckIn();
                    if (tempCheckIn.getDateId()!=null){
                        checkIn.setDateId(tempCheckIn.getDateId());
                    }
                    if (tempCheckIn.getUserId()!=null){
                        checkIn.setUserId(tempCheckIn.getUserId());
                    }
                    checkIn.setStatus(tempCheckIn.getStatus());
                    checkIn.setLyDo(tempCheckIn.getLyDo());
                    checkIn.setNhanXet(tempCheckIn.getNhanXet());
                    checkIns.add(checkIn);
                }
                iModelGetCheckinCallback.getCheckinSuccess(userId,checkIns);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iModelGetCheckinCallback.getCheckinFaild();
            }
        });
    }
}
