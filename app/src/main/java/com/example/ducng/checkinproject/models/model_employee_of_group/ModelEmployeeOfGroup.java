package com.example.ducng.checkinproject.models.model_employee_of_group;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_employee_of_group.IModelEmployeeOfGroup;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.IGetEmployeeOfGroupFirebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ModelEmployeeOfGroup implements IModelEmployeeOfGroup {
    private DatabaseReference mData  = FirebaseDatabase.getInstance().getReference();
    @Override
    public void loadEmployeeOfGroup(final IGetEmployeeOfGroupFirebase iGetEmployeeOfGroupFirebase
            , final String groupId, final int i) {
        final List<User> users = new ArrayList<User>();
        mData.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    Log.e("key: ",dataSnapshot1.getKey()+"");
                    User user = dataSnapshot1.getValue(User.class);
                    if(user != null ){
                        {
                            if(user.getGroupId() != null){
                                if(user.getGroupId().equals(groupId)){
                                    users.add(user);
                                }
                            }
                        }

                    }
                }
                iGetEmployeeOfGroupFirebase.getEmployeeSucceed(users, i);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iGetEmployeeOfGroupFirebase.getEmployeeFailue();
            }
        });
    }

    @Override
    public void loadEmployeeOfGroupUseEventBus(final String groupId, final int i) {
        final List<User> users = new ArrayList<User>();
        mData.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    Log.e("key: ",dataSnapshot1.getKey()+"");
                    User user = dataSnapshot1.getValue(User.class);
                    if(user != null ){
                        {
                            if(user.getGroupId() != null){
                                if(user.getGroupId().equals(groupId)){
                                    users.add(user);
                                }
                            }
                        }

                    }
                }
                if(users != null){
                    List<User> userArrange = new ArrayList<User>();
                    if(users != null){
                        for(int index = 0; index < users.size(); index++){
                            if(users.get(index).getPriovity() == 1){
                                userArrange.add(users.get(index));
                                continue;
                            }
                        }
                        for(int index = 0; index < users.size(); index++){
                            if(users.get(index).getPriovity() != 1){
                                userArrange.add(users.get(index));
                                continue;
                            }
                        }
                    }
                    EventBus.getDefault().post(new EventEmployeOfGroup(userArrange, i));
                }else {
                    EventBus.getDefault().post(new EventEmployeeOfGroupMessage("Load nhan vien cua nhom that bai"));
                }
                //iGetEmployeeOfGroupFirebase.getEmployeeSucceed(users, i);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               // iGetEmployeeOfGroupFirebase.getEmployeeFailue();
                EventBus.getDefault().post(new EventEmployeeOfGroupMessage("Load nhan vien cua nhom that bai"));
            }
        });
    }
}
