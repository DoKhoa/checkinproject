package com.example.ducng.checkinproject.ui.mvp_display_group;


import com.example.ducng.checkinproject.models.model_display_group.IModelDisplayGroup;
import com.example.ducng.checkinproject.models.model_employee_of_group.IModelEmployeeOfGroup;
import com.example.ducng.checkinproject.models.model_display_group.ModelDisplayGroup;
import com.example.ducng.checkinproject.models.model_employee_of_group.ModelEmployeeOfGroup;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.IGetEmployeeOfGroupFirebase;

import java.util.ArrayList;
import java.util.List;

public class DisplayGroupPresenter implements IDisplayGroupContact.IDisplayPresenter,
        IGetGroupFromFirebase, IGetEmployeeOfGroupFirebase {
    private IDisplayGroupContact.IDisplayGroupView view;
    IModelDisplayGroup modelDisplayGroup;
    IModelEmployeeOfGroup modelEmployeeOfGroup;

    public DisplayGroupPresenter(IDisplayGroupContact.IDisplayGroupView view) {
        this.view = view;
        modelDisplayGroup = new ModelDisplayGroup();
        modelEmployeeOfGroup = new ModelEmployeeOfGroup();
    }

    @Override
    public void loadGroupFromFirebase() {
       //modelDisplayGroup.loadGroupFormFirebase(this);
        modelDisplayGroup.loadGrouFromFirebaseUseEventBus();
    }

    @Override
    public void loadUserOfGroupFromFirebase(String groupId, int i) {
        //modelEmployeeOfGroup.loadEmployeeOfGroup(this, groupId,i);
        modelEmployeeOfGroup.loadEmployeeOfGroupUseEventBus(groupId, i);
    }

    @Override
    public void getSucceed(List<Group> groups) {
        view.getGroupSucceed(groups);
    }

    @Override
    public void getFailue() {
        view.getGroupFailure();
    }

    @Override
    public void getEmployeeSucceed(List<User> users, int i) {
        List<User> userArrange = new ArrayList<User>();
        if(users != null){
            for(int index = 0; index < users.size(); index++){
                if(users.get(index).getPriovity() == 1){
                    userArrange.add(users.get(index));
                    continue;
                }
            }
            for(int index = 0; index < users.size(); index++){
                if(users.get(index).getPriovity() != 1){
                    userArrange.add(users.get(index));
                    continue;
                }
            }
        }
//        view.getUserOfGroupSucceed(users, i);
        view.getUserOfGroupSucceed(userArrange, i);
    }

    @Override
    public void getEmployeeFailue() {
        view.getUserOfGroupFailure();
    }
}
