package com.example.ducng.checkinproject.models.model_employee_all;

import android.support.annotation.NonNull;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_employee_all.IModelEmployeeAllManager;
import com.example.ducng.checkinproject.ui.mvp_employee_all_manager.IGetAllEmployeeFirebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ModelEmployeeAllManager implements IModelEmployeeAllManager {
    private DatabaseReference mData  = FirebaseDatabase.getInstance().getReference();

    @Override
    public void loadAllEmployee(final IGetAllEmployeeFirebase iGetAllEmployeeFirebase) {
        final List<User> users = new ArrayList<User>();
        mData.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    User user = dataSnapshot1.getValue(User.class);
                    if(user.getPriovity() !=2){
                        users.add(user);
                    }
                }
               iGetAllEmployeeFirebase.getAllEmployeeSucceed(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               iGetAllEmployeeFirebase.getAllEmployeeFailue();
            }
        });
    }

    @Override
    public void loadAllEmployeeUseEventBus() {
        final List<User> users = new ArrayList<User>();
        mData.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    User user = dataSnapshot1.getValue(User.class);
                    if(user.getPriovity() !=2){
                        users.add(user);
                    }
                }
                //iGetAllEmployeeFirebase.getAllEmployeeSucceed(users);
                EventBus.getDefault().post(new EventEmployees(users));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //iGetAllEmployeeFirebase.getAllEmployeeFailue();
                EventBus.getDefault().post(new EventEmployeesMessage("Không có nhân viên nào để hiển thị"));
            }
        });
    }
}
