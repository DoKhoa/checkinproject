package com.example.ducng.checkinproject.util.mvp.mvp_login;

import com.example.ducng.checkinproject.data.model.AccountLogin;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IModelLogin {
    void XuLyDuLieuLogin(ICallBack iCallBack);
    void DeleteValueAccount(String email);
    void AddUser(User user);

}
