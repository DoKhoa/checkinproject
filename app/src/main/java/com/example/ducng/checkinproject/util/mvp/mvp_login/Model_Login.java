package com.example.ducng.checkinproject.util.mvp.mvp_login;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.ducng.checkinproject.data.model.AccountLogin;
import com.example.ducng.checkinproject.data.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Model_Login implements IModelLogin{
    DatabaseReference mdata;
    List<AccountLogin> accountLoginList;


    public Model_Login() {
        mdata =FirebaseDatabase.getInstance().getReference("newaccounts");
        accountLoginList = new ArrayList<>();
    }

    @Override
    public void XuLyDuLieuLogin(final ICallBack iCallBack) {
        final List<AccountLogin> accountLogins = new ArrayList<>();
        final List<String> listKey = new ArrayList<>();
        mdata.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    AccountLogin accountLogin = dataSnapshot1.getValue(AccountLogin.class);
                    accountLogins.add(accountLogin);
                    listKey.add(dataSnapshot1.getKey());
                }
                iCallBack.OnSuccess(accountLogins,listKey);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void DeleteValueAccount(String key) {
        FirebaseDatabase.getInstance().getReference().child("newaccounts").child(key).setValue(null);
    }

    @Override
    public void AddUser(User user) {
        FirebaseDatabase.getInstance().getReference().child("users").child(user.getId()).setValue(user);
    }


}
