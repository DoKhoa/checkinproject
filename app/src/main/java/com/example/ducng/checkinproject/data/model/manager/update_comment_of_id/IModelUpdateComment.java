package com.example.ducng.checkinproject.data.model.manager.update_comment_of_id;

public interface IModelUpdateComment {
    void updateComment(IModelUpdateCommentCallback iModelUpdateCommentCallback, String userId, String dateCheckin, String comment);
}
