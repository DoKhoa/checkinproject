package com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IRequestCheckInOfManagerCallback {
    void respondCheckinSuccessful(CheckIn checkInNow,List<CheckIn> checkIns);
    void respondCheckinFalse();
}
