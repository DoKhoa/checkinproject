package com.example.ducng.checkinproject.util;

import android.support.annotation.NonNull;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class DummyData {
    public static void dummyData() {

        FirebaseAuth.getInstance().createUserWithEmailAndPassword("testQuanLy@gmail.com","12345678").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userQuanLy = new User(task.getResult().getUser().getUid(),"Quan ly","11/12/1996",true,"iphone","123456789","testQuanLy@gmail.com","0973926028","",2);
                    FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(userQuanLy);
                }
            }
        });
        FirebaseAuth.getInstance().createUserWithEmailAndPassword("testTruongNhom@gmail.com","12345678").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userQuanLy = new User(task.getResult().getUser().getUid(),"Truong Nhom","11/12/1996",true,"iphone","123456789","testTruongNhom@gmail.com","0973926028","GroupTest",1);
                    FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(userQuanLy);
                    Group group = new Group();
                    group.setId("GroupTest");
                    group.setManagerId(task.getResult().getUser().getUid());
                    group.setName("GroupTest");
                    group.setNumMem(0);
                    FirebaseDatabase.getInstance().getReference().child("groups").child("GroupTest").setValue(group);
                }
            }
        });
        FirebaseAuth.getInstance().createUserWithEmailAndPassword("testNhanVien@gmail.com","12345678").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userQuanLy = new User(task.getResult().getUser().getUid(),"Nhan vien","11/12/1996",true,"iphone","123456789","testNhanVien@gmail.com","0973926028","GroupTest",0);
                    FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(userQuanLy);

                }
            }
        });
    }
    public static void addNhanVien() {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword("testNhanVien1@gmail.com","12345678").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userQuanLy = new User(task.getResult().getUser().getUid(),"Nhan vien 1","11/12/1996",true,"iphone","123456789","testNhanVien1@gmail.com","0973926028","GroupTest",0);
                    FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(userQuanLy);

                }
            }
        });
        FirebaseAuth.getInstance().createUserWithEmailAndPassword("testNhanVien2@gmail.com","12345678").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userQuanLy = new User(task.getResult().getUser().getUid(),"Nhan vien 2","11/12/1996",true,"iphone","123456789","testNhanVien1@gmail.com","0973926028","GroupTest",0);
                    FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(userQuanLy);

                }
            }
        });
        FirebaseAuth.getInstance().createUserWithEmailAndPassword("testNhanVien3@gmail.com","12345678").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User userQuanLy = new User(task.getResult().getUser().getUid(),"Nhan vien 3","11/12/1996",true,"iphone","123456789","testNhanVien1@gmail.com","0973926028","GroupTest",0);
                    FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(userQuanLy);

                }
            }
        });
    }
}
