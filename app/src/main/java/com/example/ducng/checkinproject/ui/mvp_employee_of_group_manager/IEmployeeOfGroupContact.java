package com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IEmployeeOfGroupContact {
    interface IEmployeeOfGroupView{
        void loadEmployeeOfGroupSucceed(List<User> users);
        void loadEmployeeOfGroupFailue();
    }
    interface IEmployeeOfGroupPresenter{
        void loadEmployeeOfGroup(String groupId, int i);
    }
}
