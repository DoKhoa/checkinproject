package com.example.ducng.checkinproject.util.mvp;

public interface IBasePresenter<ViewT> {
    void onViewActive(ViewT view);

    void onViewInactive();
}
