package com.example.ducng.checkinproject.ui.mvp_display_group;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.models.GroupExhance;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.IGetEmployeeOfGroupFirebase;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupRecyclerAdapter extends RecyclerView.Adapter<GroupRecyclerAdapter.GroupRecyclerViewHolder>
        implements UserOffGroupRecyclerAdapter.IUserOffGroupRecyclerAdapter {


    private IGroupRecyclerAdapter mGroupRecyclerAdapter;
    private List<GroupExhance> groups;
   // private List<Group>groups;

    public GroupRecyclerAdapter(IGroupRecyclerAdapter mGroupRecyclerAdapter, List<GroupExhance> groups) {
        this.mGroupRecyclerAdapter = mGroupRecyclerAdapter;
        this.groups = groups;
       // users = new ArrayList<>();
    }

    @NonNull
    @Override
    public GroupRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.custom_item_group, viewGroup, false);

        GroupRecyclerViewHolder groupRecyclerViewHolder = new GroupRecyclerViewHolder(view);
        return groupRecyclerViewHolder;
    }
//    List<User> users;
//    public void setListUser(List<User> user) {
//        this.users.clear();
//        this.users.addAll(user);
//        notifyDataSetChanged();
//    }
    @Override
    public void onBindViewHolder(@NonNull GroupRecyclerViewHolder groupRecyclerViewHolder, final int position) {
//        Group group = groups.get(position);
//        users = mGroupRecyclerAdapter.getUsersOfGroup(position);
        //List<User> users = group.getUsers();

        final GroupExhance groupExhance = groups.get(position);
        List<User> users = groupExhance.getUsers();
        String nameUser = "";
        if(users.size() <= 3){
            for(int i = 0; i < users.size()-1; i++){
                nameUser= nameUser + users.get(i).getName() +  ", ";
            }
            nameUser = nameUser + users.get(users.size()-1).getName() ;
        }else {
            for(int i = 0; i < 3; i++){
                nameUser= nameUser + users.get(i).getName() +  ", ";
            }
            nameUser += "và " + (users.size() - 3) + " người khác";
        }

        groupRecyclerViewHolder.tvDecriptionGroup.setText(groupExhance.getGroup().getId() + ": " + nameUser);
        groupRecyclerViewHolder.cardViewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGroupRecyclerAdapter.onClickItemGroup(position);
//                EventBus.getDefault().post(groupExhance.getGroup());
            }
        });
        displayRecylerViewEmployee(groupRecyclerViewHolder, users);

    }

    private void displayRecylerViewEmployee(GroupRecyclerViewHolder groupRecyclerViewHolder, List<User> users) {
        if(users == null){
            groupRecyclerViewHolder.rcvItemGroup.setVisibility(View.INVISIBLE);
        }else {
            final UserOffGroupRecyclerAdapter adapter;
            if(users.size() <= 3){
                adapter = new UserOffGroupRecyclerAdapter(
                        this, users, users);
            }else {
                List<User> usersDisplays = new ArrayList<User>();
                for(int i = 0; i < 4; i++){
                    usersDisplays.add(users.get(i));
                }
                adapter = new UserOffGroupRecyclerAdapter(
                        this, usersDisplays, users);
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                    groupRecyclerViewHolder.rcvItemGroup.getContext(), LinearLayoutManager.HORIZONTAL, false
            );
            groupRecyclerViewHolder.rcvItemGroup.setLayoutManager(linearLayoutManager);
            groupRecyclerViewHolder.rcvItemGroup.setAdapter(adapter);
//            adapter.setLoadMore(new UserOffGroupRecyclerAdapter.LoadMore() {
//                @Override
//                public void loadMore() {
//                }
//            });
        }
    }

    @Override
    public int getItemCount() {
        if(groups == null){
            return 0;
        }else {
            return groups.size();
        }

    }

//    @Override
//    public void onClickToEmployee(User user) {
//        mGroupRecyclerAdapter.onClickItemEmployee(user);
//    }

    public static class GroupRecyclerViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.rcv_item_group)
        RecyclerView rcvItemGroup;
        @BindView(R.id.tv_name_number_group)
        TextView tvDecriptionGroup;
        @BindView(R.id.card_view_group)
        CardView cardViewGroup;
        public GroupRecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
//    public void addAll(List<Group> groups) {
//        int prevSize = getItemCount();
//        this.groups.addAll(groups);
//        notifyItemRangeInserted(prevSize, groups.size());
//    }

    public void addAll(List<GroupExhance> groups) {
        int prevSize = getItemCount();
//        this.groups.clear();
//        this.groups.addAll(groups);
        notifyItemRangeInserted(prevSize, this.groups.size());
    }

    interface IGroupRecyclerAdapter{
        //List<User> getUsersOfGroup(int position);
        void onClickItemGroup(int position);
       // void onClickItemEmployee(User user);
    }
}
