package com.example.ducng.checkinproject.ui.mvp_transfer_group;

import android.app.AlertDialog;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.Notification;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_add_employee.EventAddEmpoyeeMessage;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.MemberCheckInDetailFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.SendingUserClick;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.EmployeeOfGroupFragment;
import com.example.ducng.checkinproject.ui.notification.ActivityTestNotification;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransferGroupDialog extends DialogFragment
        implements View.OnClickListener, ITransferGroupContact.ITransferGroupView,
        AdapterView.OnItemSelectedListener {


    private static final String TAG = "TransferGroupDialog";
    User user;
    IListenClickOkInDialog mClick;

    ITransferGroupContact.ITransferGroupPresenter presenter;
    String groupIDTransfer;
    List<String> nameGroups;
    @BindView(R.id.img_avatar_tranfer_employee)
    ImageView imgAvatarTranfer;
    @BindView(R.id.tv_name_tranfer_employee)
    TextView tvNameTranfer;
    @BindView(R.id.tv_ob_tranfer_employee)
    TextView tvJobTranfer;
    @BindView(R.id.spinner_tranfer_group)
    Spinner spinner;
    @BindView(R.id.rd_tranfer_leader)
    RadioButton rdTranferLeader;
    @BindView(R.id.btn_view_work_history)
    Button btnViewWorkHistory;
    @BindView(R.id.btn_ok_tranfer)
    Button btnOKTranfer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_transfer_group, container, false);
        ButterKnife.bind(this, view);
        inits();
        //getEmployeeFromFragment();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        displayUI();
    }

    public void setOnClick(IListenClickOkInDialog mClick) {
        this.mClick = mClick;

    }

    private void inits() {
        nameGroups = new ArrayList<>();
        groupIDTransfer = "";
        presenter = new TransferGroupPresenter(this);
        presenter.loadAllNameGroup();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(SendUser sendUser) {
        if (sendUser != null) {
            this.user = sendUser.getUser();
            groupIDTransfer = user.getGroupId();
            EventBus.getDefault().removeStickyEvent(user);
        }
    }

//    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
//    public void onEventGetManagerIdChange(String managerId) {
//        if (managerId != null) {
//            this.managerId = managerId;
//        }
//    }

    private void displayUI() {
        tvNameTranfer.setText(user.getName());
        switch (user.getPriovity()) {
            case 0:
                tvJobTranfer.setText("Nhân viên");
                break;
            case 1:
                tvJobTranfer.setText("Trưởng nhóm");
                break;
            case 2:
                tvJobTranfer.setText("Quản lý");
                break;
        }
        if (!user.getImage().isEmpty()) {
            Picasso.get()
                    .load(user.getImage())
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.ic_error_black)
                    .into(imgAvatarTranfer);
        }
//        imgAvatarTranfer.setBackgroundResource(R.drawable.absent_color);
        if (user.getPriovity() == 1) {
            rdTranferLeader.setChecked(true);
        } else {
            rdTranferLeader.setChecked(false);
        }
        final boolean[] check = {rdTranferLeader.isChecked()};
        rdTranferLeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check[0]) {
                    rdTranferLeader.setChecked(false);
                    check[0] = false;
                } else {
                    rdTranferLeader.setChecked(true);
                    check[0] = true;
                }
            }
        });
        spinner.setOnItemSelectedListener(this);
        btnOKTranfer.setOnClickListener(this);
        btnViewWorkHistory.setOnClickListener(this);
        tvNameTranfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check[0]) {
                    rdTranferLeader.setChecked(false);
                    check[0] = false;
                } else {
                    rdTranferLeader.setChecked(true);
                    check[0] = true;
                }
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (user.getPriovity() == 1) {
                    rdTranferLeader.setChecked(true);
                } else {
                    rdTranferLeader.setChecked(false);
                }
            }
        }, 50);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_view_work_history:
//            Goi den dialog show lich su lam viec
//                Bundle bundle = new Bundle();
//                Gson gson = new Gson();
//                String userString = gson.toJson(user);
//                bundle.putString(ManagerFragment.ITEM_USER_CLICK, userString);
//                bundle.putInt(EmployeeOfGroupFragment.TYPE_TRANSFER,2)


//                Log.d(TAG,  user.getName());
                EventBus.getDefault().postSticky(new SendingUserClick(user, 2));
                MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
                getFragmentManager().beginTransaction()
                        .add(R.id.content_manager,
                                memberCheckInDetailFragment, HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
                        .show(memberCheckInDetailFragment)
                        .commit();
//                memberCheckInDetailFragment.setArguments(bundle);
                dismiss();


                //ben phia cua thanh
                break;
            case R.id.btn_ok_tranfer:
                if (user.getPriovity() == 1 /*&& groupIDTransfer.equals(user.getGroupId())*/) {
                    showDialog(user.getName());
                } else {
                    transferGroup(user, groupIDTransfer);
                }
                break;
        }
    }

    private void showDialog(String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(name + " dang la nhom truong cua nhom, khong the chuyen di");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    private void transferGroup(User user, String groupIDTransfer) {
        if (!groupIDTransfer.equals("") && user != null && user.getId() != null) {
            presenter.updateGroupOfEmployee(user, groupIDTransfer, rdTranferLeader.isChecked() ? 1 : 0);
        } else {
            Toast.makeText(getContext(), "Nhan vien hoac nhom chua duoc chon, hay bo sung", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void displaySpinner(List<String> nameGroups) {
        this.nameGroups = nameGroups;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, nameGroups);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        int selectedPosition = adapter.getPosition(user.getGroupId());
        spinner.setSelection(selectedPosition);
    }

    @Override
    public void showToast(String str) {
        Toast.makeText(getContext(), str, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateSucceed(String userId) {

        if(rdTranferLeader.isChecked()){
            Toast.makeText(getContext(), "Update thanh cong", Toast.LENGTH_LONG).show();
            presenter.updatePriorityOldLeader(userId);
        }else {
            mClick.click();
            this.dismiss();
        }

    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEventAddSucceed(EventAddEmpoyeeMessage message) {
//        if (message.getTarget() == 1) {
//            if (rdTranferLeader.isChecked()) {
//                Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_LONG).show();
//                presenter.changeLeadOfGroup(user, groupIDTransfer);
//            } else {
//                mClick.click();
//                sendNotification(user.getId(), groupIDTransfer, "Bạn đã được chuyển sang nhóm", new SendNotification() {
//                    @Override
//                    public void onSuccess() {
//                        Log.e(TAG, "send notification change group success");
//                    }
//
//                    @Override
//                    public void onFailure() {
//                        Log.e(TAG, "send notification change group failure");
//                    }
//
//                    @Override
//                    public void onNetworkFailure() {
//                        Log.e(TAG, "send notification change group failure network");
//                    }
//                });
//                this.dismiss();
//            }
//        } else if (message.getTarget() == 2) {
//            if (rdTranferLeader.isChecked()) {
//                presenter.updatePriorityOldLeader(managerId);
//            } else {
//                mClick.click();
//                this.dismiss();
//            }
//        } else if (message.getTarget() == 3) {
//            Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_LONG).show();
//            mClick.click();
//            sendNotification(user.getId(), groupIDTransfer, "Bạn đã được xét làm trưởng nhóm", new SendNotification() {
//                @Override
//                public void onSuccess() {
//                    Log.e(TAG, "send notification success");
//                }
//
//                @Override
//                public void onFailure() {
//                    Log.e(TAG, "send notification failure");
//                }
//
//                @Override
//                public void onNetworkFailure() {
//                    Log.e(TAG, "send notification failure network");
//                }
//            });
//            this.dismiss();
//        }
//
//    }

    @Override
    public void addSucceed() {
            if (rdTranferLeader.isChecked()) {
                Toast.makeText(getContext(), "Update thanh cong", Toast.LENGTH_LONG).show();
                presenter.changeLeadOfGroup(user, groupIDTransfer);
            } else {
                mClick.click();
                sendNotification(user.getId(), groupIDTransfer, "Bạn đã được chuyển sang nhóm", new SendNotification() {
                    @Override
                    public void onSuccess() {
                        Log.e(TAG, "send notification change group success");
                    }

                    @Override
                    public void onFailure() {
                        Log.e(TAG, "send notification change group failure");
                    }

                    @Override
                    public void onNetworkFailure() {
                        Log.e(TAG, "send notification change group failure network");
                    }
                });
                this.dismiss();
            }
    }

    @Override
    public void completeChangeSucceed() {
            Toast.makeText(getContext(), "Update thanh cong", Toast.LENGTH_LONG).show();
            mClick.click();
            sendNotification(user.getId(), groupIDTransfer, "Bạn đã được xét làm trưởng nhóm", new SendNotification() {
                @Override
                public void onSuccess() {
                    Log.e(TAG, "send notification success");
                }

                @Override
                public void onFailure() {
                    Log.e(TAG, "send notification failure");
                }

                @Override
                public void onNetworkFailure() {
                    Log.e(TAG, "send notification failure network");
                }
            });
            this.dismiss();
    }

    public void sendNotification(String idUser, String nameGroup, String body,
                                 final SendNotification callBack) {
        Notification notification = new Notification();
        notification.setIdUser(idUser);
        notification.setBody(body + " " + nameGroup);
        notification.setTitle("Thông báo");
        notification.setShow(false);
        FirebaseDatabase.getInstance().getReference().child("notifications").child(idUser)
                .push().setValue(notification).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callBack.onSuccess();
                } else {
                    callBack.onFailure();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callBack.onNetworkFailure();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        groupIDTransfer = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        groupIDTransfer = user.getGroupId();
    }

    @Override
    public void onDestroy() {
        // rdTranferLeader.setSelected(false);
        super.onDestroy();
    }

    public interface SendNotification {
        void onSuccess();

        void onFailure();

        void onNetworkFailure();

    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }
}
