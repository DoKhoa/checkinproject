package com.example.ducng.checkinproject.util;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.Calendar;
import java.util.Date;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Utils {
    public static String getDate(int pickedDate) {
        long today = Calendar.getInstance().getTimeInMillis();
        long date = today + 24 * 60 * 60 * 1000 * pickedDate;
        Date datePicked = new Date(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datePicked);
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        int calMon = calendar.get(Calendar.MONTH) + 1;
        String month = calMon < 10 ? "0" + calMon : String.valueOf(calMon);
        int calDay = calendar.get(Calendar.DAY_OF_MONTH);
        String day = calDay < 10 ? "0" + calDay : String.valueOf(calDay);
        return year + month + day;
    }

    public static int getDayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    public static final String URL_STORAGE_REFERENCE = "gs://checkinproject-bb6f9.appspot.com";
    public static final String FOLDER_STORAGE_IMG = "images";
    public static final String FOLDER_AVATAR_IMG = "images";

    public static String getSaltString() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-=/*-+[];',.";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isCMTValid(String cmt) {
        if (cmt.length() == 9 || cmt.length() == 12) {
            return true;
        } else {
            return false;
        }
    }

    public static String getIdUserCurrent() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public static <T> String convertObjectToGson(T object) {
        return new Gson().toJson(object);
    }

    public static <T> T convertGsonToObject(String object, Class<T> anonymousClass) {
        return new Gson().fromJson(object, anonymousClass);
    }
    public static void tranformFragment(Context context, int containerViewId, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerViewId,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public static void tranformFragmentNotAddToStack(Context context, int containerViewId,Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerViewId,fragment);
        fragmentTransaction.commit();
    }
}

