package com.example.ducng.checkinproject.models.model_display_group;

public class EventGroupMessage {
    private String message;

    public EventGroupMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
