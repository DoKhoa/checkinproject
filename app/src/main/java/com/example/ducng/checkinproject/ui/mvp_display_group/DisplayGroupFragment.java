package com.example.ducng.checkinproject.ui.mvp_display_group;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.models.GroupExhance;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_display_group.EventGroupMessage;
import com.example.ducng.checkinproject.models.model_display_group.EventGroups;
import com.example.ducng.checkinproject.models.model_employee_of_group.EventEmployeOfGroup;
import com.example.ducng.checkinproject.models.model_employee_of_group.EventEmployeeOfGroupMessage;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.MemberCheckInDetailFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.SendingUserClick;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_all_manager.EmployeeAllManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.EmployeeOfGroupFragment;
import com.example.ducng.checkinproject.util.mvp.BaseView;
import com.example.ducng.checkinproject.util.mvp.IBaseView;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DisplayGroupFragment extends Fragment implements
        IDisplayGroupContact.IDisplayGroupView,
        GroupRecyclerAdapter.IGroupRecyclerAdapter {
    EmployeeOfGroupFragment employeeOfGroupFragment;
    @BindView(R.id.rcv_group)
    RecyclerView rcvGroup;
    @BindView(R.id.progress_bar_group)
    ProgressBar progressBar;
    List<GroupExhance> groups;
    //List<Group>groups;
    GroupExhance groupExhance;
    List<User> userList;
    private GroupRecyclerAdapter groupRecyclerAdapter;
    private IDisplayGroupContact.IDisplayPresenter displayPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groups = new ArrayList<GroupExhance>();
        groupExhance = new GroupExhance();
        //groups = new ArrayList<Group>();
        userList = new ArrayList<User>();
        employeeOfGroupFragment = new EmployeeOfGroupFragment();
        displayPresenter = new DisplayGroupPresenter(this);
        displayPresenter.loadGroupFromFirebase();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_manager, container, false);
        ButterKnife.bind(this, view);
        progressBar.setVisibility(View.VISIBLE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        groupRecyclerAdapter = new GroupRecyclerAdapter(this, groups);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rcvGroup.setLayoutManager(linearLayoutManager);
        rcvGroup.setAdapter(groupRecyclerAdapter);
    }
//    @Override
//    public void getUsersOfGroup(int position) {
//        displayPresenter.loadUserOfGroupFromFirebase(groups.get(position).getGroup().getId(), position);
//        Log.e("check getUsersOfGroup",userList.size()+"");
////        return userList;
//    }

        @Override
    public void onClickItemGroup(int position) {
//        Bundle bundle = new Bundle();
//        bundle.putString("groupId",groups.get(position).getGroup().getId());
//        employeeOfGroupFragment.setArguments(bundle);
        EventBus.getDefault().postSticky(groups.get(position).getGroup());
        setFragment(employeeOfGroupFragment);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(User user) {
        if (user != null) {
//            Bundle bundle = new Bundle();
//            Gson gson = new Gson();
//            String userString = gson.toJson(user);
//            bundle.putString(ManagerFragment.ITEM_USER_CLICK, userString)
            EventBus.getDefault().postSticky(new SendingUserClick(user,1));
            MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.content_manager,
                            memberCheckInDetailFragment, HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
                    .show(memberCheckInDetailFragment)
                    .commit();
        }
    }
//    @Override
//    public void onClickItemEmployee(User user) {
//        if(user != null){
//            Bundle bundle = new Bundle();
//            Gson gson = new Gson();
//            String userString = gson.toJson(user);
//            bundle.putString(ManagerFragment.ITEM_USER_CLICK, userString);
//            bundle.putInt(EmployeeOfGroupFragment.TYPE_TRANSFER,1);
//            MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
//            getFragmentManager().beginTransaction()
//                    .add(R.id.content_manager,
//                            memberCheckInDetailFragment, HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
//                    .show(memberCheckInDetailFragment)
//                    .commit();
//            memberCheckInDetailFragment.setArguments(bundle);
//        }
//    }

    @Override
    public void getGroupSucceed(List<Group> groups) {
////        this.groups = groups;
//////        groupRecyclerAdapter = new GroupRecyclerAdapter(this,groups);
////////        groupRecyclerAdapter.notifyDataSetChanged();
//////        rcvGroup.setAdapter(groupRecyclerAdapter);
////        if(groups != null){
////            groupRecyclerAdapter.addAll(this.groups);
////        }
//        if (groups != null) {
//            for (int i = 0; i < groups.size(); i++) {
//                this.groups.add(new GroupExhance(groups.get(i)));
//                displayPresenter.loadUserOfGroupFromFirebase(groups.get(i).getId(), i);
//            }
//        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventGroups(EventGroups eventGroups) {
        if (eventGroups != null) {
            for (int i = 0; i < eventGroups.getGroups().size(); i++) {
                this.groups.add(new GroupExhance(eventGroups.getGroups().get(i)));
                displayPresenter.loadUserOfGroupFromFirebase(eventGroups.getGroups().get(i).getId(), i);
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventGroupsMessage(EventGroupMessage message){
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void getGroupFailure() {
//        progressBar.setVisibility(View.GONE);
//        Toast.makeText(getContext(), "Không có nhóm nào được hiển thị", Toast.LENGTH_LONG).show();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void getUserOfGroupSucceed(List<User> users, int i) {
//        //this.userList = users;
//        progressBar.setVisibility(View.GONE);
//        this.groups.get(i).setUsers(users);
//        groupRecyclerAdapter.addAll(groups);
////        groupRecyclerAdapter.setListUser(userList);
////        groupRecyclerAdapter.notifyDataSetChanged();
////        Log.e("check getUserOfGroupSucceed", String.valueOf(userList.size()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventEmployeeOfGroup(EventEmployeOfGroup eventEmployeOfGroup){
        progressBar.setVisibility(View.GONE);
        this.groups.get(eventEmployeOfGroup.getI()).setUsers(eventEmployeOfGroup.getUsers());
        groupRecyclerAdapter.addAll(groups);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventEmployeeOfGroupFailue(EventEmployeeOfGroupMessage message){
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void getUserOfGroupFailure() {
//        progressBar.setVisibility(View.GONE);
//        Toast.makeText(getContext(), "Không có nhân viên nào trong nhóm", Toast.LENGTH_LONG).show();
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        // replace
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.content_manager, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }
}
