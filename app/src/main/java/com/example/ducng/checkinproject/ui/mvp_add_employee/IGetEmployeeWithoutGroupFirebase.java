package com.example.ducng.checkinproject.ui.mvp_add_employee;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IGetEmployeeWithoutGroupFirebase {
    void getEmployeeWithoutGroupSucceed(List<User> users);
    void getEmployeeWithoutGroupFailue();
}
