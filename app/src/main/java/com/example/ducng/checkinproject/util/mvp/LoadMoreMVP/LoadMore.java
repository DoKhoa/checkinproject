package com.example.ducng.checkinproject.util.mvp.LoadMoreMVP;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class LoadMore extends RecyclerView.OnScrollListener {
    int tongItem = 0;
    int itemDaLoad = 0;
    int itemLoadTruoc = 5;
    IILoadMore iloadMore;

    public LoadMore(IILoadMore iloadMore) {
        this.iloadMore = iloadMore;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        tongItem = linearLayoutManager.getItemCount();
        itemDaLoad = linearLayoutManager.findFirstVisibleItemPosition();
        if(tongItem <= itemDaLoad + itemLoadTruoc){
            iloadMore.iloadMore(tongItem, itemDaLoad, itemLoadTruoc);
        }
    }

}