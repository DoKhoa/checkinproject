package com.example.ducng.checkinproject.data.model;

public class NewAccount {
    private String email;
    private String defaultPassword;

    public NewAccount() {
    }

    public NewAccount(String email, String defaultPassword) {
        this.email = email;
        this.defaultPassword = defaultPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }
}
