package com.example.ducng.checkinproject.ui.home.checkin;

import com.example.ducng.checkinproject.data.model.CheckIn;

public class EventCheckin {
    public static final String KEY_GET = "get";
    public static final String KEY_UPDATE = "update";
    public static final String KEY_GET_ERROR = "get_error";
    private CheckIn checkIn;
    private String key;
    private String uid;
    private int day;
    public EventCheckin(CheckIn checkIn, String key) {
        this.checkIn = checkIn;
        this.key = key;
    }

    public EventCheckin( String key, String uid, int day) {
        this.key = key;
        this.uid = uid;
        this.day = day;
    }

    public CheckIn getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(CheckIn checkIn) {
        this.checkIn = checkIn;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
