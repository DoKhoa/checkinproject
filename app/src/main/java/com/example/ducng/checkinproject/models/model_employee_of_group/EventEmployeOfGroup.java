package com.example.ducng.checkinproject.models.model_employee_of_group;

import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public class EventEmployeOfGroup {
    private List<User> users;
    private int i;

    public EventEmployeOfGroup(List<User> users) {
        this.users = users;
    }

    public EventEmployeOfGroup(List<User> users, int i) {
        this.users = users;
        this.i = i;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
