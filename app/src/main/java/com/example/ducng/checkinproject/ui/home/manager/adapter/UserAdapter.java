package com.example.ducng.checkinproject.ui.home.manager.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserAdapter extends BaseAdapter {
    private List<User> users;
    private Context context;
    private LayoutInflater inflater;

    public UserAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.d("hgsd", "getView:dfygdfh");
        ViewHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.item_users_manager, viewGroup, false);
            viewHolder = new ViewHolder();
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.imgAvata = view.findViewById(R.id.img_ava_in_item_user);
        if (!users.get(i).getImage().isEmpty()) {
            Picasso.get()
                    .load(users.get(i).getImage())
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.ic_error_black)
                    .into(viewHolder.imgAvata);
        } else {
            viewHolder.imgAvata.setBackgroundResource(R.drawable.frame_ava_in_check_in_detail);
        }
        viewHolder.txtUserName = view.findViewById(R.id.txt_user_name_in_item_user);
        String userName = users.get(i).getName();
        viewHolder.txtUserName.setText(userName);
        return view;
    }

    private class ViewHolder {
        private ImageView imgAvata;
        private TextView txtUserName;
    }
}
