package com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ducng.checkinproject.ui.home.checkin.CheckInFragment;
import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.home.manager.SendUserLogin;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.MemberCheckInDetailFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.login.LoginActivity;
import com.example.ducng.checkinproject.util.NetworkHelper;
import com.example.ducng.checkinproject.util.mvp.manager.request_user_logined.IPresenterRequestUserLogined;
import com.example.ducng.checkinproject.util.mvp.manager.request_user_logined.PresenterRequestUserLogined;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class HomeManagerFragment extends Fragment implements View.OnClickListener {
    public static final String USER_LOGIN = "user_login";
    public static final String FRAMENT_CHECK_IN = "frament_check_in";
    public static final String FRAMENT_LIST_MEMBER = "frament_list_member";
    public static final String FRAMENT_CHECKIN_MEMBER = "frament_checkin_member";
    private static final String TAG = "HomeManagerFragment";
    private Context context;
    private View rootView;
    private ManagerFragment managerFragment;
    private MemberCheckInDetailFragment memberCheckInDetailFragment;
    private ImageView btnCheckIn;
    private TextView txtCheckIn;
    private ImageView btnManager;
    private TextView txtManager;
    private ProgressDialog progressDialog;
    private User userLogin;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getUserLogin(SendUserLogin sendUserLogin) {
        if (sendUserLogin != null) {
            this.userLogin = sendUserLogin.getUser();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home_of_manager, container, false);
        this.context = container.getContext();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Lấy thông tin đăng nhập của người dùng
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Xin hãy đợi...");

        NetworkHelper networkHelper = new NetworkHelper();
        if (!networkHelper.isNetworkAvailable(context)) {
            Toast.makeText(context, "Kết nối Internet để tiếp tục sử dụng!", Toast.LENGTH_LONG).show();
            return;
        }
        init();

        //Mặc định show màn hình checkin
        showCheckInFragment();
    }

    private void init() {
        btnCheckIn = rootView.findViewById(R.id.btn_check_in_in_fragment_menu_manager);
        btnCheckIn.setOnClickListener(this);
        btnManager = rootView.findViewById(R.id.btn_manager_in_fragment_menu_manager);
        btnManager.setOnClickListener(this);
        txtCheckIn = rootView.findViewById(R.id.txt_check_in_in_fragment_menumanager);
        txtManager = rootView.findViewById(R.id.txt_manager_in_fragment_menumanager);
    }

    private void showCheckInFragment() {
        if (getFragmentManager().findFragmentByTag(FRAMENT_CHECK_IN) != null) {
            this.getActivity().getSupportFragmentManager().beginTransaction()
                    .show(getFragmentManager().findFragmentByTag(FRAMENT_CHECK_IN))
                    .commit();
            if (getFragmentManager().findFragmentByTag(FRAMENT_LIST_MEMBER) != null) {
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .hide(getFragmentManager().findFragmentByTag(FRAMENT_LIST_MEMBER))
                        .commit();
            }
            if (getFragmentManager().findFragmentByTag(FRAMENT_CHECKIN_MEMBER) != null) {
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .hide(getFragmentManager().findFragmentByTag(FRAMENT_CHECKIN_MEMBER))
                        .commit();
            }
        } else {
            CheckInFragment checkInFragment = new CheckInFragment();
            this.getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.frament_in_frament_home_manager, checkInFragment, FRAMENT_CHECK_IN)
                    .show(checkInFragment)
                    .commit();
        }
    }

    private void showManagerFragment() {

        //Show fragment manager
        if (getFragmentManager().findFragmentByTag(FRAMENT_LIST_MEMBER) != null) {
            this.getActivity().getSupportFragmentManager().beginTransaction()
                    .show(getFragmentManager().findFragmentByTag(FRAMENT_LIST_MEMBER))
                    .commit();

            //Ẩn fragment checkin
            if (getFragmentManager().findFragmentByTag(FRAMENT_CHECK_IN) != null) {
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .hide(getFragmentManager().findFragmentByTag(FRAMENT_CHECK_IN))
                        .commit();
            }

            //Ẩn fragment detail checkin 0f member
            if (getFragmentManager().findFragmentByTag(FRAMENT_CHECKIN_MEMBER) != null) {
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .hide(getFragmentManager().findFragmentByTag(FRAMENT_CHECKIN_MEMBER))
                        .commit();
            }
        } else {
            //Khi chưa có managerFragment tạo mới và truyền đối tượng userLogin
            managerFragment = new ManagerFragment();
//            Gson gson = new Gson();
//            String temp = gson.toJson(userLogin);
//            Bundle bundle = new Bundle();
//            bundle.putString(USER_LOGIN, temp);
//            managerFragment.setArguments(bundle);
            this.getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.frament_in_frament_home_manager, managerFragment, FRAMENT_LIST_MEMBER)
                    .show(managerFragment)
                    .commit();

            //Ẩn đi fragment checkin
            if (getFragmentManager().findFragmentByTag(FRAMENT_CHECK_IN) != null) {
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .hide(getFragmentManager().findFragmentByTag(FRAMENT_CHECK_IN))
                        .commit();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_check_in_in_fragment_menu_manager:
                showCheckInFragment();
                RelativeLayout relativeLayout = rootView.findViewById(R.id.relaLayout_check_in_in_manager);
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.grayButtonManager));
                btnCheckIn.setBackgroundResource(R.drawable.item_view_pager_color);
                btnManager.setBackgroundResource(R.drawable.item_view_pager_gray);
                txtManager.setTextColor(getResources().getColor(R.color.colorTextSelectFalse));
                txtCheckIn.setTextColor(getResources().getColor(R.color.colorTextSelectTrue));
                break;
            case R.id.btn_manager_in_fragment_menu_manager:
                RelativeLayout relativeLayout1 = rootView.findViewById(R.id.relaLayout_check_in_in_manager);
                relativeLayout1.setBackgroundColor(getResources().getColor(R.color.colorButtonManager));
                btnManager.setBackgroundResource(R.drawable.item_view_pager_color);
                btnCheckIn.setBackgroundResource(R.drawable.item_view_pager_gray);
                txtCheckIn.setTextColor(getResources().getColor(R.color.colorTextSelectFalse));
                txtManager.setTextColor(getResources().getColor(R.color.colorTextSelectTrue));
                showManagerFragment();
                break;
            default:
                break;
        }
    }
}

