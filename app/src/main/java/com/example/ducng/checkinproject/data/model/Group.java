package com.example.ducng.checkinproject.data.model;

public class Group {
    private String id;
    private String managerId;
    private int numMem;
    private String name;

    public Group() {
    }

    public Group(String id, String managerId, String name) {
        this.id = id;
        this.managerId = managerId;
        this.name = name;
        this.numMem=0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public int getNumMem() {
        return numMem;
    }

    public void setNumMem(int numMem) {
        this.numMem = numMem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
