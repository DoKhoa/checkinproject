package com.example.ducng.checkinproject.models.model_display_group;

import android.support.annotation.NonNull;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_display_group.IModelDisplayGroup;
import com.example.ducng.checkinproject.ui.mvp_display_group.IGetGroupFromFirebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ModelDisplayGroup implements IModelDisplayGroup {
    private DatabaseReference mData  = FirebaseDatabase.getInstance().getReference();
    @Override
    public void loadGroupFormFirebase(final IGetGroupFromFirebase iGetGroupFromFirebase) {
        final List<Group> groups = new ArrayList<Group>();
        mData.child("groups").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    Group group = dataSnapshot1.getValue(Group.class);
                    groups.add(group);
                }
                iGetGroupFromFirebase.getSucceed(groups);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iGetGroupFromFirebase.getFailue();
            }
        });
    }

    @Override
    public void loadGrouFromFirebaseUseEventBus() {
        final List<Group> groups = new ArrayList<Group>();
        mData.child("groups").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    Group group = dataSnapshot1.getValue(Group.class);
                    groups.add(group);
                }
//                iGetGroupFromFirebase.getSucceed(groups);
                EventBus.getDefault().post(new EventGroups(groups));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               EventBus.getDefault().post(new EventGroupMessage("Load group that bai"));
            }
        });
    }

}
