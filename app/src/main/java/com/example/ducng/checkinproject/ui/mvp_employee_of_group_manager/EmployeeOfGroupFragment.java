package com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_employee_of_group.EventEmployeOfGroup;
import com.example.ducng.checkinproject.models.model_employee_of_group.EventEmployeeOfGroupMessage;
import com.example.ducng.checkinproject.ui.edit_create_user.EditAndCreateUserFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.MemberCheckInDetailFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.SendingUserClick;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_add_employee.AddEmployeeDialog;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IClickListenAddDialog;
import com.example.ducng.checkinproject.ui.mvp_common_employee_manager.CommonEmployeeManager;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.IListenClickOkInDialog;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.ITransferGroupContact;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.SendUser;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.TransferGroupDialog;
import com.example.ducng.checkinproject.util.Utils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeOfGroupFragment extends Fragment
        implements View.OnClickListener,
        IEmployeeOfGroupContact.IEmployeeOfGroupView,
        EmployeeOfGroupRecyclerAdapter.IEmployeeOfGroup ,
        IListenClickOkInDialog, IClickListenAddDialog{
    public static final String TYPE_TRANSFER= "type_transfer";

    CommonEmployeeManager commonEmployeeManager;
    TransferGroupDialog transferGroupDialog;
    AddEmployeeDialog addEmployeeDialog;

    List<User> users;
    EmployeeOfGroupRecyclerAdapter adapter;
    IEmployeeOfGroupContact.IEmployeeOfGroupPresenter presenter;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.tv_name_group)
    TextView tvNameGroup;
    @BindView(R.id.rcv_list_employee)
    RecyclerView rcvListEmployee;
    @BindView(R.id.img_add_employee)
    ImageView imgAddEmployee;
    @BindView(R.id.progress_bar_employee_of_group)
    ProgressBar progressBar;
    String groupId;
    String tempgroupId = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_manager, container, false);
        ButterKnife.bind(this, view);
        Bundle mBundle = new Bundle();
        mBundle = getArguments();

        if(mBundle != null){
            groupId = mBundle.getString("groupId");
        }
        tempgroupId=groupId;
        inits();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = true ,threadMode = ThreadMode.MAIN)
    public void onEvent(Group group) {
        if(group != null){
            groupId = group.getId();
            tempgroupId = groupId;
            inits();
            EventBus.getDefault().removeStickyEvent(group);
        }
    }

    private void inits() {
        commonEmployeeManager = new CommonEmployeeManager();
        transferGroupDialog = new TransferGroupDialog();
        transferGroupDialog.setStyle(DialogFragment.STYLE_NO_TITLE,0);
        transferGroupDialog.setOnClick(this);
        addEmployeeDialog = new AddEmployeeDialog();
        addEmployeeDialog.setOnClick(this);
        users = new ArrayList<User>();
        tvNameGroup.setText(groupId);
        presenter = new EmployeeOfGroupPresenter(this);
        presenter.loadEmployeeOfGroup(groupId, 0);
        progressBar.setVisibility(View.VISIBLE);
        adapter = new EmployeeOfGroupRecyclerAdapter( this, users);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rcvListEmployee.setLayoutManager(linearLayoutManager);
        rcvListEmployee.setAdapter(adapter);
        btnBack.setOnClickListener(this);
        imgAddEmployee.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                setFragment(commonEmployeeManager);
                break;
            case R.id.img_add_employee:
//                openTransferGroupDialog(addEmployeeDialog);
                openTransferCreateUser();
                break;
        }
    }

    private void openTransferCreateUser() {
        EditAndCreateUserFragment fragment = new EditAndCreateUserFragment(EditAndCreateUserFragment.TYPE_CREATE,groupId);
        Utils.tranformFragment(getActivity(),R.id.main_frame,fragment);
    }


    @Override
    public void loadEmployeeOfGroupSucceed(List<User> users) {
//        Log.d("fgh", "loadEmployeeOfGroupSucceed: ");
//        progressBar.setVisibility(View.GONE);
//        imgAddEmployee.setVisibility(View.VISIBLE);
//        this.users.clear();
//        this.users.addAll(users);
//        adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventEmployeeOfGroup(EventEmployeOfGroup eventEmployeOfGroup){
        progressBar.setVisibility(View.GONE);
        imgAddEmployee.setVisibility(View.VISIBLE);
        this.users.clear();
        this.users.addAll(eventEmployeOfGroup.getUsers());
        adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventEmployeeOfGroupFailue(EventEmployeeOfGroupMessage message){
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void loadEmployeeOfGroupFailue() {
//        progressBar.setVisibility(View.GONE);
//        Toast.makeText(getContext(), "Nhóm không có thành viên nào", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickEdit(int position) {
        User user = users.get(position);
//        Bundle bundle = new Bundle();
//        Gson gson= new Gson();
//        String userString = gson.toJson(user);
//        bundle.putString("user",userString);
//        transferGroupDialog.setArguments(bundle);
        EventBus.getDefault().postSticky(new SendUser(user));
        openTransferGroupDialog(transferGroupDialog);
        Log.d("fdh", "onClickEdit: ");
    }

    @Override
    public void onClickCardView(int position) {
        User user = users.get(position);
////        User user = users.get(i);
//        Bundle bundle = new Bundle();
//        Gson gson = new Gson();
//        String userString = gson.toJson(user);
//        bundle.putString(ManagerFragment.ITEM_USER_CLICK, userString);
//        bundle.putInt(TYPE_TRANSFER,1);
        Log.d("fdh", "onClickCardView: ");
        EventBus.getDefault().postSticky(new SendingUserClick(user,1));
        MemberCheckInDetailFragment memberCheckInDetailFragment = new MemberCheckInDetailFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.content_manager,
                        memberCheckInDetailFragment, HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
                .show(memberCheckInDetailFragment)
                .commit();
//        memberCheckInDetailFragment.setArguments(bundle);
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        // replace
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.content_manager, fragment);
        fragmentTransaction.commit();
    }

    private void openTransferGroupDialog(DialogFragment dialogFragment){
        FragmentManager  fm = this.getActivity().getSupportFragmentManager();
        dialogFragment.show(fm, "Open Dialog Add Employee");
    }

    @Override
    public void click() {
        Log.d("GroupId", ": " + groupId);
        presenter.loadEmployeeOfGroup(groupId,0);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDismissAddDialog() {
        presenter.loadEmployeeOfGroup(groupId,0);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }
}
