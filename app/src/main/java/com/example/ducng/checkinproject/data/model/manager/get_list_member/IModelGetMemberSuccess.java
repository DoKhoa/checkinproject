package com.example.ducng.checkinproject.data.model.manager.get_list_member;

import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IModelGetMemberSuccess {
    void respondSuccessful(List<User> users);
    void respondFalse();
}
