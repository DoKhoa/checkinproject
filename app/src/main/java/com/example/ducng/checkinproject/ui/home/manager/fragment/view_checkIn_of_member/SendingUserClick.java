package com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member;

import com.example.ducng.checkinproject.data.model.User;

public class SendingUserClick {
    int type;
    User user;
    public SendingUserClick(User user, int type){
        this.user=user;
        this.type=type;
    }

    public User getUser(){
        return user;
    }

    public int getType() {
        return type;
    }
}
