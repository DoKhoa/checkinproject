package com.example.ducng.checkinproject.data.model.manager.get_checkin_of_member;

public interface IModelGetCheckinMember {
    void getDataCheckininFribase(IModelGetCheckinCallback iModelGetCheckinCallback, String userId);
}
