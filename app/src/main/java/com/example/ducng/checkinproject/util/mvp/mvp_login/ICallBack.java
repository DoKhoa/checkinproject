package com.example.ducng.checkinproject.util.mvp.mvp_login;

import com.example.ducng.checkinproject.data.model.AccountLogin;

import java.util.List;

public interface ICallBack {
    void OnSuccess(List<AccountLogin> accountLogins,List<String> listKey);
}
