package com.example.ducng.checkinproject.ui.mvp_employee_all_manager;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IGetAllEmployeeFirebase {
    void getAllEmployeeSucceed(List<User> users);
    void getAllEmployeeFailue();

}
