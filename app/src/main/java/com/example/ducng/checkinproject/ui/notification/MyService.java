package com.example.ducng.checkinproject.ui.notification;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;

import com.example.ducng.checkinproject.data.model.Notification;
import com.example.ducng.checkinproject.util.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;

public class MyService extends Service {
    public static final String NOTIFICATION = "notification";
    public static final int RESULT_CODE_NOTIFICATION = 1;
    public static final String KEY_NOTIFICATION = "key_notification";
    ResultReceiver rec;
    ChildEventListener childEventListener;
    private String idUser;
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getParcelableExtra("receiverTag")!=null) {
            rec = intent.getParcelableExtra("receiverTag");
        }
        if (FirebaseAuth.getInstance().getCurrentUser()!=null) {
            idUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
            childEventListener = new ChildEventListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    if (dataSnapshot.getValue(Notification.class)!=null) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isShow()) {
                            /*use result receiver
                            Bundle bundle = new Bundle();
                            bundle.putString(NOTIFICATION, Utils.convertObjectToGson(notification));
                            bundle.putString(KEY_NOTIFICATION,dataSnapshot.getKey());
                            rec.send(RESULT_CODE_NOTIFICATION,bundle);
                            */
                            //use event bus
                            EventNotification eventNotification = new EventNotification();
                            eventNotification.setBody(notification.getBody());
                            eventNotification.setIdUser(notification.getIdUser());
                            eventNotification.setShow(notification.isShow());
                            eventNotification.setTitle(notification.getTitle());
                            eventNotification.setKey(dataSnapshot.getKey());
                            EventBus.getDefault().post(eventNotification);
                        }

                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
            FirebaseDatabase.getInstance().getReference().child("notifications").child(idUser)
                    .addChildEventListener(childEventListener);
        }

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (childEventListener!=null) {
            if (idUser!=null ){
                FirebaseDatabase.getInstance().getReference().child("notifications").child(idUser)
                        .removeEventListener(childEventListener);
            }

        }
    }
}
