package com.example.ducng.checkinproject.ui.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.ducng.checkinproject.BaseActivity;
import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.edit_create_user.EditAndCreateUserFragment;
import com.example.ducng.checkinproject.ui.home.checkin.CheckInFragment;
import com.example.ducng.checkinproject.ui.home.manager.SendUserLogin;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.EventCallbackUserLogInSuccess;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.EventCallbackUserLoginFailt;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.IViewRequestUserCheckined;
import com.example.ducng.checkinproject.ui.login.LoginActivity;
import com.example.ducng.checkinproject.ui.mvp_common_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.notification.MyService;
import com.example.ducng.checkinproject.util.Constaint;
import com.example.ducng.checkinproject.util.NetworkHelper;
import com.example.ducng.checkinproject.util.Utils;
import com.example.ducng.checkinproject.util.mvp.BaseViewActivity;
import com.example.ducng.checkinproject.util.mvp.IBaseView;
import com.example.ducng.checkinproject.util.mvp.manager.request_user_logined.IPresenterRequestUserLogined;
import com.example.ducng.checkinproject.util.mvp.manager.request_user_logined.PresenterRequestUserLogined;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.hdodenhof.circleimageview.CircleImageView;
import butterknife.ButterKnife;


public class HomeActivity extends BaseActivity {
    private static final String TAG = "HomeActivity";
    private CircleImageView circleImageView;
    private TextView txtUserName;
    private TextView txtChucVu;
    private ImageView btnEditUser;
    IBaseView iBaseView;
    ProgressDialog progressBar;
    private int userPriovity;
    String openFromNotification;
    private User userLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        EventBus.getDefault().register(this);

        this.setupUI(this.getWindow().getDecorView().findViewById(android.R.id.content));
        progressBar = new ProgressDialog(this);
        progressBar.setMessage("Đang cập nhật thông tin...");
        NetworkHelper networkHelper = new NetworkHelper();
        if (!networkHelper.isNetworkAvailable(this)) {
            progressBar.hide();
            Toast.makeText(this, "Kết nối Internet để tiếp tục sử dụng!", Toast.LENGTH_LONG).show();
            return;
        }
        circleImageView = findViewById(R.id.cir_img_user);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragmentEdituser();
            }
        });
        txtUserName = findViewById(R.id.txt_user_name_in_home_activity);
        txtChucVu = findViewById(R.id.txt_priovity_user);
        btnEditUser = findViewById(R.id.btn_edit_user);
        btnEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFragmentEdituser();
                setBtnEditUser(View.GONE);
            }
        });
        if (getIntent() != null) {
            Intent intent = getIntent();
            if (intent.getStringExtra(Constaint.OPEN_FROM_NOTIFICATION) != null) {
                openFromNotification = intent.getStringExtra(Constaint.OPEN_FROM_NOTIFICATION);
            } else {
                openFromNotification = null;
            }
        }
        progressBar.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                getUserLogined();
            }
        }).start();

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void evenCallbackUserLoginSuccess(EventCallbackUserLogInSuccess eventCallbackUserLogInSuccess) {
        if (eventCallbackUserLogInSuccess != null) {
            Log.d("ModelGetUser", "evenCallbackUserLoginSuccess: ");
            User user = eventCallbackUserLogInSuccess.getUser();
            userLogin= user;
            if (!user.getImage().isEmpty()) {
                Picasso.get()
                        .load(user.getImage())
                        .resize(200, 150)
                        .centerCrop()
                        .error(R.drawable.ic_error_black)
                        .into(circleImageView);
            }
            txtUserName.setText(user.getName());
            userPriovity = user.getPriovity();
            if (openFromNotification != null) {
                progressBar.dismiss();
                showFragmentEdituser();
            } else {
                switch (user.getPriovity()) {
                    case 0:
                        txtChucVu.setText("Nhân viên");
                        showFragmentOfMember();
                        break;
                    case 1:
                        txtChucVu.setText("Trưởng nhóm");
                        showFragmentOfTeamLeader();
                        break;
                    case 2:
                        txtChucVu.setText("Quản lý");
                        showFragmentOfAdmin();
                        break;
                    default:
                        break;
                }
            }
            progressBar.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void evenCallbackUserLoginFailt(EventCallbackUserLoginFailt loginFailt) {
        Log.d("ModelGetUser", "flait: ");
        Toast.makeText(this, "Quá trình lấy thông tin thất bại", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        progressBar.dismiss();

    }


    public void setBtnEditUser(int visible) {
        btnEditUser.setVisibility(visible);
    }

    private void getUserLogined() {
        Log.d("ModelGetUser", "getUserLogined: ");
        IPresenterRequestUserLogined iPGetUser = new PresenterRequestUserLogined();
        iPGetUser.requestUser();
    }

    public void setAvatar(String link) {
        Picasso.get()
                .load(link)
                .resize(200, 150)
                .centerCrop()
                .error(R.drawable.ic_error_black)
                .into(circleImageView);
    }

    private void showFragmentOfMember() {

        CheckInFragment checkInFragment = new CheckInFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, checkInFragment)
                .show(checkInFragment)
                .commit();
    }

    private void showFragmentOfTeamLeader() {
        EventBus.getDefault().postSticky(new SendUserLogin(userLogin));
        HomeManagerFragment homeManagerFragment = new HomeManagerFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, homeManagerFragment)
                .show(homeManagerFragment)
                .commit();
//        ButterKnife.bind(this);
//        FirebaseAuth.getInstance().signInWithEmailAndPassword("admin01@gmail.com","123456").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                if (task.isSuccessful()) {
//                    EditAndCreateUserFragment editAndCreateUserFragment = new EditAndCreateUserFragment(EditAndCreateUserFragment.TYPE_EDIT);
//                    FragmentTransaction fragmentTransaction =getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.main_frame,editAndCreateUserFragment);
//                    fragmentTransaction.commit();
//                }
//            }
//        });

    }

    private void showFragmentOfAdmin() {
        ManagerFragment managerFragment = new ManagerFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, managerFragment)
                .show(managerFragment)
                .commit();
    }

    private void showFragmentEdituser() {

        EditAndCreateUserFragment editAndCreateUserFragment = new EditAndCreateUserFragment(EditAndCreateUserFragment.TYPE_EDIT, null);
        Utils.tranformFragmentNotAddToStack(this, R.id.main_frame, editAndCreateUserFragment);
    }

    public void signOut() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent myService = new Intent(HomeActivity.this, MyService.class);
        stopService(myService);
    }
}
