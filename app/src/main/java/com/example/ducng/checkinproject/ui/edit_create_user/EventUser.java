package com.example.ducng.checkinproject.ui.edit_create_user;

import com.example.ducng.checkinproject.data.model.User;

public class EventUser {
    public static final String KEY_GET = "get";
    public static final String KEY_CREATE = "create";
    public static final String KEY_EDIT = "edit";

    public EventUser(User user, String key) {
        this.user = user;
        this.key = key;
    }

    private User user;
    private String key;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
