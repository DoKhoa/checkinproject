package com.example.ducng.checkinproject.ui.home.checkin;

import com.example.ducng.checkinproject.data.database.checkindatabase.CheckInDataRepository;
import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.util.Utils;
import com.example.ducng.checkinproject.util.mvp.BasePresenter;

public class CheckInPresenter extends BasePresenter<ICheckInContract.View> implements ICheckInContract.Presenter {
    private CheckInDataRepository checkInDataRepository;
    CheckInPresenter(ICheckInContract.View view, CheckInDataRepository checkInDataRepository) {
        this.view = view;
        this.checkInDataRepository =checkInDataRepository;
    }

    @Override
    public void setWorkStatus(int status) {
        view.showWorkStatus(status);
    }

    @Override
    public void updateCheckIn(String uid, int pickedDate, int status, String reason) {
        if (status<3&&reason.equals("")){
            view.showReasonError();
            return;
        }
        if (view == null) {
            return;
        }
        view.setProgressBar(true);
        CheckIn checkIn = new CheckIn(uid,Utils.getDate(pickedDate),status,reason);
//        checkInDataRepository.updateCheckIn(uid, pickedDate, checkIn, new CheckInDataRepository.UpdateCheckInCallback() {
//            @Override
//            public void onSuccess() {
//                if (view!=null){
//                    view.setProgressBar(false);
//                    view.showToastMessage("Update Success!");
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//                if (view!=null){
//                    view.setProgressBar(false);
//                    view.showToastMessage("Update Fail!");
//                }
//            }
//        });
        checkInDataRepository.updateCheckInUseEventBus(uid,pickedDate,checkIn);
    }

    @Override
    public void getCheckIn(final String uid, final int day) {
        if (view == null) {
            return;
        }
        view.setProgressBar(true);
//        checkInDataRepository.getCheckIn(uid, day, new CheckInDataRepository.GetCheckInCallback() {
//            @Override
//            public void onSuccess(CheckIn checkIn) {
//                if (view != null) {
//                    view.setProgressBar(false);
//                    view.setUpViewWithCheckIn(checkIn);
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//                if (view != null) {
//                    view.setProgressBar(false);
//                    CheckIn checkIn = new CheckIn(uid, Utils.getDate(day),0);
//                    view.setUpViewWithCheckIn(checkIn);
//                }
//            }
//        });
        checkInDataRepository.getCheckInUseEventBus(uid,day);
    }
}
