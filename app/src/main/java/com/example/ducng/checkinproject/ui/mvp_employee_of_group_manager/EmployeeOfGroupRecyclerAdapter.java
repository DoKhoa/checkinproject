package com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeOfGroupRecyclerAdapter extends
        RecyclerView.Adapter<EmployeeOfGroupRecyclerAdapter.EmployeeOfGroupViewHolder>{

    IEmployeeOfGroup iEmployeeOfGroup;
    List<User> users;

    public EmployeeOfGroupRecyclerAdapter(IEmployeeOfGroup iEmployeeOfGroup, List<User> users) {
        this.iEmployeeOfGroup = iEmployeeOfGroup;
        this.users = users;
    }

    @NonNull
    @Override
    public EmployeeOfGroupViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.custom_list_employee, viewGroup, false);

        EmployeeOfGroupViewHolder employeeOfGroupViewHolder = new EmployeeOfGroupViewHolder(view);
        return employeeOfGroupViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeOfGroupViewHolder employeeOfGroupViewHolder, final int position) {
        User user = users.get(position);
 //       employeeOfGroupViewHolder.imgAvatarEmployee.setBackgroundResource(R.drawable.absent_color);
//        if(user.getImage() != null && !user.getImage().equals("")) {
//            GlideApp.with(employeeOfGroupViewHolder.imgAvatarEmployee.getContext())
//                    .load(user.getImage().toString())
//                    .centerCrop()
//                    .placeholder(R.drawable.shape_gray)
//                    .error(R.drawable.shape_gray)
//                    .into(employeeOfGroupViewHolder.imgAvatarEmployee);
//        }
        if (!user.getImage().isEmpty()) {
            Picasso.get()
                    .load(user.getImage())
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.ic_error_black)
                    .into(employeeOfGroupViewHolder.imgAvatarEmployee);
        }
        employeeOfGroupViewHolder.tvNameEmployee.setText(user.getName());
        int priovity = user.getPriovity();
        String job = "";
        switch (priovity){
            case 0:
                job = "Nhân viên";
                break;
            case 1:
                job = "Trưởng nhóm";
                break;
            case 2:
                job = "Quản lý";
                break;
        }
        employeeOfGroupViewHolder.tvJobEmployee.setText(job);
        employeeOfGroupViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iEmployeeOfGroup.onClickEdit(position);
            }
        });
        employeeOfGroupViewHolder.cardViewEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iEmployeeOfGroup.onClickCardView(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(users == null){
            return 0;
        }else {
            return users.size();
        }
    }

    public static class EmployeeOfGroupViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_avatar_employee)
        ImageView imgAvatarEmployee;
        @BindView(R.id.tv_name_employee)
        TextView tvNameEmployee;
        @BindView(R.id.tv_job_employee)
        TextView tvJobEmployee;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.card_view_employee)
        CardView cardViewEmployee;

        public EmployeeOfGroupViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    interface IEmployeeOfGroup{
        void onClickEdit(int position);
        void onClickCardView(int position);
    }

}
