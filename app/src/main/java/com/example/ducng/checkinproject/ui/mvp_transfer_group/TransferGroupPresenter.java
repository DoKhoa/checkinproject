package com.example.ducng.checkinproject.ui.mvp_transfer_group;

import com.example.ducng.checkinproject.models.model_add_employee.IModelAddEmployee;
import com.example.ducng.checkinproject.models.model_display_group.IModelDisplayGroup;
import com.example.ducng.checkinproject.models.model_add_employee.ModelAddEmployee;
import com.example.ducng.checkinproject.models.model_display_group.ModelDisplayGroup;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IAddEmployeeFirebase;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IUpdateLeaderFirebase;
import com.example.ducng.checkinproject.ui.mvp_display_group.IGetGroupFromFirebase;

import java.util.ArrayList;
import java.util.List;


public class TransferGroupPresenter implements ITransferGroupContact.ITransferGroupPresenter,
        IGetGroupFromFirebase, IAddEmployeeFirebase, IUpdateLeaderFirebase, IUpdatePriorityFirebase {
    ITransferGroupContact.ITransferGroupView view;
    IModelDisplayGroup modelDisplayGroup;
    IModelAddEmployee modelAddEmployee;

    public TransferGroupPresenter(ITransferGroupContact.ITransferGroupView view) {
        this.view = view;
        modelDisplayGroup = new ModelDisplayGroup();
        modelAddEmployee = new ModelAddEmployee();
    }

    @Override
    public void loadAllNameGroup() {
        modelDisplayGroup.loadGroupFormFirebase(this);
    }

    @Override
    public void updateGroupOfEmployee(User user, String groupId, int priovity) {
        modelAddEmployee.addEmployeeToGroup(this, user, groupId,priovity );
        //modelAddEmployee.addEmployeeToGroupUseEventBus(user, groupId, priovity);
    }

    @Override
    public void changeLeadOfGroup(User user, String groupId) {
        modelAddEmployee.updateLeaderGroup(this, user, groupId);
        //modelAddEmployee.updateLeaderGroupUseEventBus(user, groupId);
    }

    @Override
    public void updatePriorityOldLeader(String oldLeaderId) {
        modelAddEmployee.updatePriorityOldLeader(this, oldLeaderId);
        //modelAddEmployee.updatePriorityOldLeaderUseEventBus(oldLeaderId);
    }


    @Override
    public void getSucceed(List<Group> groups) {
        List<String> groupNames = new ArrayList<String>();
        if (groupNames != null) {
            for (Group group : groups) {
                groupNames.add(group.getId());
            }
        }
        view.displaySpinner(groupNames);
    }

    @Override
    public void getFailue() {
        view.showToast("Khong the lay du lieu");
    }

    @Override
    public void addEmployeeSucceed() {
        view.addSucceed();
    }

    @Override
    public void addEmployeeFailue() {
        view.showToast("Update that bai");
    }

    @Override
    public void updateLeadSucceed(String userId) {
        view.updateSucceed(userId);
    }

    @Override
    public void updateLeadFailue() {
        view.showToast("Update that bai");
    }


    @Override
    public void getChangePrioritySucceed() {
        view.completeChangeSucceed();
    }

    @Override
    public void getChangePriorityFailue() {
        view.showToast("Update that bai");
    }
}
