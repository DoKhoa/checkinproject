package com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager;

import com.example.ducng.checkinproject.models.model_employee_of_group.IModelEmployeeOfGroup;
import com.example.ducng.checkinproject.models.model_employee_of_group.ModelEmployeeOfGroup;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import java.util.ArrayList;
import java.util.List;

public class EmployeeOfGroupPresenter implements
        IEmployeeOfGroupContact.IEmployeeOfGroupPresenter
        , IGetEmployeeOfGroupFirebase {
    IEmployeeOfGroupContact.IEmployeeOfGroupView view;
    IModelEmployeeOfGroup iModelEmployeeOfGroup;
    public EmployeeOfGroupPresenter(IEmployeeOfGroupContact.IEmployeeOfGroupView view) {
        this.view = view;
        iModelEmployeeOfGroup = new ModelEmployeeOfGroup();
    }

    @Override
    public void loadEmployeeOfGroup(String groupId, int i) {
        //iModelEmployeeOfGroup.loadEmployeeOfGroup(this,  groupId, i);
        iModelEmployeeOfGroup.loadEmployeeOfGroupUseEventBus(groupId, i);
    }

    @Override
    public void getEmployeeSucceed(List<User> users, int i) {
        List<User> userArrange = new ArrayList<User>();
        if(users != null){
            for(int index = 0; index < users.size(); index++){
                if(users.get(index).getPriovity() == 1){
                    userArrange.add(users.get(index));
                    continue;
                }
            }
            for(int index = 0; index < users.size(); index++){
                if(users.get(index).getPriovity() != 1){
                    userArrange.add(users.get(index));
                    continue;
                }
            }
        }
        //view.loadEmployeeOfGroupSucceed(users);
        view.loadEmployeeOfGroupSucceed(userArrange);
    }

    @Override
    public void getEmployeeFailue() {
        view.loadEmployeeOfGroupFailue();
    }
}
