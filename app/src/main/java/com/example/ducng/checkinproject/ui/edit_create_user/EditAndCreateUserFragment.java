package com.example.ducng.checkinproject.ui.edit_create_user;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.ducng.checkinproject.BuildConfig;
import com.example.ducng.checkinproject.data.database.DaoUser;
import com.example.ducng.checkinproject.data.model.EventMessage;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.home.HomeActivity;
import com.example.ducng.checkinproject.ui.home.checkin.CheckInFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_common_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.EmployeeOfGroupFragment;
import com.example.ducng.checkinproject.util.NetworkHelper;
import com.example.ducng.checkinproject.util.Utils;
import com.example.ducng.checkinproject.util.mvp.BaseView;
import com.example.ducng.checkinproject.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

@SuppressLint("ValidFragment")
public class EditAndCreateUserFragment extends BaseView implements EditAndCreateUserContract.View {
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_dob)
    EditText etDayOfBirth;
    @BindView(R.id.et_gender)
    EditText etGender;
    @BindView(R.id.et_device)
    EditText etDevice;
    @BindView(R.id.et_cmt)
    EditText etCMT;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.bt_back)
    Button btBack;
    @BindView(R.id.bt_change)
    Button btChange;
    @BindView(R.id.view_dob)
    View viewDoB;
    @BindView(R.id.view_gender)
    View viewGender;
    @BindView(R.id.bt_signout)
    Button btSignOut;
    public static final int TYPE_CREATE = 0;
    public static final int TYPE_EDIT = 1;
    public static final String GENDER_MALE = "Nam";
    public static final String GENDER_FEMALE = "Nữ";
    private EditAndCreateUserContract.Presenter presenter;
    private int type;
    private String group;
    // 0: thêm người dùng
    // 1: sửa người dùng
    private String linkAvatar;
    private User currentUser;


    /**
     * @param type   = 1: sua
     *               = 0: them
     * @param group: null neu sua thong tin
     *               khac null neu them user
     */
    @SuppressLint("ValidFragment")
    public EditAndCreateUserFragment(int type, String group) {
        this.type = type;
        this.group = group;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new EditAndCreateUserPresenter(this, DaoUser.getInstance(NetworkHelper.getInstance()));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_profile, container, false);
        ButterKnife.bind(this, view);
        linkAvatar = null;
        setupView(type);
        return view;
    }

    private void setupView(int type) {
        if (type == TYPE_CREATE) {
            btChange.setText("THÊM");
            btSignOut.setVisibility(View.GONE);
            etEmail.setEnabled(true);
        } else {
            btChange.setText("THAY ĐỔI");
            presenter.getUser(getContext());
            btSignOut.setVisibility(View.VISIBLE);
            etEmail.setEnabled(false);
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getSuccessUser(EventUser eventUser) {
        User user = eventUser.getUser();
        if (eventUser.getKey().equals(EventUser.KEY_GET)) {
            setupViewWithUser(user);
        } else if (eventUser.getKey().equals(EventUser.KEY_EDIT)) {
            setAvatarHome(user.getImage());
        } else if (eventUser.getKey().equals(EventUser.KEY_CREATE)) {
            clearAll();
            presenter.sendEmail(user.getEmail(),user.getDefaultPassword());
        }

        setProgressBar(false);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getFailureUser(EventMessage eventMessage) {
        setProgressBar(false);
        showToastMessage(eventMessage.getMessage());

    }
    public void setupViewWithUser(User user) {
        this.currentUser = user;
        if (!user.getName().isEmpty()) {
            etName.setText(user.getName());
        }
        if (!user.getDob().isEmpty()) {
            etDayOfBirth.setText(user.getDob());
        }
        if (user.isGender()) {
            etGender.setText(GENDER_MALE);
        } else {
            etGender.setText(GENDER_FEMALE);
        }
        if (!user.getDevice().isEmpty()) {
            etDevice.setText(user.getDevice());
        }
        if (!user.getCmt().isEmpty()) {
            etCMT.setText(user.getCmt());
        }
        if (!user.getEmail().isEmpty()) {
            etEmail.setText(user.getEmail());
        }
        if (!user.getPhone().isEmpty()) {
            etPhone.setText(user.getPhone());
        }
        if (!user.getImage().isEmpty()) {
            Picasso.get()
                    .load(user.getImage())
                    .resize(200, 150)
                    .centerCrop()
                    .error(R.drawable.ic_error_black)
                    .into(ivAvatar);
            linkAvatar = user.getImage();
        }
    }

    @Override
    public void startActivity(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startAct(Intent intent) {
        startActivity(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onViewActive(this);
        presenter.onActivityResult(requestCode, resultCode, data, getContext());
    }

    @Override
    public void setImageUser(String url) {
        if (ivAvatar == null)
            return;

        if (url.equals("Null")) {
            ivAvatar.setImageResource(R.drawable.default_avatar);
            return;
        }
        Picasso.get()
                .load(url)
                .resize(200, 150)
                .centerCrop()
                .error(R.drawable.ic_error_black)
                .into(ivAvatar);
    }

    @Override
    public void setImageUser(Uri uri) {
        Picasso.get()
                .load(uri)
                .resize(200, 150)
                .centerCrop()
                .error(R.drawable.ic_error_black)
                .into(ivAvatar);
    }

    @OnClick(R.id.bt_back)
        public void clickButtonBack() {
        removeFragment();
        if (type == EditAndCreateUserFragment.TYPE_EDIT) {
            if (currentUser.getPriovity()== 1 ) {
                ((HomeActivity) getActivity()).setBtnEditUser(View.VISIBLE);
                HomeManagerFragment homeManagerFragment = new HomeManagerFragment();
                Utils.tranformFragmentNotAddToStack(getActivity(), R.id.main_frame, homeManagerFragment);
            }
            if (currentUser.getPriovity()==0) {
                ((HomeActivity) getActivity()).setBtnEditUser(View.VISIBLE);
                CheckInFragment checkInFragment = new CheckInFragment();
                Utils.tranformFragmentNotAddToStack(getActivity(),R.id.main_frame,checkInFragment);

            }
            if (currentUser.getPriovity()==2) {
                ((HomeActivity) getActivity()).setBtnEditUser(View.VISIBLE);
                ManagerFragment managerFragment = new ManagerFragment();
                Utils.tranformFragmentNotAddToStack(getActivity(),R.id.main_frame,managerFragment);
//                getFragmentManager().popBackStack();
            }

        }
        if (type == EditAndCreateUserFragment.TYPE_CREATE) {
//            ManagerFragment fragment = new ManagerFragment();
//            Utils.tranformFragmentNotAddToStack(getActivity(),R.id.main_frame,fragment);
            getFragmentManager().popBackStack();
        }

//        ((HomeActivity) getActivity()).setView();
    }

    private void removeFragment() {
        if (getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_LIST_MEMBER) != null) {
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_LIST_MEMBER));
            trans.commit();
        }
        //Ẩn fragment checkin
        if (getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECK_IN) != null) {
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECK_IN));
            trans.commit();
        }

        //Ẩn fragment detail checkin 0f member
        if (getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECKIN_MEMBER) != null) {
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECKIN_MEMBER));
            trans.commit();
        }


    }

    @OnClick(R.id.bt_change)
    public void clickButtonChange() {
//        showToastMessage("click button change");
        if (validateData()) {
            User user = new User();
            user.setName(etName.getText().toString());
            user.setCmt(etCMT.getText().toString());
            user.setDevice(etDevice.getText().toString());
            user.setDob(etDayOfBirth.getText().toString());
            user.setEmail(etEmail.getText().toString());
            if (etGender.getText().toString().equals(GENDER_FEMALE)) {
                user.setGender(false);
            } else {
                user.setGender(true);
            }
            user.setPhone(etPhone.getText().toString());
            user.setImage(getLink());
            if (group != null) {
                user.setGroupId(group);
            }
            if (type == TYPE_CREATE) {
                presenter.createUser(user, getActivity());
            } else {
                user.setId(currentUser.getId());
                user.setGroupId(currentUser.getGroupId());
                user.setPriovity(currentUser.getPriovity());
                presenter.updateUser(user, getActivity());
            }
        }
    }
    @OnClick(R.id.bt_signout)
    public void clickBtSignOut() {
        showDialog();
    }
    public void showDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_sign_out);


        Button dialogButtonYes = (Button) dialog.findViewById(R.id.bt_yes);
        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).signOut();
                dialog.dismiss();
            }
        });
        Button dialogButtonNo = dialog.findViewById(R.id.bt_no);
        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
    @OnClick(R.id.view_dob)
    public void clickEtDayOfBirth() {
//        showToastMessage("click edit text day of birth");
        presenter.showTimePicker(getActivity());
    }

    @OnClick(R.id.view_gender)
    public void clickEtGender() {
//        showToastMessage("click edit text gender");
        presenter.showGenderPicker(getActivity());
    }

    public String getLink() {
        return linkAvatar;
    }

    @Override
    public void setLinkAvatar(String link) {
        linkAvatar = link;
    }

    @Override
    public boolean allowStoragePermissions(String[] permissions, int requestCode) {
        int permission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    permissions,
                    requestCode
            );
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void setDayOfBirth(String date) {
        etDayOfBirth.setText(date);
    }

    @Override
    public void setGender(String gender) {
        etGender.setText(gender);
    }

    @Override
    public void clearAll() {
        etName.setText(null);
        etDayOfBirth.setText(null);
        etGender.setText(null);
        etDevice.setText(null);
        etCMT.setText(null);
        etEmail.setText(null);
        etPhone.setText(null);
        Picasso.get()
                .load(R.drawable.default_avatar)
                .resize(200, 150)
                .centerCrop()
                .error(R.drawable.ic_error_black)
                .into(ivAvatar);
    }

    @Override
    public void setAvatarHome(String link) {
        ((HomeActivity) getActivity()).setAvatar(link);
    }


    @OnClick(R.id.iv_avatar)
    public void clickImageViewAvatar() {
//        showToastMessage("click image view");
        presenter.changeAvatar(getActivity());

    }

    public boolean validateData() {
        if (etName.getText().toString().isEmpty()) {
            etName.setError("Tên không để trống");
            return false;
        }
        if (etDayOfBirth.getText().toString().isEmpty()) {
            etPhone.setError("Nhập ngày sinh");
            return false;
        }
        if (!Utils.isCMTValid(etCMT.getText().toString())) {
            etCMT.setError("Chứng minh thư gồm 9 hoặc 12 số");
            return false;
        }
        if (etDevice.getText().toString().isEmpty()) {
            etDevice.setError("Nhập thông tin máy tính");
            return false;
        }
        if (!Utils.isEmailValid(etEmail.getText().toString())) {
            etEmail.setError("Email sai định dạng");
            return false;
        }
        return true;
    }


    @Override
    public void onPause() {
        presenter.onViewInactive();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onViewActive(this);
    }
}
