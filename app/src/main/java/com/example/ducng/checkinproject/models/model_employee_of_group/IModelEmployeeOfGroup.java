package com.example.ducng.checkinproject.models.model_employee_of_group;

import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.IGetEmployeeOfGroupFirebase;

public interface IModelEmployeeOfGroup {
    void loadEmployeeOfGroup(IGetEmployeeOfGroupFirebase iGetEmployeeOfGroupFirebase, String groupId,  int i);
    void loadEmployeeOfGroupUseEventBus(String groupId, int i);
}
