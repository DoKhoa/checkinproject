package com.example.ducng.checkinproject.ui.mvp_transfer_group;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface ITransferGroupContact {
    interface ITransferGroupView{
        void displaySpinner(List<String> nameGroups);
        void showToast(String str);
        void updateSucceed(String userId);
        void addSucceed();
        void completeChangeSucceed();
    }

    interface ITransferGroupPresenter{
        void loadAllNameGroup();
        void updateGroupOfEmployee(User user, String groupId, int priovity);
        void changeLeadOfGroup(User user,String groupId);
        void updatePriorityOldLeader(String oldLeaderId);
    }
}
