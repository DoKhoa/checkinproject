package com.example.ducng.checkinproject.data.model.manager.update_comment_of_id;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ModelUpdateComment implements IModelUpdateComment {
    @Override
    public void updateComment(final IModelUpdateCommentCallback iModelUpdateCommentCallback, final String userId, final String dateCheckin, final String comment) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("checkin");
        databaseReference.child(userId).child(dateCheckin).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Update nhận xét
                CheckIn checkIn = dataSnapshot.getValue(CheckIn.class);
                if (checkIn == null) {
                    checkIn = new CheckIn();
                    checkIn.setDateId(dateCheckin);
                }
                checkIn.setNhanXet(comment);
                databaseReference.child(userId).child(dateCheckin).setValue(checkIn);


                //Get ListCheckin after update;
                final List<CheckIn> checkIns = new ArrayList<>();
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("checkin");
                reference.child(userId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            CheckIn tempCheckIn = snapshot.getValue(CheckIn.class);
                            CheckIn checkIn = new CheckIn();
                            if (tempCheckIn.getDateId() != null) {
                                checkIn.setDateId(tempCheckIn.getDateId());
                            }
                            if (tempCheckIn.getUserId() != null) {
                                checkIn.setUserId(tempCheckIn.getUserId());
                            }
                            checkIn.setStatus(tempCheckIn.getStatus());
                            checkIn.setLyDo(tempCheckIn.getLyDo());
                            checkIn.setNhanXet(tempCheckIn.getNhanXet());
                            checkIns.add(checkIn);
                        }
                        iModelUpdateCommentCallback.updateCommentSuccessly(checkIns);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iModelUpdateCommentCallback.updateCommentFailt();
            }
        });
    }
}
