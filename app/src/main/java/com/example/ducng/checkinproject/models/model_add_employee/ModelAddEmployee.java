package com.example.ducng.checkinproject.models.model_add_employee;

import android.support.annotation.NonNull;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.models.model_add_employee.IModelAddEmployee;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IAddEmployeeFirebase;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IGetEmployeeWithoutGroupFirebase;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IUpdateLeaderFirebase;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.IUpdatePriorityFirebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ModelAddEmployee implements IModelAddEmployee {
    private DatabaseReference mData  = FirebaseDatabase.getInstance().getReference();

    @Override
    public void loadEmployeeWithoutGroup(final IGetEmployeeWithoutGroupFirebase iGetEmployeeWithoutGroupFirebase) {
        final List<User> users = new ArrayList<User>();
        mData.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    User user = dataSnapshot1.getValue(User.class);
                    if(user.getGroupId() == null || user.getGroupId().equals("")){
                        users.add(user);
                    }
                }
                iGetEmployeeWithoutGroupFirebase.getEmployeeWithoutGroupSucceed(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iGetEmployeeWithoutGroupFirebase.getEmployeeWithoutGroupFailue();
            }
        });
    }

    //transfer
    @Override
    public void addEmployeeToGroup(final IAddEmployeeFirebase iAddEmployeeFirebase,
                                   User user, final String groupId, final int priovity) {
        mData.child("users").child(user.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user != null){
                    user.setGroupId(groupId);
                    user.setPriovity(priovity);
                    mData.child("users").child(user.getId()).setValue(user);
                }else {
                    iAddEmployeeFirebase.addEmployeeFailue();
                }
                iAddEmployeeFirebase.addEmployeeSucceed();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iAddEmployeeFirebase.addEmployeeFailue();
            }
        });
    }

    @Override
    public void updateLeaderGroup(final IUpdateLeaderFirebase iUpdateLeaderFirebase,
                                  final User user, final String groupId) {
        mData.child("groups").child(groupId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Group group = dataSnapshot.getValue(Group.class);
                String managerId = "";
                if(group != null){
                    managerId = group.getManagerId();
                    group.setManagerId(user.getId());
                    mData.child("groups").child(groupId).setValue(group);
                }else {
                    iUpdateLeaderFirebase.updateLeadFailue();
                }
                iUpdateLeaderFirebase.updateLeadSucceed(managerId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iUpdateLeaderFirebase.updateLeadFailue();
            }
        });
    }

    @Override
    public void updatePriorityOldLeader(final IUpdatePriorityFirebase iUpdatePriorityFirebase, final String oldUserId) {
        mData.child("users").child(oldUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user != null){
                    user.setPriovity(0);
                    mData.child("users").child(oldUserId).setValue(user);
                }else {
                    iUpdatePriorityFirebase.getChangePriorityFailue();
                }
                iUpdatePriorityFirebase.getChangePrioritySucceed();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                iUpdatePriorityFirebase.getChangePriorityFailue();
            }
        });
    }


    @Override
    public void addEmployeeToGroupUseEventBus(User user, final String groupId, final int piority) {
        mData.child("users").child(user.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user != null){
                    user.setGroupId(groupId);
                    user.setPriovity(piority);
                    mData.child("users").child(user.getId()).setValue(user);
                }else {
//                    iAddEmployeeFirebase.addEmployeeFailue();
                    EventBus.getDefault().post(new EventAddEmpoyeeMessage("Them nhan vien that bai", 0));
                }
                //iAddEmployeeFirebase.addEmployeeSucceed();
                EventBus.getDefault().post(new EventAddEmpoyeeMessage("Them nhan vien thanh cong vao nhom", 1));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                EventBus.getDefault().post(new EventAddEmpoyeeMessage("Them nhan vien that bai", 0));
            }
        });
    }

    @Override
    public void updateLeaderGroupUseEventBus(final User user, final String groupId) {
        mData.child("groups").child(groupId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Group group = dataSnapshot.getValue(Group.class);
                String managerId = "";
                if(group != null){
                    managerId = group.getManagerId();
                    group.setManagerId(user.getId());
                    mData.child("groups").child(groupId).setValue(group);
                }else {

                    EventBus.getDefault().post(new EventAddEmpoyeeMessage("Update nhom truong that bai", 0));
                }
                EventBus.getDefault().post(managerId);
                EventBus.getDefault().post(new EventAddEmpoyeeMessage("Update nhom truong thanh cong", 2));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                EventBus.getDefault().post(new EventAddEmpoyeeMessage("Update nhom truong that bai", 0));
            }
        });
    }

    @Override
    public void updatePriorityOldLeaderUseEventBus(final String oldUserId) {
        mData.child("users").child(oldUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user != null){
                    user.setPriovity(0);
                    mData.child("users").child(oldUserId).setValue(user);
                }else {
                    //iUpdatePriorityFirebase.getChangePriorityFailue();
                    EventBus.getDefault().post(new EventAddEmpoyeeMessage("Chuyen nhom that bai", 0));
                }
                //iUpdatePriorityFirebase.getChangePrioritySucceed();
                EventBus.getDefault().post(new EventAddEmpoyeeMessage("Chuyen nhom hoan thanh", 3));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //iUpdatePriorityFirebase.getChangePriorityFailue();
                EventBus.getDefault().post(new EventAddEmpoyeeMessage("Chuyen nhom that bai", 0));
            }
        });
    }


}
