package com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member;

import com.example.ducng.checkinproject.data.model.CheckIn;

import java.util.List;

public interface IRequestUpdateComentCallback {
    void updateSeccess(List<CheckIn> checkIns);
    void updateFailt();
}
