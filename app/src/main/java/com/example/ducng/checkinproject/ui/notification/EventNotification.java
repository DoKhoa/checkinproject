package com.example.ducng.checkinproject.ui.notification;

public class EventNotification {
    private String idUser;
    private boolean isShow;
    private String body;
    private String title;
    private String key;
    public EventNotification(String idUser, boolean isShow, String body, String title) {
        this.idUser = idUser;
        this.isShow = isShow;
        this.body = body;
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public EventNotification() {
    }

    public String getIdUser() {
        if (idUser== null) {
            return "";
        }
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public String getBody() {
        if (body== null) {
            return "";
        }
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        if (title== null) {
            return "";
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
