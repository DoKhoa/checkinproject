package com.example.ducng.checkinproject.models.model_add_employee;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IAddEmployeeFirebase;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IGetEmployeeWithoutGroupFirebase;
import com.example.ducng.checkinproject.ui.mvp_add_employee.IUpdateLeaderFirebase;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.IUpdatePriorityFirebase;

public interface IModelAddEmployee {
    void loadEmployeeWithoutGroup(IGetEmployeeWithoutGroupFirebase iGetEmployeeWithoutGroupFirebase);
    void addEmployeeToGroup(IAddEmployeeFirebase iAddEmployeeFirebase, User user, String groupId, int piority);
    void updateLeaderGroup(IUpdateLeaderFirebase iUpdateLeaderFirebase, User user, String groupId);
    void updatePriorityOldLeader(IUpdatePriorityFirebase iUpdatePriorityFirebase, String oldUserId);

    void addEmployeeToGroupUseEventBus(User user, String groupId, int piority);
    void updateLeaderGroupUseEventBus(User user, String groupId);
    void updatePriorityOldLeaderUseEventBus(String oldUserId);
}
