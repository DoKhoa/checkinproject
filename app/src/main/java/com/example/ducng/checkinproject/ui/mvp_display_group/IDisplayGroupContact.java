package com.example.ducng.checkinproject.ui.mvp_display_group;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IDisplayGroupContact {
    interface IDisplayGroupView{
        void getGroupSucceed(List<Group> groups);
        void getGroupFailure();
        void getUserOfGroupSucceed(List<User> users, int i);
        void getUserOfGroupFailure();
    }

    interface IDisplayPresenter{
        void loadGroupFromFirebase();
        void loadUserOfGroupFromFirebase(String groupId, int i);
    }
}
