package com.example.ducng.checkinproject.ui.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ducng.checkinproject.BaseActivity;
import com.example.ducng.checkinproject.ui.home.HomeActivity;
import com.example.ducng.checkinproject.ui.notification.MyService;
import com.example.ducng.checkinproject.util.DummyData;
import com.example.ducng.checkinproject.util.mvp.mvp_login.PrecenterLogicButtonLogin;

import com.example.ducng.checkinproject.R;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends BaseActivity implements ViewXuLyDangNhap {
    PrecenterLogicButtonLogin precenterLogicButtonLogin;
    EditText edtID_Login, edtMK_Login;
    Button btnLogin;
    ProgressBar progressBar;
    View background;
    CheckBox checkBox;
    String id, mk;
    ImageView imgShow;
    Boolean status = false;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //code that displays the content in full screen mode
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//int flag, int mask
        setContentView(R.layout.activity_login);
        this.setupUI(this.getWindow().getDecorView().findViewById(android.R.id.content));
        if (FirebaseAuth.getInstance().getUid() != null) {
            startAct();
        }
        sharedPreferences = getSharedPreferences("Remember", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        addControl();

        edtID_Login.setText(sharedPreferences.getString("Email", ""));
        edtMK_Login.setText(sharedPreferences.getString("Password", ""));
        checkBox.setChecked(sharedPreferences.getBoolean("checkbox", false));

//        DummyData.dummyData();
//        DummyData.addNhanVien();
        showPassWord();

        precenterLogicButtonLogin = new PrecenterLogicButtonLogin(this);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                background.setVisibility(View.VISIBLE);
                id = edtID_Login.getText().toString().trim();
                mk = edtMK_Login.getText().toString().trim();
                if (mk.length() < 6) {
                    edtMK_Login.setText("");
                    progressBar.setVisibility(View.GONE);
                    background.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Mật khẩu ít nhất có 8 kí tự", Toast.LENGTH_SHORT).show();
                } else {
                    precenterLogicButtonLogin.XuLyDangNhap(id, mk);
                }
            }
        });

    }

    private void showPassWord() {
        imgShow = findViewById(R.id.showPassWord);
        imgShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (status) {
                    imgShow.setBackgroundResource(R.drawable.unshow);
                    status = false;
                    edtMK_Login.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                } else {
                    imgShow.setBackgroundResource(R.drawable.show);
                    status = true;
                    edtMK_Login.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }


            }
        });
    }


    private void addControl() {
        edtID_Login = findViewById(R.id.edtName);
        edtMK_Login = findViewById(R.id.edtMatkhau);
        btnLogin = findViewById(R.id.btnLogin);
        progressBar = findViewById(R.id.progressbar);
        background = findViewById(R.id.bgr);
        checkBox = findViewById(R.id.cb);

    }
    private void startAct() {
        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
        Intent intent = new Intent(getBaseContext(), MyService.class);
        intent.putExtra("receiverTag", mReceiver);
        startService(intent);
        startActivity(i);
        finish();
    }
    @Override
    public void DangNhapThnahCong() {
        if (checkBox.isChecked()) {
            editor.putString("Email", edtID_Login.getText().toString().trim());
            editor.putString("Password", edtMK_Login.getText().toString().trim());
            editor.putBoolean("checkbox", true);
            editor.commit();
        } else {
            editor.remove("Email");
            editor.remove("Password");
            editor.remove("checkbox");
            editor.commit();

        }
        progressBar.setVisibility(View.GONE);
        background.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Đăng Nhập thành công", Toast.LENGTH_SHORT).show();
        startAct();
    }

    @Override
    public void DangNhapThatBai() {
        edtMK_Login.setText("");
        background.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Đăng Nhập thất bại", Toast.LENGTH_SHORT).show();
    }
}
