package com.example.ducng.checkinproject.util.mvp;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;

import butterknife.BindView;

public class BaseView extends Fragment implements IBaseView {
    @BindView(R.id.progress_bar)
    protected ProgressBar progressBar;
    @BindView(R.id.background_view)
    protected View backgroundView;
    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setProgressBar(boolean show) {
        if (show) {
            backgroundView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            backgroundView.setVisibility(View.GONE);
        }
    }
}
