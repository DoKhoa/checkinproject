package com.example.ducng.checkinproject.ui.home.manager.calender;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.CheckIn;

import java.util.List;

public class DateOfCheckInAdapter extends BaseAdapter {
    private List<CheckIn> checkIns;
    private Context context;
    private int indextStart;
    private int indexEnd;
    private String today; // Ngay hien tai
    private String dateId = ""; //search Checkin
    private LayoutInflater inflater;
    private List<Integer> listDayOfMonth;


    public DateOfCheckInAdapter(Context context, List<CheckIn> checkIns, int month, int year,
                                String today, List<Integer> listDay, int indextStart, int indexEnd) {
        this.context = context;
        this.checkIns = checkIns;
        inflater = LayoutInflater.from(context);
        listDayOfMonth = listDay;
        this.indextStart = indextStart;
        this.indexEnd = indexEnd;
        dateId += year;
        if (month < 10) {
            dateId += "0" + month;
        } else {
            dateId += month;
        }
        this.today = today;
    }

    @Override
    public int getCount() {
        return listDayOfMonth.size();
    }

    @Override
    public Object getItem(int i) {
        return listDayOfMonth.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        DateOfCheckInAdapter.ViewHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.item_date_of_dialog_checkin_of_month, viewGroup, false);
            viewHolder = new DateOfCheckInAdapter.ViewHolder();
            view.setTag(viewHolder);
        } else {
            viewHolder = (DateOfCheckInAdapter.ViewHolder) view.getTag();
        }
        viewHolder.imgBackground = view.findViewById(R.id.img_background_date_of_item_date);
        viewHolder.txtDayNumber = view.findViewById(R.id.txt_date_of_item_date);
        viewHolder.txtDayNumber.setText(String.valueOf(listDayOfMonth.get(i)));
        if (i < indextStart || i > indexEnd) {
            viewHolder.txtDayNumber.setTextColor(context.getResources().getColor(R.color.colorTextNotOfMonth));
            viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.dayNotOfMonth));
        } else {
            viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.checkInAbsent));
            if ((i + 1) % 7 == 0) {
                viewHolder.txtDayNumber.setTextColor(context.getResources().getColor(R.color.colorTextOfSaturday));
                viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.checkInAbsent));
            }

            //Check status check nhieu ngay trong cung thang
            String temp = dateId;
            if (listDayOfMonth.get(i) < 10) {
                temp += "0" + listDayOfMonth.get(i);
            } else {
                temp += listDayOfMonth.get(i);
            }
            Log.d("hkhg", "getView: " + temp+"_"+today);
            if (Integer.parseInt(temp) > Integer.parseInt(today)) {
                viewHolder.txtDayNumber.setTextColor(context.getResources().getColor(R.color.colorTextNotOfMonth));
                viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.dayNotOfMonth));
            } else {
                CheckIn checkIn = searchCheckin(temp,checkIns);
                if (checkIn != null) {
                    int status = checkIn.getStatus();
                    switch (status) {
                        case 0:
                            viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.checkInAbsent));
                            break;
                        case 1:
                            viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.checkInMorning));
                            break;
                        case 2:
                            viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.checkInAfternoon));
                            break;
                        case 3:
//                            Log.d("checkItem", "getView: "+i);
                            viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.checkInFull));
                            break;
                        default:
                            break;
                    }
                }
                if ((i) % 7 == 0) {
                    viewHolder.txtDayNumber.setTextColor(context.getResources().getColor(R.color.colorTextOfSunday));
                    viewHolder.imgBackground.setBackgroundColor(context.getResources().getColor(R.color.dayNotWork));
                }
            }
        }
        return view;
    }

    public static CheckIn searchCheckin(String dateId, List<CheckIn> checkIns) {
        if (checkIns==null || checkIns.isEmpty()) {
            return null;
        }
        Log.d("checkItem", "search: "+dateId);
        int date = Integer.parseInt(dateId);
        int start = 0;
        int end = checkIns.size() - 1;
        while (end - start > 1) {
            Log.d("sdh", "searchCheckin: ");
            int temp = (end + start) / 2;
            int checkinDatre = Integer.parseInt(checkIns.get(temp).getDateId());
            if (checkinDatre > date) {
                end = temp;
            } else if (checkinDatre < date) {
                start = temp;
            } else {
                return checkIns.get(temp);
            }
        }
        int dateStart = Integer.parseInt(checkIns.get(start).getDateId());
        if (dateStart == date) {
            return checkIns.get(start);
        }
        int dateEnd = Integer.parseInt(checkIns.get(start).getDateId());
        if (dateEnd == date) {
            return checkIns.get(end);
        }
        return null;
    }


    private class ViewHolder {
        private ImageView imgBackground;
        private TextView txtDayNumber;
    }
}
