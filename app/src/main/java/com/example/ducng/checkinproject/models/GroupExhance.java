package com.example.ducng.checkinproject.models;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public class GroupExhance {
    //private String id;

    private Group group;
    private List<User> users;

    public GroupExhance(Group group) {
        this.group = group;
    }

    public GroupExhance() {
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }


    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
