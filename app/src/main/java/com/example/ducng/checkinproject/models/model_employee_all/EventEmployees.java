package com.example.ducng.checkinproject.models.model_employee_all;

import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public class EventEmployees {
    private List<User> users;

    public EventEmployees(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
