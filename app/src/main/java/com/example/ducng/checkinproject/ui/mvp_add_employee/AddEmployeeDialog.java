package com.example.ducng.checkinproject.ui.mvp_add_employee;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.mvp_transfer_group.IListenClickOkInDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AddEmployeeDialog extends DialogFragment
        implements IAddEmployeeContact.IAddEmployeeView, View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    List<String> nameGroups;
    List<User> users;
    List<String> usersName;
    User userSelected;
    String groupId;
    IAddEmployeeContact.IAddEmployeePresenter presenter;
    IClickListenAddDialog iClickListenAddDialog;

//    @BindView(R.id.ed_name_employee)
//    AutoCompleteTextView tvNameEmployee;
    @BindView(R.id.snipper_select_group)
    Spinner spinnerSelectGroup;
    @BindView(R.id.rd_select_leader)
    RadioButton rdSelectLeader;
    @BindView(R.id.btn_cancel_add_employee)
    Button btnCancel;
    @BindView(R.id.btn_save_employee)
    Button btnSave;
    @BindView(R.id.circle_img_avatar_change)
    CircleImageView  img;
    @BindView(R.id.spinner_choose_employee)
    Spinner spinnerChooseEmployee;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_employee, container, false);
        ButterKnife.bind(this, view);
        inits();
        return view;
    }
    public void setOnClick(IClickListenAddDialog mClick) {
        this.iClickListenAddDialog= mClick;

    }
    private void inits() {
        nameGroups = new ArrayList<String>();
        users = new ArrayList<User>();
        userSelected = new User();
        usersName = new ArrayList<String>();
        groupId = "";
        presenter = new AddEmployeePresenter(this);
        presenter.getAllUserWithoutGroup();
        presenter.addAllNameGroup();

        btnCancel.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        final boolean[] check = {rdSelectLeader.isChecked()};
        rdSelectLeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check[0]) {
                    rdSelectLeader.setChecked(false);
                    check[0] = false;
                } else {
                    rdSelectLeader.setChecked(true);
                    check[0] = true;
                }
            }
        });
        spinnerSelectGroup.setOnItemSelectedListener(this);
        //tvNameEmployee.setOnItemClickListener(this);
        spinnerChooseEmployee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                userSelected = users.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save_employee:
                addEmployeeToGroup(userSelected, groupId);
                break;
            case R.id.btn_cancel_add_employee:
                this.dismiss();
                break;
        }
    }

    private void addEmployeeToGroup(User user, String groupId) {
        if (!groupId.equals("") && user != null && user.getId() != null) {
            presenter.addEmployeeToGroup(user, groupId, rdSelectLeader.isChecked()?1:0);
        } else {
            Toast.makeText(getContext(), "Nhan vien hoac nhom chua duoc chon, hay bo sung", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void displaySpinnerGroup(List<String> nameGroups) {
        this.nameGroups = nameGroups;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, nameGroups);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelectGroup.setAdapter(adapter);
    }

    @Override
    public void displayAutoCompleteTVEmployee(List<User> employees) {
        this.users = employees;
        if (employees != null) {
            for (User user : employees) {
                if(user.getName() == null){
                    usersName.add("");
                }
                else {
                    usersName.add(user.getName());
                }
            }
            Log.d("Test", usersName.size() +"");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_dropdown_item, usersName);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerChooseEmployee.setAdapter(adapter);
//            tvNameEmployee.setThreshold(1);
//            tvNameEmployee.setAdapter(adapter);
        }
    }

    @Override
    public void addSucceed() {
        if (rdSelectLeader.isChecked()) {
            presenter.updateLead(userSelected, groupId);
        } else {
            Toast.makeText(getContext(), "Them thanh cong nhan vien", Toast.LENGTH_LONG).show();
            iClickListenAddDialog.onDismissAddDialog();
            this.dismiss();
        }
    }

    @Override
    public void updateSucceed() {
        Toast.makeText(getContext(), "Them thanh cong nhan vien", Toast.LENGTH_LONG).show();
        iClickListenAddDialog.onDismissAddDialog();
        this.dismiss();
    }

    @Override
    public void showToast(String str) {
        Toast.makeText(getContext(), str, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        groupId = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        groupId = "";
    }

//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        userSelected = users.get(ad);
//        img.setImageResource(R.drawable.absent_color);
////        GlideApp.with(adapterView.getContext())
////                .load(userSelected.getImage().toString())
////                .centerCrop()
////                .placeholder(R.drawable.shape_gray)
////                .error(R.drawable.shape_gray)
////                .into(img);
//    }
}
