package com.example.ducng.checkinproject.util.mvp.mvp_login;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.checkinproject.data.model.AccountLogin;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.login.ViewXuLyDangNhap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class PrecenterLogicButtonLogin implements IPrencenterLogicButtonLogin {
    ViewXuLyDangNhap viewXuLyDangNhap;
    FirebaseAuth mAuth;
    AccountLogin accountLogin = new AccountLogin();
    Model_Login model_login;
    User user;


    public PrecenterLogicButtonLogin(ViewXuLyDangNhap viewXuLyDangNhap) {
        this.viewXuLyDangNhap = viewXuLyDangNhap;
    }



    @Override
    public void XuLyDangNhap(final String id, final String mk) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(id, mk)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            viewXuLyDangNhap.DangNhapThnahCong();

                        } else{
                            model_login = new Model_Login();
                            model_login.XuLyDuLieuLogin(new ICallBack() {
                                @Override
                                public void OnSuccess(List<AccountLogin> accountLogins, final List<String> listKey) {
                                    int i;
                                    for (i = 0; i < accountLogins.size(); i++) {
                                        if (accountLogins.get(i).getEmail().equals(id) && accountLogins.get(i).getDefaultPassword().equals(mk)) {
                                            final int finalI = i;
                                            accountLogin = accountLogins.get(finalI);
                                            FirebaseAuth.getInstance().createUserWithEmailAndPassword(id, mk).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                @Override
                                                public void onComplete(@NonNull Task<AuthResult> task) {
                                                    if (task.isSuccessful()) {
                                                        GanDuLieu(listKey.get(finalI));
                                                        viewXuLyDangNhap.DangNhapThnahCong();
                                                    }else {
                                                        viewXuLyDangNhap.DangNhapThatBai();
                                                    }
                                                }
                                            });
                                            break;
                                        }
                                    }
                                    if(i==(accountLogins.size())){
                                        viewXuLyDangNhap.DangNhapThatBai();
                                    }
                                }
                            });
                            }
                    }
                });


    }


    public void GanDuLieu(String key) {
        user = new User();
            if (accountLogin.getEmail() != null) {
                user.setEmail(accountLogin.getEmail());
            }
            if (accountLogin.getCmt() != null) {
                user.setCmt(accountLogin.getCmt());
            }
            if (accountLogin.getDevice() != null) {
                user.setDevice(accountLogin.getDevice());
            }
            if (accountLogin.getDob() != null) {
                user.setDob(accountLogin.getDob());
            }

            if (accountLogin.getGroupId() != null) {
                user.setGroupId(accountLogin.getGroupId());
            }

            if (accountLogin.getImage() != null) {
                user.setImage(accountLogin.getImage());
            }

            if (accountLogin.getName() != null) {
                user.setName(accountLogin.getName());
            }


            if (accountLogin.getPhone() != null) {
                user.setPhone(accountLogin.getPhone());
            }
            user.setPriovity(0);
            user.setId(FirebaseAuth.getInstance().getUid());
        model_login.DeleteValueAccount(key);
        model_login.AddUser(user);
    }

}




 /*                               if (accountLogin == model_login.XuLyDuLieuLogin().get(i)) {
                                    mAuth.signInWithEmailAndPassword(id, mk)
                                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                @Override
                                                public void onComplete(@NonNull Task<AuthResult> task) {
                                                    if (task.isSuccessful()) {
                                                        // Sign in success, update UI with the signed-in user's information
                                                        FirebaseUser user = mAuth.getCurrentUser();
                                                        viewXuLyDangNhap.DangNhapThnahCong();
                                                    } else if (id.equals(accountLogin.getEmail()) && mk.equals(accountLogin.getDefaultPassword())) {
                                                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(accountLogin.getEmail(), accountLogin.getDefaultPassword())
                                                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                                                        if (task.isSuccessful()) {
                                                                            //Gán dữ liệu của tài khoản cũ sang tài khoản mới
                                                                            GanDuLieu();
                                                                            FirebaseDatabase.getInstance().getReference().child("users").child(task.getResult().getUser().getUid()).setValue(user);
                                                                        }
                                                                    }


                                                                });
                                                    } else {
                                                        viewXuLyDangNhap.DangNhapThatBai();
                                                    }
                                                }
                                            });
                                }*/















