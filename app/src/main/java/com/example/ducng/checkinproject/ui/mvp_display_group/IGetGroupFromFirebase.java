package com.example.ducng.checkinproject.ui.mvp_display_group;

import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IGetGroupFromFirebase {
    void getSucceed(List<Group> groups);
    void getFailue();
}
