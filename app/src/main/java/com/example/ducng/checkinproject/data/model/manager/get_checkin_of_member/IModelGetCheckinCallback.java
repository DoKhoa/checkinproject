package com.example.ducng.checkinproject.data.model.manager.get_checkin_of_member;

import com.example.ducng.checkinproject.data.model.CheckIn;

import java.util.List;

public interface IModelGetCheckinCallback {
    void getCheckinSuccess(String userId, List<CheckIn> checkIn);
    void getCheckinFaild();
}
