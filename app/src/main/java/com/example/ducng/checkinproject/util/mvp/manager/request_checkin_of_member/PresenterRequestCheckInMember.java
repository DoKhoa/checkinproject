package com.example.ducng.checkinproject.util.mvp.manager.request_checkin_of_member;

import android.util.Log;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.ui.home.manager.calender.DateOfCheckInAdapter;
import com.example.ducng.checkinproject.data.model.manager.get_checkin_of_member.IModelGetCheckinMember;
import com.example.ducng.checkinproject.data.model.manager.get_checkin_of_member.IModelGetCheckinCallback;
import com.example.ducng.checkinproject.data.model.manager.get_checkin_of_member.ModelGetCheckinOfMenber;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.IRequestCheckInOfManagerCallback;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PresenterRequestCheckInMember implements IPresenterRequestCheckInMember,
        IModelGetCheckinCallback {
    private IRequestCheckInOfManagerCallback iRequestCheckInOfManagerCallback;
    private IModelGetCheckinMember iModelGetCheckinMember;

    public PresenterRequestCheckInMember() {
    }

    public PresenterRequestCheckInMember(IRequestCheckInOfManagerCallback iRequestCheckInOfManagerCallback) {
        this.iRequestCheckInOfManagerCallback = iRequestCheckInOfManagerCallback;
    }

    @Override
    public void requestCheckInOfmanager(String userId) {
        iModelGetCheckinMember = new ModelGetCheckinOfMenber();
        iModelGetCheckinMember.getDataCheckininFribase(this, userId);
    }


    @Override
    public void getCheckinSuccess(String userId, List<CheckIn> checkIns) {
        if (checkIns == null) {
            iRequestCheckInOfManagerCallback.respondCheckinFalse();
        } else {
            sortCheckinList(checkIns);
            //Lấy ra ngày hiện taị để tìm kiếm trong list checkins
            Calendar calendar = Calendar.getInstance();
            int date = calendar.get(Calendar.DATE);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            month += 1;
            String dateNow = String.valueOf(year);
            if (month < 10) {
                dateNow += "0" + String.valueOf(month);
            } else {
                dateNow += String.valueOf(month);
            }
            if (date < 10) {
                dateNow += "0" + String.valueOf(date);
            } else {
                dateNow += String.valueOf(date);
            }

            //Tìm kiếm chekIn ngày hiện tại trong lít ngày
            CheckIn checkIn = DateOfCheckInAdapter.searchCheckin(dateNow, checkIns);
            if (checkIn != null) {
                iRequestCheckInOfManagerCallback.respondCheckinSuccessful(checkIn, checkIns);
            } else {
                checkIn = new CheckIn();
                checkIn.setDateId(dateNow);
                checkIn.setUserId(userId);
                checkIn.setStatus(0);
                iRequestCheckInOfManagerCallback.respondCheckinSuccessful(checkIn, checkIns);
            }
        }
    }

    @Override
    public void getCheckinFaild() {
        iRequestCheckInOfManagerCallback.respondCheckinFalse();
    }

    private void sortCheckinList(List<CheckIn> checkIns) {
        Collections.sort(checkIns, new Comparator<CheckIn>() {
            @Override
            public int compare(CheckIn o1, CheckIn o2) {
                return o1.getDateId().compareTo(o2.getDateId());
            }
        });
    }
}
