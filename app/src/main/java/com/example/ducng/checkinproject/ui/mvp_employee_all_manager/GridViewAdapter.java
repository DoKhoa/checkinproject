package com.example.ducng.checkinproject.ui.mvp_employee_all_manager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Group;
import com.example.ducng.checkinproject.data.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    List<User> users;
    Context context;
    LayoutInflater layoutInflater;

    public GridViewAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if(users == null){
            return 0;
        }
        else {
            return users.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        User user = users.get(i);
        final ViewHolder viewHolder;
        if(view == null){
            view = layoutInflater.inflate(R.layout.custom_employee_all, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.imgAvatar = view.findViewById(R.id.img_avatar);
            viewHolder.tvName = view.findViewById(R.id.tv_name);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
//        viewHolder.imgAvatar.setBackgroundResource(R.drawable.absent_color);
//        GlideApp.with(viewHolder.imgAvatar.getContext())
//                .load(user.getImage().toString())
//                .centerCrop()
//                .placeholder(R.drawable.shape_gray)
//               .error(R.drawable.shape_gray)
//                .into(viewHolder.imgAvatar);
        if (!user.getImage().isEmpty()) {
            Picasso.get()
                    .load(user.getImage())
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.ic_error_black)
                    .into(viewHolder.imgAvatar);
        }
        viewHolder.tvName.setText(users.get(i).getName());
        return view;
    }
    public void addAll(List<User> users) {
        //int prevSize = getCount();
        this.users.addAll(users);
        notifyDataSetChanged();
    }
    static class ViewHolder{
        ImageView imgAvatar;
        TextView tvName;
    }

}
