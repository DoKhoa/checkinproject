package com.example.ducng.checkinproject.util.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.example.ducng.checkinproject.R;

public class BaseViewActivity extends AppCompatActivity implements IBaseViewActivity {
    protected ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progressbar_custum);
        progressBar = findViewById(R.id.progressbar);

    }


    @Override
    public void setProgresbar(boolean show) {
        if (show = true) {
            progressBar.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.GONE);
        }
    }

}