package com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager;

import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public interface IViewRequestListMembe{
    void respondSuccessful(List<User> users);
    void respondFalse();
}
