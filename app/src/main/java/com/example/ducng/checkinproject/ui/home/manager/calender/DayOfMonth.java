package com.example.ducng.checkinproject.ui.home.manager.calender;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DayOfMonth {
    private int dayMax;
    private int indextStart;

    public DayOfMonth() {

    }

    /*
    Return day of weeek
        1 = Sunday
        2 = Monday
        3 = TueDay
        .........
        7 = Saturday
     */
    private int getDayOfWeek(int m, int y) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(y, m - 1, 1);
        indextStart = calendar.get(Calendar.DAY_OF_WEEK);
        return indextStart;
    }

    private int getDayMaxOfMonth(int month, int year) {
        int number = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                number = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                number = 30;
                break;
            case 2:
                if (year % 4 == 0 && year % 100 != 0) {
                    number = 29;
                } else number = 28;
                break;
            default:
                break;
        }
        Log.d("hjk", "month: " + month + "_" + number);
        return number;
    }

    private List<Integer> getListDateOfLastMonth(int month, int year, int indext) {
        ArrayList<Integer> listDay = new ArrayList<>();
        int dayMax;
        if (month == 1) {
            dayMax = getDayMaxOfMonth(12, year - 1);
        } else {
            dayMax = getDayMaxOfMonth(month - 1, year);
        }
        for (int i = 0; i < indext - 1; i++) {
            listDay.add(dayMax - ((indext - 1) - 1) + i);
        }
        return listDay;
    }

    private List<Integer> getListDateOfNextMonth(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        int numberDay = calendar.get(Calendar.DAY_OF_WEEK);
        ArrayList<Integer> listDay = new ArrayList<>();
        if (numberDay!=1) {
            for (int i = 0; i <= 7 - numberDay; i++) {
                listDay.add(i + 1);
            }
        }
        return listDay;
    }

    public int getIndexStart() {
        return indextStart-1;
    }

    public int getIndexEnd(){
        return indextStart+dayMax-2;
    }

    public int getDayMax() {
        return dayMax;
    }

    public List<Integer> createListDayOfMonth(int month, int year) {
        dayMax = getDayMaxOfMonth(month, year);
        int index = getDayOfWeek(month, year);
        List<Integer> daysOfMonth = getListDateOfLastMonth(month, year, index);
        for (int i = 1; i <= dayMax; i++) {
            daysOfMonth.add(i);
        }
        daysOfMonth.addAll(getListDateOfNextMonth(month, year));
        return daysOfMonth;
    }
}
