package com.example.ducng.checkinproject.models.model_employee_of_group;

public class EventEmployeeOfGroupMessage {
    private String message;

    public EventEmployeeOfGroupMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
