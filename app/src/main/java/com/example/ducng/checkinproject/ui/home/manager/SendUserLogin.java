package com.example.ducng.checkinproject.ui.home.manager;

import com.example.ducng.checkinproject.data.model.User;

public class SendUserLogin {
    private User user;
    public SendUserLogin(User user){
        this.user=user;
    }

    public User getUser() {
        return user;
    }
}
