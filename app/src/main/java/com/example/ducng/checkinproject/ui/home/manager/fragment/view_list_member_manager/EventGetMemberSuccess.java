package com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager;

import com.example.ducng.checkinproject.data.model.User;

import java.util.List;

public class EventGetMemberSuccess {
    private List<User> users;
    public EventGetMemberSuccess(List<User> users){
        this.users=users;
    }

    public List<User> getUsers() {
        return users;
    }
}
