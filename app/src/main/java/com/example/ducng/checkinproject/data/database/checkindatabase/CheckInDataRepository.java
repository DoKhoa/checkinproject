package com.example.ducng.checkinproject.data.database.checkindatabase;

import android.support.annotation.NonNull;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.data.model.EventMessage;
import com.example.ducng.checkinproject.ui.edit_create_user.EventUser;
import com.example.ducng.checkinproject.ui.home.checkin.EventCheckin;
import com.example.ducng.checkinproject.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.Date;

public class CheckInDataRepository {
    private static final String CHECK_IN_DATABASE = "checkin";
    private static CheckInDataRepository checkInDataRepository;
    private DatabaseReference databaseReference;

    private CheckInDataRepository() {
        databaseReference = FirebaseDatabase.getInstance().getReference(CHECK_IN_DATABASE);
    }

    public static synchronized CheckInDataRepository getInstance() {
        if (checkInDataRepository == null) {
            checkInDataRepository = new CheckInDataRepository();
        }
        return checkInDataRepository;
    }

    public void getCheckIn(String uid, int day, final GetCheckInCallback callBack) {
        databaseReference.child(uid).child(Utils.getDate(day)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(CheckIn.class) != null) {
                    CheckIn checkIn = dataSnapshot.getValue(CheckIn.class);
                    callBack.onSuccess(checkIn);
                } else {
                    callBack.onFailure(new Throwable());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callBack.onFailure(new Throwable("Get CheckIn Error"));
            }
        });

    }
    public void getCheckInUseEventBus(final String uid, final int day) {
        databaseReference.child(uid).child(Utils.getDate(day)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(CheckIn.class) != null) {
                    CheckIn checkIn = dataSnapshot.getValue(CheckIn.class);
//                    callBack.onSuccess(checkIn);
                    EventBus.getDefault().post(new EventCheckin(checkIn,EventCheckin.KEY_GET));
                } else {
//                    callBack.onFailure(new Throwable());
                    EventBus.getDefault().post(new EventCheckin(EventCheckin.KEY_GET_ERROR,uid,day));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                EventBus.getDefault().post(new EventCheckin(EventCheckin.KEY_GET_ERROR,uid,day));
            }
        });

    }

    public void updateCheckIn(String uid, int day, CheckIn checkIn, final UpdateCheckInCallback callback) {
        databaseReference.child(uid).child(Utils.getDate(day)).setValue(checkIn).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onSuccess();
                } else {
                    callback.onFailure(new Throwable("Update CheckIn Error"));
                }
            }
        });
    }
    public void updateCheckInUseEventBus(String uid, int day, CheckIn checkIn) {
        databaseReference.child(uid).child(Utils.getDate(day)).setValue(checkIn).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
//                    callback.onSuccess();
                    EventBus.getDefault().post(new EventMessage("update success"));
                } else {
                    EventBus.getDefault().post(new EventMessage("update error"));
                }
            }
        });
    }

    public interface GetCheckInCallback {
        void onSuccess(CheckIn checkIn);

        void onFailure(Throwable throwable);

    }

    public interface UpdateCheckInCallback {
        void onSuccess();

        void onFailure(Throwable throwable);

    }
}
