package com.example.ducng.checkinproject.ui.mvp_display_group;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.User;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserOffGroupRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    IUserOffGroupRecyclerAdapter mUserOffGroupRecyclerAdapter;
    //LoadMore loadMore;
    List<User> users;
    List<User> userDisplays;
    private final int TYPE_EMPLOYEE = 0;
    private final int TYPE_LEADER = 1;
    private final int TYPE_LOAD_MORE = 2;

    public UserOffGroupRecyclerAdapter(IUserOffGroupRecyclerAdapter mUserOffGroupRecyclerAdapter,
                                       List<User> users, List<User> userDisplay) {
        this.mUserOffGroupRecyclerAdapter = mUserOffGroupRecyclerAdapter;
        this.users = users;
        this.userDisplays = userDisplay;
    }

    @Override
    public int getItemViewType(int position) {
        int type;
        int size = users.size();
        if (position == 0) {
            return TYPE_LEADER;
        } else if (position == size - 1 && position == 3) {
            if(userDisplays.size() == 4) {
                return TYPE_EMPLOYEE;
            }
            return TYPE_LOAD_MORE;
//
        } else {
            return TYPE_EMPLOYEE;
        }
//        switch (position) {
//            case 0:
//                type = TYPE_LEADER;
//                break;
//            case 3:
//                if(users.get(position) != null){
//                    type = TYPE_LOAD_MORE;
//                }else {
//                    type = TYPE_EMPLOYEE;
//                }
//                break;
//            default:
//                type = TYPE_EMPLOYEE;
//                break;
//        }
//        return type;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case TYPE_LEADER:
                view = inflater.inflate(R.layout.custom_item_big_user_of_group, viewGroup, false);
                return new UserOffGroupViewHolderBig(view);
            case TYPE_EMPLOYEE:
                view = inflater.inflate(R.layout.custom_item_user_of_group, viewGroup, false);
                return new UserOffGroupViewHolder(view);
            case TYPE_LOAD_MORE:
                view = inflater.inflate(R.layout.custom_item_load_more, viewGroup, false);
                return new LoadMoreViewHolder(view);
            default:
                throw new IllegalArgumentException("Invalid item type: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {

        final User user = users.get(position);
        //use glide to display image from firebase user.getImage()
        //userOffGroupViewHolder.imgAvatarUserOfGroup.setBackgroundResource(R.drawable.absent_color);
//        GlideApp.with(userOffGroupViewHolder.imgAvatarUserOfGroup.getContext())
//                .load(user.getImage().toString())
//                .centerCrop()
//                .placeholder(R.drawable.shape_gray)
//                .error(R.drawable.shape_gray)
//                .into(userOffGroupViewHolder.imgAvatarUserOfGroup);


        if (viewHolder.getItemViewType() == TYPE_LEADER) {
            UserOffGroupViewHolderBig userOffGroupViewHolderBig = (UserOffGroupViewHolderBig) viewHolder;
            //userOffGroupViewHolderBig.imgAvatarUserBigOfGroup.setBackgroundResource(R.drawable.absent_color);
            if (!user.getImage().isEmpty()) {
                Picasso.get()
                        .load(user.getImage())
                        .resize(200, 150)
                        .centerCrop()
                        .error(R.drawable.ic_error_black)
                        .into(userOffGroupViewHolderBig.imgAvatarUserBigOfGroup);
            }
//            userOffGroupViewHolderBig.imgAvatarUserBigOfGroup.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    mUserOffGroupRecyclerAdapter.onClickToEmployee(user);
//                }
//            });
            userOffGroupViewHolderBig.imgAvatarUserBigOfGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(user);
                }
            });
        } else if (viewHolder.getItemViewType() == TYPE_EMPLOYEE) {
            UserOffGroupViewHolder userOffGroupViewHolder = (UserOffGroupViewHolder) viewHolder;
//            userOffGroupViewHolder.imgAvatarUserOfGroup.setBackgroundResource(R.drawable.absent_color);
            if (!user.getImage().isEmpty()) {
                Picasso.get()
                        .load(user.getImage())
                        .resize(200, 200)
                        .centerCrop()
                        .error(R.drawable.ic_error_black)
                        .into(userOffGroupViewHolder.imgAvatarUserOfGroup);
            }
//            userOffGroupViewHolder.imgAvatarUserOfGroup.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    mUserOffGroupRecyclerAdapter.onClickToEmployee(user);
//                }
//            });
            userOffGroupViewHolder.imgAvatarUserOfGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(user);
                }
            });
        } else {
            LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) viewHolder;
            loadMoreViewHolder.imgLoadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    users.clear();
                    users.addAll(userDisplays);
                    notifyDataSetChanged();
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        if (users == null) {
            return 0;
        } else {
            return users.size();
        }
    }

    public static class UserOffGroupViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_avatar_user_of_group)
        ImageView imgAvatarUserOfGroup;

        public UserOffGroupViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class UserOffGroupViewHolderBig extends RecyclerView.ViewHolder {
        @BindView(R.id.img_avatar_user_big_of_group)
        ImageView imgAvatarUserBigOfGroup;

        public UserOffGroupViewHolderBig(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

//    public void setLoadMore(LoadMore loadMore) {
//        this.loadMore = loadMore;
//    }

    public static class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_load_more)
        ImageView imgLoadMore;

        public LoadMoreViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface IUserOffGroupRecyclerAdapter {
      //  void onClickToEmployee(User user);
    }
//    interface LoadMore{
//        void loadMore();
//    }
}
