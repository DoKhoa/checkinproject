package com.example.ducng.checkinproject.data.model.manager.update_comment_of_id;

import com.example.ducng.checkinproject.data.model.CheckIn;

import java.util.List;

public interface IModelUpdateCommentCallback {
    void updateCommentSuccessly(List<CheckIn> checkIns);
    void updateCommentFailt();
}
