package com.example.ducng.checkinproject.ui.mvp_common_employee_manager;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.ui.mvp_display_group.DisplayGroupFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_all_manager.EmployeeAllManagerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommonEmployeeManager extends Fragment
        implements RadioGroup.OnCheckedChangeListener {

    DisplayGroupFragment displayGroupFragment;
    EmployeeAllManagerFragment employeeAllManagerFragment;

    @BindView(R.id.rd_choose_view_method)
    RadioGroup radioGroup;
    @BindView(R.id.rd_button_group)
    RadioButton rdButtonGroup;
    @BindView(R.id.rd_button_employee)
    RadioButton rdButtonEmployee;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_common_employee_manager,container, false);
        ButterKnife.bind(this,view);
        inits();
        return view;
    }

    private void inits() {
        displayGroupFragment = new DisplayGroupFragment();
        employeeAllManagerFragment = new EmployeeAllManagerFragment();
        rdButtonGroup.setChecked(true);
        setFragment(displayGroupFragment);
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.rd_button_group:
                setFragment(displayGroupFragment);
                break;
            case R.id.rd_button_employee:
                setFragment(employeeAllManagerFragment);
                break;
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        // replace
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.content_employee_manager, fragment);
        fragmentTransaction.commit();
    }
}
