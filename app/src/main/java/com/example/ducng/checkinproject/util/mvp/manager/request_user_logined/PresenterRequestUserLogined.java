package com.example.ducng.checkinproject.util.mvp.manager.request_user_logined;

import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.data.model.manager.get_user_logined.IModelGetUserCallback;
import com.example.ducng.checkinproject.data.model.manager.get_user_logined.IModelRequestUser;
import com.example.ducng.checkinproject.data.model.manager.get_user_logined.ModelGetUser;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.IViewRequestUserCheckined;

public class PresenterRequestUserLogined implements IPresenterRequestUserLogined{
    private IViewRequestUserCheckined iViewRequestUserCheckined;
    private IModelRequestUser iModelRequestUser;

    public PresenterRequestUserLogined() {
    }

    @Override
    public void requestUser() {
        iModelRequestUser= new ModelGetUser();
        iModelRequestUser.getUser();
    }

}
