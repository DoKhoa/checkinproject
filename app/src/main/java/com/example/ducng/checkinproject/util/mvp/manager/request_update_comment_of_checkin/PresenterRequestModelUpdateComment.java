package com.example.ducng.checkinproject.util.mvp.manager.request_update_comment_of_checkin;

import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.data.model.manager.update_comment_of_id.IModelUpdateComment;
import com.example.ducng.checkinproject.data.model.manager.update_comment_of_id.IModelUpdateCommentCallback;
import com.example.ducng.checkinproject.data.model.manager.update_comment_of_id.ModelUpdateComment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member.IRequestUpdateComentCallback;

import java.util.List;

public class PresenterRequestModelUpdateComment implements IModelUpdateCommentCallback,
        IPesenterRequestUpdateComment {
    private IModelUpdateComment iModelUpdateComment;
    private IRequestUpdateComentCallback iRequestUpdateComentCallback;

    public PresenterRequestModelUpdateComment(IRequestUpdateComentCallback iRequestUpdateComentCallback) {
        this.iRequestUpdateComentCallback = iRequestUpdateComentCallback;
    }

    @Override
    public void updateCommentSuccessly(List<CheckIn> checkIns) {
        iRequestUpdateComentCallback.updateSeccess(checkIns);
    }

    @Override
    public void updateCommentFailt() {
        iRequestUpdateComentCallback.updateFailt();
    }

    @Override
    public void updateComment(String userId, String dateCheckin, String coment) {
        iModelUpdateComment = new ModelUpdateComment();
        iModelUpdateComment.updateComment(this, userId, dateCheckin, coment);
    }
}
