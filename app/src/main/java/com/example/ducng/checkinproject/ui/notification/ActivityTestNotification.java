package com.example.ducng.checkinproject.ui.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.Notification;
import com.example.ducng.checkinproject.ui.edit_create_user.EditAndCreateUserFragment;
import com.example.ducng.checkinproject.ui.login.LoginActivity;
import com.example.ducng.checkinproject.util.Constaint;
import com.example.ducng.checkinproject.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ActivityTestNotification extends AppCompatActivity implements MyResultReceiver.Receiver {
    public MyResultReceiver mReceiver;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_notification);


        //set up for result receiver
        mReceiver = new MyResultReceiver(new Handler());
        mReceiver.setReceiver(this);
        //
        FirebaseAuth.getInstance().signInWithEmailAndPassword("staff08@gmail.com","123456").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
//                    EditAndCreateUserFragment editAndCreateUserFragment = new EditAndCreateUserFragment(EditAndCreateUserFragment.TYPE_EDIT);
//                    FragmentTransaction fragmentTransaction =getSupportFragmentManager().beginTransaction();
//                    fragmentTransaction.add(R.id.main_frame,editAndCreateUserFragment);
//                    fragmentTransaction.commit();
                    Intent intent = new Intent(getBaseContext(), MyService.class);
                    intent.putExtra("receiverTag", mReceiver);
                    startService(intent);
                }
            }
        });
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNotification("Q5LBDJMGO8as8nFy67VtjFoTTMt2", "Android Developer"
                        , new SendNotification() {
                            @Override
                            public void onSuccess() {
                                Toast.makeText(ActivityTestNotification.this,"send notification on success",Toast.LENGTH_SHORT);
                            }

                            @Override
                            public void onFailure() {
                                Toast.makeText(ActivityTestNotification.this,"send notification on failure",Toast.LENGTH_SHORT);
                            }

                            @Override
                            public void onNetworkFailure() {
                                Toast.makeText(ActivityTestNotification.this,"send notification on networkFailure",Toast.LENGTH_SHORT);
                            }
                        });
            }
        });

    }

    public void sendNotification(String idUser, String nameGroup, final SendNotification callBack) {
        Notification notification = new Notification();
        notification.setIdUser(idUser);
        notification.setBody("Bạn đã được xét làm trưởng nhóm "+nameGroup);
        notification.setTitle("Chúc mừng");
        notification.setShow(false);
        FirebaseDatabase.getInstance().getReference().child("notifications").child(idUser)
                .push().setValue(notification).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callBack.onSuccess();
                } else {
                    callBack.onFailure();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callBack.onNetworkFailure();
            }
        });
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == MyService.RESULT_CODE_NOTIFICATION) {
            Notification notification = Utils.convertGsonToObject(resultData.getString(MyService.NOTIFICATION),Notification.class);
            String key = resultData.getString(MyService.KEY_NOTIFICATION);
            setNotification(notification.getTitle(),notification.getBody());
            FirebaseDatabase.getInstance().getReference().child("notifications").child(notification.getIdUser()).child(key).child("show").setValue(true);
        }
    }
    private void setNotification(String title,String body) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_stat_ic_notification)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
    public interface SendNotification {
        void onSuccess();
        void onFailure();
        void onNetworkFailure();
    }

}
