package com.example.ducng.checkinproject.data.model.manager.get_list_member;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.EventGetMemberFailt;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.EventGetMemberSuccess;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ModelGetListMember implements IModelGetMember {
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private List<User> users;

    public ModelGetListMember() {
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void getUserInGroupToFirebase(final String groupId) {
        users = new ArrayList<>();
        reference = database.getReference("users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    assert user != null;
                    if (user.getGroupId().equals(groupId)){
                        users.add(user);
                    }
                }
                sortUserList(users);
                EventBus.getDefault().post(new EventGetMemberSuccess(users));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                EventBus.getDefault().post(new EventGetMemberFailt("failt"));

            }
        });
    }
    private void sortUserList(List<User> users) {
        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }
}

