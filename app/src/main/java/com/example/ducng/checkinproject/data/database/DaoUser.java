package com.example.ducng.checkinproject.data.database;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.ducng.checkinproject.data.model.EventMessage;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.edit_create_user.EventUser;
import com.example.ducng.checkinproject.util.NetworkHelper;
import com.example.ducng.checkinproject.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

public class DaoUser {
    private static final String DATABASE_USER = "users";
    private NetworkHelper networkHelper;
    private static DaoUser daoUser;
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListenerForGetUser;
    private ValueEventListener valueEventListenerForCreateUser;

    public static synchronized DaoUser getInstance(NetworkHelper networkHelper) {
        if (daoUser == null) {
            daoUser = new DaoUser(networkHelper);
        }
        return daoUser;
    }

    public DaoUser(NetworkHelper networkHelper) {
        this.networkHelper = networkHelper;
        databaseReference = FirebaseDatabase.getInstance().getReference(DATABASE_USER);
    }


    public void getUserById(String id, Context context, final GetUserCallBack callBack) {
        if (networkHelper.isNetworkAvailable(context)) {
            valueEventListenerForGetUser = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue(User.class) != null) {
                        User user = dataSnapshot.getValue(User.class);
                        callBack.onSuccess(user);
                    } else {
                        callBack.onFailure(new Throwable());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    callBack.onFailure(new Throwable());
                }
            };
            databaseReference.child(id).addValueEventListener(valueEventListenerForGetUser);
        } else {
            callBack.onNetworkFailure();
        }
    }
    public void getUserByIdUseEventBus(String id,Context context) {
        if (networkHelper.isNetworkAvailable(context)) {
            valueEventListenerForGetUser = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue(User.class) != null) {
                        User user = dataSnapshot.getValue(User.class);
                        EventBus.getDefault().post(new EventUser(user,EventUser.KEY_GET));
//                        callBack.onSuccess(user);
                    } else {
//                        callBack.onFailure(new Throwable());
                        EventBus.getDefault().post(new EventMessage("Lỗi lấy user"));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
//                    callBack.onFailure(new Throwable());
                    EventBus.getDefault().post(new EventMessage("cancel lấy user"));
                }
            };
            databaseReference.child(id).addValueEventListener(valueEventListenerForGetUser);
        } else {
            EventBus.getDefault().post(new EventMessage("Lỗi mạng"));
        }
    }
//    public void createUser(User user, final CreateUserCallBack callBack, Context context) {
//        if (networkHelper.isNetworkAvailable(context)) {
//            FirebaseAuth.getInstance().createUserWithEmailAndPassword(user.getEmail(), Utils.getSaltString()).addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
//                @Override
//                public void onComplete(@NonNull Task<AuthResult> task) {
//                    if (task.isSuccessful()) {
//
//                    }
//                }
//            });
//        } else {
//            callBack.onNetworkFailure();
//        }
//    }
    public void updateUser(final User user, final CreateUserCallBack callBack, Context context) {
        if (networkHelper.isNetworkAvailable(context)) {
            FirebaseDatabase.getInstance().getReference().child("users").child(Utils.getIdUserCurrent()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        callBack.onSuccess(user);
                    } else {
                        callBack.onFailure(new Throwable("Loi update user"));
                    }
                }
            });
        } else {
            callBack.onNetworkFailure();
        }
    }
    public void updateUserUseEventBus(final User user, Context context) {
        if (networkHelper.isNetworkAvailable(context)) {
            FirebaseDatabase.getInstance().getReference().child("users").child(Utils.getIdUserCurrent()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        EventBus.getDefault().post(new EventUser(user, EventUser.KEY_EDIT));
                        EventBus.getDefault().post(new EventMessage("update thanh cong"));
                    } else {
                        EventBus.getDefault().post(new EventMessage("Lỗi update user"));
                    }
                }
            });
        } else {
            EventBus.getDefault().post(new EventMessage("Lỗi mạng"));
        }
    }
    public void createUser(final User user, final  CreateUserCallBack callBack, Context context) {
        if (networkHelper.isNetworkAvailable(context)) {
            user.setDefaultPassword(Utils.getSaltString());
            checkEmailExists(user.getEmail(), new CheckEmailCallBack() {
                @Override
                public void onSuccess() {
                    FirebaseDatabase.getInstance().getReference().child("newaccounts").push().setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                callBack.onSuccess(user);
                            } else {
                                callBack.onFailure(new Throwable("Loi update user"));
                            }
                        }
                    });
                }

                @Override
                public void onFailure() {
                    callBack.onFailure(new Throwable("Email đã tồn tại"));
                }
            });

        } else {
            callBack.onNetworkFailure();
        }
    }
    public void createUserUseEventBus(final User user, Context context) {
        if (networkHelper.isNetworkAvailable(context)) {
            user.setDefaultPassword(Utils.getSaltString());
            checkEmailExists(user.getEmail(), new CheckEmailCallBack() {
                @Override
                public void onSuccess() {
                    FirebaseDatabase.getInstance().getReference().child("newaccounts").push().setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                EventBus.getDefault().post(new EventUser(user, EventUser.KEY_CREATE));
                            } else {
                                EventBus.getDefault().post(new EventMessage("Lỗi create user"));
                            }
                        }
                    });
                }

                @Override
                public void onFailure() {
                    EventBus.getDefault().post(new EventMessage("Mail da ton tai"));
                }
            });

        } else {
            EventBus.getDefault().post(new EventMessage("Loi mang"));
        }
    }
    private void checkEmailExists(final String email, final CheckEmailCallBack callBack) {
       FirebaseDatabase.getInstance().getReference().child("users").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if (dataSnapshot.exists()) {
                   callBack.onFailure();
               } else {
                   FirebaseDatabase.getInstance().getReference().child("newaccounts").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
                       @Override
                       public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                           if (dataSnapshot.exists()) {
                               callBack.onFailure();
                           } else {
                               callBack.onSuccess();
                           }
                       }

                       @Override
                       public void onCancelled(@NonNull DatabaseError databaseError) {

                       }
                   });
               }
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
    }
    public interface CheckEmailCallBack{
        void onSuccess();

        void onFailure();

    }
    public interface GetUserCallBack {
        void onSuccess(User user);

        void onFailure(Throwable throwable);

        void onNetworkFailure();
    }
    public interface  CreateUserCallBack {
        void onSuccess(User user);

        void onFailure(Throwable throwable);

        void onNetworkFailure();
    }
}
