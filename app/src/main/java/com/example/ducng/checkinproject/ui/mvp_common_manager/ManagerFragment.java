package com.example.ducng.checkinproject.ui.mvp_common_manager;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.ui.home.checkin.CheckInFragment;
import com.example.ducng.checkinproject.ui.mvp_common_employee_manager.CommonEmployeeManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManagerFragment extends Fragment implements View.OnClickListener {

    CommonEmployeeManager commonEmployeeManager;
    CheckInFragment checkInFragment;

    @BindView(R.id.img_view_work_time)
    ImageView imgWorkTime;
    @BindView(R.id.tv_work_time)
    TextView tvWorkTime;
    @BindView(R.id.img_view_employee_manager)
    ImageView imgEmployeeManager;
    @BindView(R.id.tv_employee_manager)
    TextView tvEmployeeManage;
    @BindView(R.id.frame_manager)
    FrameLayout frameManager;
    private View viewHolder;

    @Override
    public void onDestroyView() {
        viewHolder = getView();
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (viewHolder==null ) {
            view = inflater.inflate(R.layout.fragment_manager, container, false);
            ButterKnife.bind(this, view);
            inits();
        } else {
            view = viewHolder;
            ButterKnife.bind(this, view);
        }

        return view;
    }

    private void inits() {
        commonEmployeeManager = new CommonEmployeeManager();
        checkInFragment = new CheckInFragment();
        setFragment(checkInFragment);
        imgWorkTime.setOnClickListener(this);
        imgEmployeeManager.setOnClickListener(this);
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        // replace
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.content_manager, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_view_work_time:
                setFragment(checkInFragment);
                tvWorkTime.setTextColor(Color.WHITE);
                tvEmployeeManage.setTextColor(Color.BLACK);
                frameManager.setBackgroundColor(getResources().getColor(R.color.colorUnselect));
                imgWorkTime.setBackgroundResource(R.drawable.shape_image_view_actionbar_yellow);
                imgEmployeeManager.setBackgroundResource(R.drawable.shape_image_view_actionbar_gray);
                break;
            case R.id.img_view_employee_manager:
                setFragment(commonEmployeeManager);
                tvWorkTime.setTextColor(Color.BLACK);
                tvEmployeeManage.setTextColor(Color.WHITE);
                imgWorkTime.setBackgroundResource(R.drawable.shape_image_view_actionbar_gray);
                frameManager.setBackgroundColor(getResources().getColor(R.color.colorSelect));
                imgEmployeeManager.setBackgroundResource(R.drawable.shape_image_view_actionbar_yellow);
                break;
        }
    }
}
