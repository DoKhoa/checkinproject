package com.example.ducng.checkinproject.util.mvp.manager.request_user_logined;

public interface IPresenterRequestUserLogined {
    void requestUser();
}
