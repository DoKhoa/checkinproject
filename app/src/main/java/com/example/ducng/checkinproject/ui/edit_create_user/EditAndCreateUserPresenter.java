package com.example.ducng.checkinproject.ui.edit_create_user;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.DatePicker;

import com.example.ducng.checkinproject.BuildConfig;
import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.database.DaoUser;
import com.example.ducng.checkinproject.data.model.EventMessage;
import com.example.ducng.checkinproject.data.model.Mail;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.util.Constaint;
import com.example.ducng.checkinproject.util.Utils;
import com.example.ducng.checkinproject.util.mvp.BasePresenter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class EditAndCreateUserPresenter extends BasePresenter<EditAndCreateUserContract.View> implements EditAndCreateUserContract.Presenter {

    private static final String TAG = "EditAndCreateUserPresenter";
    private DaoUser daoUser;
    final CharSequence[] options = {"Camera", "Gallery"};
    private static String[] PERMISSIONS = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    public static final int IMAGE_GALLERY_REQUEST = 1;
    public static final int IMAGE_CAMERA_REQUEST = 2;
    private File pathImageCamera;
    String stDateFormat = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
    FirebaseStorage storage = FirebaseStorage.getInstance();


    public EditAndCreateUserPresenter(EditAndCreateUserContract.View view, DaoUser daoUser) {
        this.view = view;
        this.daoUser = daoUser;
    }




    @Override
    public void getUser(final Context context) {
        if (view == null) {
            return;
        }
        view.setProgressBar(true);
//        daoUser.getUserById(FirebaseAuth.getInstance().getUid(), context, new DaoUser.GetUserCallBack() {
//            @Override
//            public void onSuccess(User user) {
//                if (view != null) {
//                    view.setupViewWithUser(user);
//                    view.setProgressBar(false);
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//                if (view != null) {
//                    view.setProgressBar(false);
//                    view.showToastMessage(context.getString(R.string.error_msg));
//                }
//            }
//
//            @Override
//            public void onNetworkFailure() {
//                if (view != null) {
//                    view.setProgressBar(false);
//                    view.showToastMessage(context.getString(R.string.network_failure_msg));
//                }
//            }
//        });
        daoUser.getUserByIdUseEventBus(FirebaseAuth.getInstance().getUid(),context);
    }

    @Override
    public void updateUser(User user, Context context) {
        if (view == null) {
            return;
        }
        view.setProgressBar(true);
//        daoUser.updateUser(user, new DaoUser.CreateUserCallBack() {
//            @Override
//            public void onSuccess(User user) {
//                view.showToastMessage("Update user successful");
//                view.setAvatarHome(user.getImage());
//                view.setProgressBar(false);
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//                view.showToastMessage(throwable.getMessage());
//                view.setProgressBar(false);
//            }
//
//            @Override
//            public void onNetworkFailure() {
//                view.showToastMessage("Error network");
//                view.setProgressBar(false);
//            }
//        }, context);
        daoUser.updateUserUseEventBus(user,context);
    }

    @Override
    public void createUser(final User user, Context context) {
        if (view == null) {
            return;
        }
        view.setProgressBar(true);
//        daoUser.createUser(user, new DaoUser.CreateUserCallBack() {
//            @Override
//            public void onSuccess(User user) {
//                view.showToastMessage("Create user successful");
//                sendEmail(user.getEmail(),user.getDefaultPassword());
//                view.clearAll();
//                view.setProgressBar(false);
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//                view.showToastMessage(throwable.getMessage());
//                view.setProgressBar(false);
//            }
//
//            @Override
//            public void onNetworkFailure() {
//                view.showToastMessage("Error network");
//                view.setProgressBar(false);
//            }
//        }, context);
        daoUser.createUserUseEventBus(user,context);
    }



    @SuppressLint("LongLogTag")
    @Override
    public void sendEmail(String email,String defaultPassword) {
//        Log.i("Send email", "");
//
//        String[] TO = {email};
//        String[] CC = {"something"};
//        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setData(Uri.parse("mailto:"));
//        emailIntent.setType("text/plain");
//
//
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
//        emailIntent.putExtra(Intent.EXTRA_CC, CC);
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tài khoản ứng dụng Check In");
//        emailIntent.putExtra(Intent.EXTRA_TEXT, "Tài khoản: "+email+" mật khẩu: "+defaultPassword);
//
//        try {
//            view.startAct(Intent.createChooser(emailIntent, "Send mail..."));
//            Log.i("Finished sending email...", "");
//        } catch (android.content.ActivityNotFoundException ex) {
//            view.showToastMessage( "There is no email client installed.");
//        }
        try {

            try {
                //send mail to yourmail@gmail.com
                Mail m = new Mail(Constaint.EMAIL_SEND, Constaint.PASSWORD_EMAIL_SEND);// create new mail object
                String[] toArr = {email};
                m.setTo(toArr);//set destionation mail
                m.setFrom(Constaint.EMAIL_SEND);//set source mail
                m.setSubject("Your account on App Check in");//set mail subject
                m.setBody("Your id: "+email+"\nYour password: "+defaultPassword+"\nOpen app: www.abc.com/id=1");// set mail content
                //send mail:
                if (m.send()) {
//                    Log.e("send mail", "OKE");
                    view.showToastMessage("Send mail success");
                } else {
//                    Log.e("send mail", "ERROR");
                    view.showToastMessage("Error send mail");
                }

            } catch (Exception e) {
                Log.d("Exception caught", e.getMessage());
                view.showToastMessage("Exception send mail");
            }

        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
            view.showToastMessage("Exception send mail");
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data, Context context) {
        StorageReference storageRef = storage.getReferenceFromUrl(Utils.URL_STORAGE_REFERENCE).child(Utils.FOLDER_AVATAR_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    sendFile(storageRef, selectedImageUri);
//                      view.setImageUser(selectedImageUri);
                } else {

                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (pathImageCamera != null && pathImageCamera.exists()) {
                    StorageReference imageCameraRef = storageRef.child(pathImageCamera.getName() + "_camera");
                    sendFile(imageCameraRef, pathImageCamera, context);
                } else {

                }
            }
        }
    }


    public void changeAvatar(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose Source ");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    if (view.allowStoragePermissions(PERMISSIONS, REQUEST_EXTERNAL_STORAGE)) {
                        photoCameraIntent(context);
                    }
                }
                if (options[item].equals("Gallery")) {
                    photoGallery(context);
                }
            }
        });
        builder.show();
    }

    @Override
    public void showTimePicker(Context context) {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub
                /*      Your code   to get date and time    */
                selectedmonth = selectedmonth + 1;
//                eReminderDate.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                view.setDayOfBirth("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select Date");
        mDatePicker.show();
    }

    @Override
    public void showGenderPicker(Context context) {
        final CharSequence[] gender = {EditAndCreateUserFragment.GENDER_MALE, EditAndCreateUserFragment.GENDER_FEMALE};
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Giới tính");
        alert.setSingleChoiceItems(gender, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (gender[which] == EditAndCreateUserFragment.GENDER_MALE) {
                    view.setGender(EditAndCreateUserFragment.GENDER_MALE);
                    dialog.dismiss();
                } else if (gender[which] == EditAndCreateUserFragment.GENDER_FEMALE) {
                    view.setGender(EditAndCreateUserFragment.GENDER_FEMALE);
                    dialog.dismiss();
                }
            }
        });
        alert.show();

    }



    private void photoGallery(Context context) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        view.startActivity(Intent.createChooser(intent, context.getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }

    private void photoCameraIntent(Context context) {

        pathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), stDateFormat + "camera.jpg");
        if (pathImageCamera.exists()) {
            pathImageCamera.delete();
        } else {
            pathImageCamera.getParentFile().mkdirs();
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI = FileProvider.getUriForFile(context,
                BuildConfig.APPLICATION_ID + ".provider",
                pathImageCamera);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ClipData clip =
                    ClipData.newUri(context.getContentResolver(), "A photo", photoURI);

            intent.setClipData(clip);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            List<ResolveInfo> resInfoList =
                    context.getPackageManager()
                            .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, photoURI,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        }
        view.startActivity(intent, IMAGE_CAMERA_REQUEST);
    }


    private void sendFile(StorageReference storageReference, final File file, Context context) {
        if (storageReference != null) {
            view.setProgressBar(true);
            Uri photoURI = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @SuppressLint("LongLogTag")
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Log.e(TAG, uri.toString());
                            view.setImageUser(uri.toString());
                            view.setLinkAvatar(uri.toString());
                            view.setProgressBar(false);
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    view.setProgressBar(false);
                    view.showToastMessage("Lỗi tải ảnh");
                }
            });
        }
    }


    private void sendFile(StorageReference storageReference, final Uri file) {
        if (storageReference != null) {
            view.setProgressBar(true);
            StorageReference imageGalleryRef = storageReference.child(stDateFormat + "_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    view.setProgressBar(false);
                    view.showToastMessage("Lỗi tải ảnh");
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @SuppressLint("LongLogTag")
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Log.e(TAG, uri.toString());
                            view.setImageUser(uri.toString());
                            view.setLinkAvatar(uri.toString());
                            view.setProgressBar(false);
                        }
                    });

                }
            });
        }
    }

}
