package com.example.ducng.checkinproject.ui.mvp_add_employee;

public interface IAddEmployeeFirebase {
    void addEmployeeSucceed();
    void addEmployeeFailue();
}
