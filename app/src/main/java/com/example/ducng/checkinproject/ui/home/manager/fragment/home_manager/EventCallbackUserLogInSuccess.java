package com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager;

import com.example.ducng.checkinproject.data.model.User;

public class EventCallbackUserLogInSuccess {
    private User user;
    public EventCallbackUserLogInSuccess(User user){
        this.user= user;
    }

    public User getUser() {
        return user;
    }
}
