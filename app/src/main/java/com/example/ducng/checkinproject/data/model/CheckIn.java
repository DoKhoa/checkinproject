package com.example.ducng.checkinproject.data.model;

public class CheckIn {
    private String userId;
    private String dateId;
    /**
     * 0: Nghi lam
     * 1: Lam buoi sang
     * 2: Lam buoi chieu
     * 3: Lam ca ngay
     */
    private int status;
    private String lyDo;
    private String nhanXet;

    public CheckIn() {
    }

    public CheckIn(String userId, String dateId, int status) {
        this.userId = userId;
        this.dateId = dateId;
        this.status = status;
    }
    public CheckIn(String userId, String dateId, int status,String reason) {
        this.userId = userId;
        this.dateId = dateId;
        this.status = status;
        this.lyDo = reason;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDateId() {
        return dateId;
    }

    public void setDateId(String dateId) {
        this.dateId = dateId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLyDo() {
        return lyDo;
    }

    public void setLyDo(String lyDo) {
        this.lyDo = lyDo;
    }

    public String getNhanXet() {
        return nhanXet;
    }

    public void setNhanXet(String nhanXet) {
        this.nhanXet = nhanXet;
    }
}
