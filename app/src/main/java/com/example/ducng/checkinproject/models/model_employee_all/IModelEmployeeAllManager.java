package com.example.ducng.checkinproject.models.model_employee_all;

import com.example.ducng.checkinproject.ui.mvp_employee_all_manager.IGetAllEmployeeFirebase;

import java.util.List;

public interface IModelEmployeeAllManager {
   void loadAllEmployee(IGetAllEmployeeFirebase iGetAllEmployeeFirebase);
   void loadAllEmployeeUseEventBus();
}
