package com.example.ducng.checkinproject.models.model_employee_all;

public class EventEmployeesMessage {
    private String message;

    public EventEmployeesMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
