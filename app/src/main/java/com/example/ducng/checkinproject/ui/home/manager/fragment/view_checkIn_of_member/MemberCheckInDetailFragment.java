package com.example.ducng.checkinproject.ui.home.manager.fragment.view_checkIn_of_member;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ducng.checkinproject.R;
import com.example.ducng.checkinproject.data.model.CheckIn;
import com.example.ducng.checkinproject.data.model.User;
import com.example.ducng.checkinproject.ui.home.manager.SendUserLogin;
import com.example.ducng.checkinproject.ui.home.manager.calender.DateOfCheckInAdapter;
import com.example.ducng.checkinproject.ui.home.manager.calender.DayOfMonth;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.HomeManagerFragment;
import com.example.ducng.checkinproject.ui.home.manager.fragment.home_manager.IViewRequestUserCheckined;
import com.example.ducng.checkinproject.ui.home.manager.fragment.view_list_member_manager.ManagerFragment;
import com.example.ducng.checkinproject.ui.login.LoginActivity;
import com.example.ducng.checkinproject.ui.mvp_common_employee_manager.CommonEmployeeManager;
import com.example.ducng.checkinproject.ui.mvp_display_group.DisplayGroupFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_all_manager.EmployeeAllManagerFragment;
import com.example.ducng.checkinproject.ui.mvp_employee_of_group_manager.EmployeeOfGroupFragment;
import com.example.ducng.checkinproject.util.NetworkHelper;
import com.example.ducng.checkinproject.util.mvp.manager.request_update_comment_of_checkin.IPesenterRequestUpdateComment;
import com.example.ducng.checkinproject.util.mvp.manager.request_checkin_of_member.IPresenterRequestCheckInMember;
import com.example.ducng.checkinproject.util.mvp.manager.request_checkin_of_member.PresenterRequestCheckInMember;
import com.example.ducng.checkinproject.util.mvp.manager.request_update_comment_of_checkin.PresenterRequestModelUpdateComment;
import com.example.ducng.checkinproject.util.mvp.manager.request_user_logined.IPresenterRequestUserLogined;
import com.example.ducng.checkinproject.util.mvp.manager.request_user_logined.PresenterRequestUserLogined;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MemberCheckInDetailFragment extends Fragment implements IRequestCheckInOfManagerCallback,
        View.OnClickListener, IViewRequestUserCheckined,
        IRequestUpdateComentCallback {
    private int typeTransfer = 0;
    //type =1: transfer tuwf fragment
    //type=2: transfer tu dialog
    private User userIsClick;
    private User userLogined;
    private View rootView;
    private Context context;

    private ImageView imgAva;
    private TextView txtUserName;
    private TextView txtDateOfBirth;
    private TextView txtGrender;
    private TextView txtDateCheckin;
    private ImageView imgCheckInStatus;
    private TextView txtCause;
    private EditText edtComment;
    private Button btnAllCheckin;
    private Button btnOk;

    //Tạo biến lưu trạng thái ngày được chọn sử dụng sau update
    private String beforDateShow;
    private int beforStatus;
    private String beforCause = "";
    private String beforComment = "";
    private String befoorCommentOfOtherDay = "";


    private String today = ""; //Dữ liệu ngày hôm nay định dạng 20180709.
    private boolean flagChooseDay = false; // Kiển tra đã chọn 1 ngày khác ngày hiện tại hay chưa?
    private List<CheckIn> listCheckinOfUser;
    private Dialog dialogCheckinOfMonth;

    private IPresenterRequestCheckInMember iPresenterRequestCheckInMember;
    private IPesenterRequestUpdateComment iPesenterRequestUpdateComment;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_member_check_in_detail_manager, container, false);
        context = container.getContext();
        return rootView;
    }

    /*
        Xác định các thành phần con và lắng nghe sự kiện backPress.
        Cac lớp muốn gọi đến cần truyền vào uerClick thông qua Arguments() với key=ManagerFragment.ITEM_USER_CLICK

         getFragmentManager().beginTransaction()
                            .add(R.id.frament_in_frament_home_manager,
                                    memberCheckInDetailFragment,
                                    HomeManagerFragment.FRAMENT_CHECKIN_MEMBER)
                            .show(memberCheckInDetailFragment)
                            .commit();

        Các fragment trước nên để add(), không sử dụng replace Truyền vào key cho fragmet và truyền cho
        hàm kiểm tra ok và back
         */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NetworkHelper networkHelper = new NetworkHelper();
        if (!networkHelper.isNetworkAvailable(context)) {
            Toast.makeText(context, "Kết nối Internet để tiếp tục sử dụng!", Toast.LENGTH_LONG).show();
            return;
        }

        inits();
    }

    private void onBackPress() {
        //bắt sự kiện backPress
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    if (dialogCheckinOfMonth != null) {
                        if (dialogCheckinOfMonth.isShowing()) {
                            //Không hiểu chỗ này
                            dialogCheckinOfMonth.show();
                        } else {
                            if (flagChooseDay) {

                                //Sau khi vào xem thông tin check in của từng ngày
                                dialogCheckinOfMonth.show();
                            } else

                                //Sau khi chọn vào xem chi tiết, nhưng dialog lịch bị ẩn
                                backToFatherFrament();
                        }
                    } else {
                        //Thông tin check in ngày hiện tại của nhân viên.
                        // sau khi ấn back sẽ trở về màn hình list nhân viên
                        backToFatherFrament();
                    }
                }
                return true;
            }
        });
    }

    private void backToFatherFrament() {
        if (userLogined.getPriovity() == 1) {
            getFragmentManager().beginTransaction()
                    .remove(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECKIN_MEMBER))
                    .show(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_LIST_MEMBER))
                    .commit();
        } else {
            getFragmentManager().beginTransaction()
                    .remove(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECKIN_MEMBER))
                    .commit();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
//        String tempUser = this.getArguments().getString(ManagerFragment.ITEM_USER_CLICK);
//        int tempType= this.getArguments().getInt(EmployeeOfGroupFragment.TYPE_TRANSFER);
//        if (tempType!=0){
//            this.typeTransfer=tempType;
//        }
//        Gson gson = new Gson();
//        userIsClick = gson.fromJson(tempUser, User.class);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void event(SendingUserClick sendingUserClick) {
        if (sendingUserClick != null) {
            userIsClick = sendingUserClick.getUser();
            this.typeTransfer = sendingUserClick.getType();
            EventBus.getDefault().removeStickyEvent(sendingUserClick);
        }

    }


    private void inits() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Xin hãy đợi...");
        listCheckinOfUser = new ArrayList<>();
        imgAva = rootView.findViewById(R.id.img_ava_in_checkin_detail);
        if (!userIsClick.getImage().isEmpty()) {
            Picasso.get()
                    .load(userIsClick.getImage())
                    .resize(200, 200)
                    .centerCrop()
                    .error(R.drawable.ic_error_black)
                    .into(imgAva);
        } else {
            imgAva.setBackgroundResource(R.drawable.frame_ava_in_check_in_detail);
        }
        txtUserName = rootView.findViewById(R.id.txt_user_name_in_checkin_detail);
        txtUserName.setText(userIsClick.getName());
        txtDateOfBirth = rootView.findViewById(R.id.txt_user_ns_in_checkin_detail);
        txtGrender = rootView.findViewById(R.id.txt_grender_in_checkin_detail);
        txtDateOfBirth.setText(userIsClick.getDob());
        txtGrender = rootView.findViewById(R.id.txt_group_in_checkin_detail);
        if (userIsClick.isGender()) {
            txtGrender.setText("Nam");
        } else txtGrender.setText("Nữ");
        txtDateCheckin = rootView.findViewById(R.id.txt_date_in_checkin_detail);
        imgCheckInStatus = rootView.findViewById(R.id.img_check_in_status_in_checkin_detail);
        txtCause = rootView.findViewById(R.id.txt_ly_do_in_checkin_detail);
        edtComment = rootView.findViewById(R.id.edt_coment_in_checkin_detail);
        btnAllCheckin = rootView.findViewById(R.id.btn_all_chek_in_in_checkin_detail);
        btnOk = rootView.findViewById(R.id.btn_ok_in_checkin_detail);
        btnAllCheckin.setOnClickListener(this);
        btnOk.setOnClickListener(this);

        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {

                iPresenterRequestCheckInMember = new PresenterRequestCheckInMember(MemberCheckInDetailFragment.this);
                iPresenterRequestCheckInMember.requestCheckInOfmanager(userIsClick.getId());
            }
        }).start();
        onBackPress();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_all_chek_in_in_checkin_detail:
                showDialogAllCheckIn();
                break;
            case R.id.btn_ok_in_checkin_detail:
                String tempDate = txtDateCheckin.getText().toString();
                String[] listTemp = tempDate.split("/", 3);
                final String dateOfCheckIn = "" + listTemp[2] + "" + listTemp[1] + "" + listTemp[0];
//                getView().setVisibility(View.GONE);
                if (dialogCheckinOfMonth == null) {
                    if (!beforComment.equals(edtComment.getText().toString())) {
                        progressDialog.show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                iPesenterRequestUpdateComment = new PresenterRequestModelUpdateComment(MemberCheckInDetailFragment.this);
                                iPesenterRequestUpdateComment.updateComment(userIsClick.getId(),
                                        dateOfCheckIn, edtComment.getText().toString());
                            }
                        }).start();
                    }
                    backToFatherFrament();
                } else {
                    Log.d("abccb", "onClick: 2");
                    if (!befoorCommentOfOtherDay.equals(edtComment.getText().toString())) {
                        befoorCommentOfOtherDay = edtComment.getText().toString();
                        progressDialog.show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                iPesenterRequestUpdateComment = new PresenterRequestModelUpdateComment(MemberCheckInDetailFragment.this);
                                iPesenterRequestUpdateComment.updateComment(userIsClick.getId(),
                                        dateOfCheckIn, edtComment.getText().toString());
                            }
                        }).start();
                    }
                    dialogCheckinOfMonth.show();
                }
                break;
            default:
                break;
        }

    }

    @Override
    public void respondCheckinSuccessful(CheckIn checkInNow, List<CheckIn> checkIns) {
        listCheckinOfUser = checkIns;
        String dayIsShow = checkInNow.getDateId();
        String temp = dayIsShow;
        String dayShow = "";
        String year = temp.substring(0, 4);
        temp = dayIsShow;
        String month = temp.substring(4, 6);
        temp = dayIsShow;
        String date = temp.substring(6, 8);
        dayShow += date + "/" + month + "/" + year;
        txtDateCheckin.setText(dayShow);
        beforDateShow = dayShow;
        switch (checkInNow.getStatus()) {
            case 0:
                imgCheckInStatus.setBackgroundResource(R.drawable.absent_color);
                break;
            case 1:
                imgCheckInStatus.setBackgroundResource(R.drawable.morning_color);
                break;
            case 2:
                imgCheckInStatus.setBackgroundResource(R.drawable.afternoon_color);
                break;
            case 3:
                imgCheckInStatus.setBackgroundResource(R.drawable.full_color);
                LinearLayout linearLayout = rootView.findViewById(R.id.lin_layout_lydo_in_fragment_check_in_detail);
                linearLayout.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        beforStatus = checkInNow.getStatus();
        if (checkInNow.getLyDo() != null) {
            txtCause.setText(checkInNow.getLyDo());
            beforCause = checkInNow.getLyDo();
        } else {
            beforCause = "";
            txtCause.setText("");
        }
        if (checkInNow.getNhanXet() != null) {
            edtComment.setText(checkInNow.getNhanXet());
        } else edtComment.setText("");
        beforComment = edtComment.getText().toString();

        if (typeTransfer == 2) {
            btnAllCheckin.setVisibility(View.GONE);
            showDialogAllCheckIn();
        }
        progressDialog.hide();
    }

    @Override
    public void respondCheckinFalse() {
        listCheckinOfUser = new ArrayList<>();

        String dayShow = "";
        Calendar calendar = Calendar.getInstance();
        int date = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        month += 1;
        dayShow += date + "/" + month + "/" + year;
        txtDateCheckin.setText(dayShow);
        beforDateShow = dayShow;
        txtDateCheckin.setText(dayShow);
        imgCheckInStatus.setBackgroundResource(R.drawable.absent_color);
        txtCause.setText("");
        beforCause = "";
        beforComment = "";
        edtComment.setText("");
        progressDialog.hide();
    }

    @Override
    public void updateSeccess(List<CheckIn> checkIns) {
        Toast.makeText(context, "Cập nhật nhận xét thành công", Toast.LENGTH_SHORT).show();
        this.listCheckinOfUser = checkIns;
        if (flagChooseDay) {
            txtDateCheckin.setText(beforDateShow);
            switch (beforStatus) {
                case 0:
                    imgCheckInStatus.setBackgroundResource(R.drawable.absent_color);
                    break;
                case 1:
                    imgCheckInStatus.setBackgroundResource(R.drawable.morning_color);
                    break;
                case 2:
                    imgCheckInStatus.setBackgroundResource(R.drawable.afternoon_color);
                    break;
                case 3:
                    imgCheckInStatus.setBackgroundResource(R.drawable.full_color);
                    LinearLayout linearLayout = rootView.findViewById(R.id.lin_layout_lydo_in_fragment_check_in_detail);
                    linearLayout.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
            txtCause.setText(beforCause);
            edtComment.setText(befoorCommentOfOtherDay);
            progressDialog.hide();
            dialogCheckinOfMonth.show();
        } else {
            getFragmentManager().beginTransaction()
                    .remove(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_CHECKIN_MEMBER))
                    .show(getFragmentManager().findFragmentByTag(HomeManagerFragment.FRAMENT_LIST_MEMBER))
                    .commit();
        }
    }

    @Override
    public void updateFailt() {
        progressDialog.hide();
        Toast.makeText(context, "Cập nhật nhận xét gặp lỗi. Xin vui lòng thử lại", Toast.LENGTH_SHORT).show();
    }


    public void showDialogAllCheckIn() {
        today = "";
        Calendar calendar = Calendar.getInstance();
        int date = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        month += 1;
        today += String.valueOf(year);
        if (month < 10) {
            today += "0" + String.valueOf(month);
        } else {
            today += String.valueOf(month);
        }
        if (date < 10) {
            today += "0" + String.valueOf(date);
        } else {
            today += String.valueOf(date);
        }
        dialogCheckinOfMonth = new Dialog(context);
        dialogCheckinOfMonth.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCheckinOfMonth.setContentView(R.layout.dialog_check_in_of_month);
        initCompomentDialog(dialogCheckinOfMonth);
        dialogCheckinOfMonth.show();
    }

    private void initCompomentDialog(final Dialog dialog) {
        beforComment = "";
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        month++;
        TextView txtUsername = dialog.findViewById(R.id.txt_user_name_in_dialog_checkin);
        txtUsername.setText(userIsClick.getName());
        ImageView btnClose = dialog.findViewById(R.id.btn_close_dialog);
        ImageView btnLatsMonth = dialog.findViewById(R.id.btn_last_month_in_dialog);
        final TextView txtMonth = dialog.findViewById(R.id.txt_month_in_dialog_checkin);
        txtMonth.setText(String.valueOf(month));
        final TextView txtYear = dialog.findViewById(R.id.txt_year_in_dialog_check_in);
        txtYear.setText(String.valueOf(year));
        ImageView btnNextMonth = dialog.findViewById(R.id.btn_next_month_in_dialog);
        final GridView gridView = dialog.findViewById(R.id.grdView_in_dialog);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.btn_close_dialog:
                        if (flagChooseDay) {
                            flagChooseDay = false;
                            dialog.dismiss();
                            backToFatherFrament();
                        } else
                            dialog.dismiss();
                        break;
                    case R.id.btn_last_month_in_dialog:
                        int year = Integer.parseInt(txtYear.getText().toString());
                        int month = Integer.parseInt(txtMonth.getText().toString());
                        if (txtMonth.getText().toString().equals("1")) {
                            txtMonth.setText("12");
                            year--;
                            txtYear.setText(String.valueOf(year));
                            updateNewMonth(12, year, gridView);
                        } else {
                            month--;
                            txtMonth.setText(String.valueOf(month));
                            updateNewMonth(month, year, gridView);
                        }
                        break;
                    case R.id.btn_next_month_in_dialog:
                        int year2 = Integer.parseInt(txtYear.getText().toString());
                        int month2 = Integer.parseInt(txtMonth.getText().toString());
                        if (txtMonth.getText().toString().equals("12")) {
                            txtMonth.setText("1");
                            year2++;
                            txtYear.setText(String.valueOf(year2));
                            updateNewMonth(1, year2, gridView);
                        } else {
                            month2++;
                            txtMonth.setText(String.valueOf(month2));
                            updateNewMonth(month2, year2, gridView);
                        }
                        break;
                    default:
                        break;
                }
            }
        };
        btnClose.setOnClickListener(onClickListener);
        btnLatsMonth.setOnClickListener(onClickListener);
        btnNextMonth.setOnClickListener(onClickListener);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    if (flagChooseDay) {
                        backToFatherFrament();
                    }
                }
                return false;
            }
        });

        updateNewMonth(Integer.parseInt(txtMonth.getText().toString()), Integer.parseInt(txtYear.getText().toString()), gridView);
    }

    private void updateNewMonth(final int month, final int year, final GridView gridView) {
        dialogCheckinOfMonth.show();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
        DayOfMonth dayOfMonth = new DayOfMonth();
        final List<Integer> listdate = dayOfMonth.createListDayOfMonth(month, year);
        final int indexStart = dayOfMonth.getIndexStart();
        final int indexEnd = dayOfMonth.getIndexEnd();
        String startDay = "" + year;
        if (month < 11) {
            if (month == 1) {
                startDay = "" + (year - 1) + "1231";
            } else
                startDay += "0" + (month - 1) + "28";
        } else
            startDay += (month - 1) + "28";

        String endDay = "" + year;
        if (month < 9) {
            endDay += "0" + (month + 1) + "01";
        } else if (month == 12) {
            endDay = "" + (year + 1) + "0101";
        } else
            endDay += (month + 1) + "01";

        final List<CheckIn> checkIns = splitListCheckin(startDay, endDay);
        DateOfCheckInAdapter dateOfCheckInAdapter = new DateOfCheckInAdapter(context,
                checkIns, month, year, today, listdate, indexStart, indexEnd);
        gridView.setAdapter(dateOfCheckInAdapter);
        progressDialog.hide();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                flagChooseDay = true;
                String tempDate = "" + year;
                if (month < 10) {
                    tempDate += "0" + month;
                } else {
                    tempDate += month;
                }
                if (listdate.get(i) < 10) {
                    tempDate += "0" + listdate.get(i);
                } else tempDate += listdate.get(i);

                if (i < indexStart || i > indexEnd || i % 7 == 0 ||
                        Integer.parseInt(tempDate) > Integer.parseInt(today)) {
                    return;
                } else {
                    String dateClick = "" + year;
                    beforDateShow = "";

                    if (month < 10) {
                        dateClick += "0" + month;
                    } else
                        dateClick += month;

                    if (listdate.get(i) < 10) {
                        dateClick += "0" + listdate.get(i);
                        beforDateShow += "0" + listdate.get(i);
                    } else {
                        dateClick += listdate.get(i);
                        beforDateShow += listdate.get(i);
                    }

                    if (month < 10) {
                        beforDateShow += "/0" + month + "/" + year;
                    } else
                        beforDateShow += "/" + month + "/" + year;

                    CheckIn checkinIsClick = DateOfCheckInAdapter.searchCheckin(dateClick, listCheckinOfUser);
                    txtDateCheckin.setText(beforDateShow);
                    if (checkinIsClick == null) {
                        imgCheckInStatus.setBackgroundResource(R.drawable.absent_color);
                        beforStatus = 0;
                    } else {
                        beforStatus = checkinIsClick.getStatus();
                        switch (checkinIsClick.getStatus()) {
                            case 0:
                                imgCheckInStatus.setBackgroundResource(R.drawable.absent_color);
                                break;
                            case 1:
                                imgCheckInStatus.setBackgroundResource(R.drawable.morning_color);
                                break;
                            case 2:
                                imgCheckInStatus.setBackgroundResource(R.drawable.afternoon_color);
                                break;
                            case 3:
                                imgCheckInStatus.setBackgroundResource(R.drawable.full_color);
                                LinearLayout linearLayout = rootView.findViewById(R.id.lin_layout_lydo_in_fragment_check_in_detail);
                                linearLayout.setVisibility(View.GONE);
                                break;
                            default:
                                break;
                        }
                        Log.d("abccd", "onItemClick: 1");
                        if (checkinIsClick.getLyDo() == null) {
                            beforCause = "";
                        } else
                            beforCause = checkinIsClick.getLyDo();
                        if (checkinIsClick.getNhanXet() == null) {
                            befoorCommentOfOtherDay = "";
                        } else
                            befoorCommentOfOtherDay = checkinIsClick.getNhanXet();
                        txtCause.setText(beforCause);
                        edtComment.setText(befoorCommentOfOtherDay);
                    }
                    btnAllCheckin.setVisibility(View.GONE);
                    dialogCheckinOfMonth.hide();
                }
            }
        });
    }
//        }).start();
//    }


    //Trả về vị trí gần nhất với vị trí gần tìm kiếm
    //index=0: tìm phần tử đầu của chuỗi
//    index= 1: tìm phần tử cuối của chuỗi
    private int searchCheckinReturnIndex(String dateId, int index) {
        if (listCheckinOfUser.isEmpty()) {
            return 0;
        }
        int date = Integer.parseInt(dateId);
        int start = 0;
        int end = listCheckinOfUser.size() - 1;
        while (end - start > 1) {
            int temp = (end + start) / 2;
            int checkinDate = Integer.parseInt(listCheckinOfUser.get(temp).getDateId());
            if (checkinDate > date) {
                end = temp;
            } else if (checkinDate < date) {
                start = temp;
            } else {
                return temp;
            }
        }
        int dateStart = Integer.parseInt(listCheckinOfUser.get(start).getDateId());
        if (dateStart == date) {
            return start;
        }
        int dateEnd = Integer.parseInt(listCheckinOfUser.get(start).getDateId());
        if (dateEnd == date) {
            return end;
        }
        if (index == 0) {
            return start;
        } else
            return end;
    }

    private List<CheckIn> splitListCheckin(String dayStart, String dayEnd) {
        if (listCheckinOfUser == null || listCheckinOfUser.size() == 0) {
            return null;
        }
        int indexStart = searchCheckinReturnIndex(dayStart, 0);
        int indexEnd = searchCheckinReturnIndex(dayEnd, 1);
        List<CheckIn> checkIns = new ArrayList<>();
        for (int i = indexStart; i <= indexEnd; i++) {
            checkIns.add(listCheckinOfUser.get(i));
        }
        return checkIns;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getUserLogin(SendUserLogin sendUserLogin) {
        Log.d("gfh", "getUserLogin: ");
        if (sendUserLogin != null) {
            this.userLogined = sendUserLogin.getUser();
        }

    }
    @Override
    public void getUserSuccess(User user) {
//        userLogined = user;
    }

    @Override
    public void getUserFailt() {
//        Toast.makeText(context, "Không lấy được thông tin người dùng, xin vui lòng đăng nhập lại!", Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(context, LoginActivity.class);
//        startActivity(intent);
    }
}
